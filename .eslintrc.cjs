module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  plugins: ['@typescript-eslint', 'eslint-plugin-import'],
  ignorePatterns: ['*.cjs'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020
  },
  env: {
    es2017: true,
    node: true
  },
  rules: {
    '@typescript-eslint/no-unused-vars': 'error',
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        'pathGroups': [{
          'pattern': '$*/**',
          'group': 'internal',
          'position': 'before'
        }, {
          'pattern': '@webbr/**',
          'group': 'external',
          'position': 'after',
        }, {
          'pattern': '$lib/**',
          'group': 'external',
          'position': 'after',
        }],
        'pathGroupsExcludedImportTypes': [],
        'groups': [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
          'object',
          'type'
        ]
      }
    ],
    'no-console': 'error',
  },
};
