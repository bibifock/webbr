// @ts-check
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import eslintPluginImportX from 'eslint-plugin-import-x';
import prettier from "eslint-config-prettier";

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommendedTypeChecked,
  {
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigRootDir: import.meta.dirname
      }
    }
  },
  prettier,
  eslintPluginImportX.flatConfigs.recommended,
  eslintPluginImportX.flatConfigs.typescript,
  {
    rules: {
      '@typescript-eslint/no-unused-vars': 'error',
      'import-x/order': [
        'error',
        {
          'newlines-between': 'always',
          pathGroups: [
            {
              pattern: '$*/**',
              group: 'internal',
              position: 'before'
            },
            {
              pattern: '@webbr/**',
              group: 'external',
              position: 'after'
            },
            {
              pattern: '$lib/**',
              group: 'external',
              position: 'after'
            }
          ],
          pathGroupsExcludedImportTypes: [],
          groups: [
            'builtin',
            'external',
            'internal',
            'parent',
            'sibling',
            'index',
            'object',
            'type'
          ]
        }
      ],
      'no-console': 'error'
    }
  }
);
