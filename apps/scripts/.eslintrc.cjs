module.exports = {
  extends: '../../.eslintrc.cjs',
  plugins: ['eslint-plugin-jest'],
  env: {
    'jest/globals': true
  }
};
