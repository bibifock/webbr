/* eslint-disable quotes */
const colors = {
  Reset: '\x1b[0m',
  Red: '\x1b[31m',
  Green: '\x1b[32m',
  Yellow: '\x1b[33m'
};
/* eslint-enable */

/* eslint-disable no-console */
const infoLog = console.info;
const errorLog = console.error;
const warnLog = console.warn;
const defaultLog = console.log;
/* eslint-enable */

const log = function (...args: unknown[]) {
  defaultLog(...[colors.Reset, ...args, colors.Reset]);
};

const info = function (...args: unknown[]) {
  infoLog(...[colors.Green, ...args, colors.Reset]);
};

const warn = function (...args: unknown[]) {
  warnLog(...[colors.Yellow, ...args, colors.Reset]);
};

const error = function (...args: unknown[]) {
  errorLog(...[colors.Red, ...args, colors.Reset]);
};

export default {
  info,
  error,
  warn,
  log
};
