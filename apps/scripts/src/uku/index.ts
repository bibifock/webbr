#!/usr/bin/env node
import path from 'path';

import yargs from 'yargs';

import { makeCli } from '../makeCli';

makeCli('uku', path.dirname(__filename));

yargs.help().argv;
