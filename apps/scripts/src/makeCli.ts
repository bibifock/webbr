import yargs from 'yargs';

import { makeMigrate } from './makeMigrate';

export const makeCli = (name: string, migrationsPath: string) => {
  const cli = yargs.scriptName(name);

  const { usage, description, definition, action } = makeMigrate(
    migrationsPath + '/migrations'
  );

  cli.command(usage, definition, description, action);

  return cli;
};
