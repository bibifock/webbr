#!/usr/bin/env node
import path from 'path';

import yargs from 'yargs';

import { makeCli } from '../makeCli';

import syncLibraryCmd from './sync-library';

const commands = [syncLibraryCmd];

const cli = makeCli('lirelu', path.dirname(__filename));

commands.map(({ usage, description, definition, action }) =>
  cli.command(usage, description, definition, action)
);

yargs.help().argv;
