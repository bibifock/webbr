// eslint.config.js
export default [
  {
    ignores: ['*.cjs'], // Similar to `ignorePatterns`
  },
  {
    languageOptions: {
      ecmaVersion: 2020,  // Set ECMAScript version to 2020
      sourceType: 'module',  // Use ECMAScript modules
    },
    parser: '@typescript-eslint/parser',
    plugins: {
      '@typescript-eslint': '@typescript-eslint/eslint-plugin',
      import: 'eslint-plugin-import',
    },
    rules: {
      '@typescript-eslint/no-unused-vars': 'error',
      'import/order': [
        'error',
        {
          'newlines-between': 'always',
          'pathGroups': [
            {
              'pattern': '$*/**',
              'group': 'internal',
              'position': 'before',
            },
            {
              'pattern': '@webbr/**',
              'group': 'external',
              'position': 'after',
            },
            {
              'pattern': '$lib/**',
              'group': 'external',
              'position': 'after',
            },
          ],
          'pathGroupsExcludedImportTypes': [],
          'groups': [
            'builtin',
            'external',
            'internal',
            'parent',
            'sibling',
            'index',
            'object',
            'type',
          ],
        },
      ],
      'no-console': 'error',
    },
    settings: {
      // You can include specific settings for imports or TypeScript here if needed.
    },
    env: {
      es2017: true,
      node: true,
    },
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  },
];

