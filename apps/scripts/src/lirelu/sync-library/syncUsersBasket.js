import { loanModel, userModel } from '@webbr/lirelu-db';
import { parisBibli } from '@webbr/api-bibli';

import log from '../../console';

import { importNovel } from './utils/importNovel';

const  { getUserBasket } = parisBibli;

const getUsersAccount = (id) =>
  id ? userModel.allByProps({ id }) : userModel.getActiveAccounts();

export const syncUsersBasket = async (id) => {
  const users = await getUsersAccount(id);
  log.log(`${users.length} users with stack found`);

  for (const user of users) {
    const { email: username, dateOfBirth: password } = user;

    log.log(` ----------------- [${user.name}]`);
    log.log(` --> import bibli user basket`);
    const basket = await getUserBasket({ username, password });
    log.log('   clean loans');
    await loanModel.cleanUser(user);

    log.log(`    ${basket.length} novels found`);

    for (const novel of basket) {
      await importNovel(novel, user);
    }

    log.log(`    basket up to date`);
  }
};
