import { parisBibli } from '@webbr/api-bibli';
import { searchModel, Search } from '@webbr/lirelu-db';

import log from '../../console';

export const updateSearchs = async () => {
  log.log(' ----------------- [update search]');
  const items = await searchModel.getActiveSearch();

  log.log(`${items.length} searchs found`);
  const searchsToUpdate: Search[] = [];
  for (const item of items) {
    const result = await parisBibli.search(item.search);

    if (result.length === item.nbResults) {
      continue;
    }

    searchsToUpdate.push({
      ...item,
      nbResults: result.length,
      lastUpdate: new Date()
        .toISOString()
        .replace(/T/, ' ')
        .replace(/\..+$/, '')
    });
  }
  log.log(`${searchsToUpdate.length} searchs to update`);

  if (!searchsToUpdate.length) {
    return;
  }

  await searchModel.saveAndPopulate(searchsToUpdate);
};
