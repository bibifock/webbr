import { bookModel } from '@webbr/lirelu-db';

import log from '../../console';

import { addBookcases } from './utils/addBookcases';

export const syncBooksInfos = async () => {
  log.log(' ----------------- [update books]');
  const books = await bookModel.getBookWithoutSaga();
  log.log(`    ${books.length} books found`);

  await addBookcases(books);

  log.log('   books up to date');
};
