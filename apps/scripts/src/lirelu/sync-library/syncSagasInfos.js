import { sagaModel } from '@webbr/lirelu-db';

import log from '../../console';

import { importNovel } from './utils/importNovel';

export const syncSagasInfos = async () => {
  // first re - import all sagas
  log.log(` ----------------- [looking for new saga novel]`);
  const sagas = await sagaModel.all();
  log.log(`   ${sagas.length} sagas found`);

  for (const saga of sagas) {
    await importNovel({ saga });
  }

  log.log(`    sagas up to date`);
};
