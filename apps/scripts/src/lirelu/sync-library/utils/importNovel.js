import { saveNovel } from '@webbr/lirelu-db';
import { parisBibli } from '@webbr/api-bibli';

const { extractBooksInfos, getBookcases } = parisBibli;

/**
 * import novel and saga from paris bibli in db and user stack
 * @param {object} novel
 * @param {objetc} user
 *
 * @return {promise}
 */
export const importNovel = async (novel, user = undefined) => {
  const booksInfos = await extractBooksInfos(novel);

  if (!booksInfos) {
    // eslint-disable-next-line no-console
    console.warn('importNovel', 'nothing found for this novel', novel);
    return null;
  }

  return saveNovel(booksInfos, user, {
    booksPath: process.env.UPLOAD_BOOKS_PATH,
    uploadDir: process.env.UPLOAD_DIR,
    getBookcases
  });
};
