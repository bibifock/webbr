import { parisBibli } from '@webbr/api-bibli';
import { saveBookcases } from '@webbr/lirelu-db';

/**
 * load all book cases informations
 * @param {array} books
 *
 * @return {promise}
 */
export const addBookcases = async (books) => {
  const results = await parisBibli.getBookcases(books);

  await saveBookcases(results);
};
