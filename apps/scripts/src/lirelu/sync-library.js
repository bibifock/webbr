import log from '../console';

import { syncUsersBasket } from './sync-library/syncUsersBasket';
import { syncBooksInfos } from './sync-library/syncBooksInfos';
import { syncSagasInfos } from './sync-library/syncSagasInfos';
import { updateSearchs } from './sync-library/updateSearchs';

const doAction = async ({ user: id }) => {
  try {
    await syncUsersBasket(id);
  } catch (e) {
    log.error('syncUsersBasket', e);
  }

  try {
    await syncSagasInfos();
  } catch (e) {
    log.error('syncSagasInfos', e);
  }

  try {
    await syncBooksInfos();
  } catch (e) {
    log.error('syncBooksInfos', e);
  }

  try {
    await updateSearchs();
  } catch (e) {
    log.error('updateSearchs', e);
  }

  process.exit;
};

const action = (args) => doAction(args).catch(log.error);

const usage = 'sync-library';
const description = 'sync library data';
const definition = (yargs) =>
  yargs.option('user', {
    alias: 'u',
    describe: 'user id to import'
  });

export default {
  action,
  usage,
  definition,
  description
};
