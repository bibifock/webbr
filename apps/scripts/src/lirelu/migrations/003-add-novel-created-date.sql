--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE novels RENAME TO _novels;

CREATE TABLE novels (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL COLLATE NOCASE,
    description TEXT NULL COLLATE NOCASE,
    idAuthor INTEGER NULL,
    idSaga INTEGER NULL,
    sagaVolume INTEGER NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO novels (id, title, description, idAuthor, idSaga, sagaVolume)
  SELECT id, title, description, idAuthor, idSaga, sagaVolume
  FROM _novels;

DROP TABLE _novels;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE novels RENAME TO _novels;

CREATE TABLE novels (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL COLLATE NOCASE,
    description TEXT NULL COLLATE NOCASE,
    idAuthor INTEGER NULL,
    idSaga INTEGER NULL,
    sagaVolume INTEGER NULL
);

INSERT INTO novels (id, title, description, idAuthor, idSaga, sagaVolume)
  SELECT id, title, description, idAuthor, idSaga, sagaVolume
  FROM _novels;

DROP TABLE _novels;
