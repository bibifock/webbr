--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE searchs (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    search TEXT NOT NULL COLLATE NOCASE,
    nbResults INTEGER DEFAULT 0,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastUpdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE searchFollowed (
    idUser INTEGER NOT NULL,
    idSearch INTEGER NOT NULL,
    isActive INTEGER NULL,
    PRIMARY KEY (idUser, idSearch)
);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE searchs;
DROP TABLE searchFollowed;
