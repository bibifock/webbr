--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
-- fix roshar
UPDATE novels SET sagaVolume=1.1 WHERE idSaga=45 AND sagaVolume=1;
UPDATE novels SET sagaVolume=2.1 WHERE idSaga=45 AND sagaVolume=2;
UPDATE novels SET sagaVolume=3.1 WHERE idSaga=45 AND sagaVolume=3;
UPDATE novels SET sagaVolume=4.1 WHERE idSaga=45 AND sagaVolume=4;

-- delete bad import
DELETE FROM novels
WHERE id=320 -- licanius import
  OR id=3 -- chevauche brumes
  OR id=217 OR id=347 -- ramirez
  OR id=185 -- Terry Pratchett - Sourcellerie
  OR id=66 -- les dieux silencieux
  OR id=279 -- maître enlumineurs
;

DELETE FROM books WHERE idNovel NOT IN (SELECT DISTINCT id FROM novels);
DELETE FROM bookcase WHERE idBook NOT IN (SELECT DISTINCT id FROM books);
DELETE FROM novelsStack WHERE idNovel NOT IN (SELECT DISTINCT id FROM novels);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
