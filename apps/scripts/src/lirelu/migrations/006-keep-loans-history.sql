--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE loans ADD isActive INTEGER NOT NULL DEFAULT 1;

ALTER TABLE loans ADD updated TIMESTAMP;
UPDATE loans SET updated = CURRENT_TIMESTAMP WHERE 1=1;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
BEGIN TRANSACTION;

ALTER TABLE loans RENAME TO _loans_old;

CREATE TABLE loans (
    idUser INTEGER NOT NULL,
    idBook INTEGER NOT NULL,
    idLibrary INTEGER NOT NULL,
    whenBack TEXT NULL COLLATE NOCASE,
    details TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (idUser, idBook, idLibrary)
);

INSERT INTO files (idUser, idBook, idLibrary, whenBack, details)
  SELECT idUser, idBook, idLibrary, whenBack, details
  FROM _loans_old;

DROP TABLE _loans_old;

COMMIT;

