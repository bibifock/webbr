--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE users (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email TEXT NOT NULL COLLATE NOCASE,
    name TEXT NULL COLLATE NOCASE,
    password TEXT NULL,
    dateOfBirth TEXT NULL COLLATE NOCASE,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastConnection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE status (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NULL COLLATE NOCASE
);

INSERT INTO status (id, name) VALUES
    (1, 'to read'),
    (2, 'is reading'),
    (3, 'finished'),
    (4, 'unfinished');

CREATE TABLE authors (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT NULL COLLATE NOCASE
);

CREATE TABLE saga (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL COLLATE NOCASE,
    nbNovels INTEGER NULL
);

CREATE TABLE novels (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL COLLATE NOCASE,
    description TEXT NULL COLLATE NOCASE,
    idAuthor INTEGER NULL,
    idSaga INTEGER NULL,
    sagaVolume INTEGER NULL
);

CREATE TABLE books (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    idNovel INTEGER NOT NULL,
    isbn TEXT NOT NULL COLLATE NOCASE,
    edition TEXT NULL COLLATE NOCASE,
    url TEXT NULL COLLATE NOCASE,
    image TEXT NULL COLLATE NOCASE,
    details TEXT NULL COLLATE NOCASE
);

CREATE TABLE novelsStack (
    idUser INTEGER NOT NULL,
    idNovel INTEGER NOT NULL,
    idStatus INTEGER NOT NULL,
    remark TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (idUser, idNovel)
);

CREATE TABLE libraries (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT NULL COLLATE NOCASE,
    url TEXT NULL COLLATE NOCASE
);

CREATE TABLE bookcase (
    idLibrary INTEGER NOT NULL,
    idBook INTEGER NOT NULL,
    isAvailable INTEGER NULL,
    isReservable INTEGER NULL,
    whenBack TEXT NULL COLLATE NOCASE,
    details TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (idLibrary, idBook)
);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE users;
DROP TABLE status;
DROP TABLE authors;
DROP TABLE saga;
DROP TABLE novels;
DROP TABLE novelsStack;
DROP TABLE books;
DROP TABLE libraries;
DROP TABLE bookcase;

