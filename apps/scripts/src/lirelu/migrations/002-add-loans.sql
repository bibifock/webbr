--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE loans (
    idUser INTEGER NOT NULL,
    idBook INTEGER NOT NULL,
    idLibrary INTEGER NOT NULL,
    whenBack TEXT NULL COLLATE NOCASE,
    details TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (idUser, idBook, idLibrary)
);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE loans;
