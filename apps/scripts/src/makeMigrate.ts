import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

import log from './console';

const dbParams = {
  filename: process.env.DB_SOURCE ?? '',
  driver: sqlite3.Database
};

export const migrateAction = (migrationsPath: string) => () =>
  open(dbParams)
    .then((db) => db.migrate({ migrationsPath }))
    .catch((e: unknown) => log.error('Failed: ', e));

export const makeMigrate = (migrationsPath: string) => ({
  action: migrateAction(migrationsPath),
  usage: 'migrate',
  definition: 'play migration in database .env.DB_SOURCE',
  description: {}
});
