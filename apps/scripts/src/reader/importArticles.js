import { feedsSync } from './feedsSync';
import { spotifySync } from './spotifySync';

const action = async (args) => {
  await feedsSync(args);

  if (args.idFeed) {
    return;
  }
  await spotifySync();
};

const usage = 'import:articles';
const description = 'check all feeds new items';
const definition = (yargs) =>
  yargs.option('idFeed', {
    alias: 'id',
    describe: 'id feed to import'
  });

export default {
  action,
  usage,
  definition,
  description
};
