#!/usr/bin/env node
import path from 'path';

import yargs from 'yargs';

import { makeCli } from '../makeCli';

import importArticlesCmd from './importArticles';

const commands = [importArticlesCmd];

const cli = makeCli('reader', path.dirname(__filename));

commands.map(({ usage, description, definition, action }) =>
  cli.command<{ idFeed: string }>(usage, description, definition, action)
);

yargs.help().argv;
