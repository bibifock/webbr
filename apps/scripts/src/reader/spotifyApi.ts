import SpotifyWebApi from 'spotify-web-api-node';

import log from '../console';

type APIResponse =
  | {
      statusCode: 429;
      headers: {
        'retry-after': number;
      };
    }
  | {
      statusCode: number;
      headers: Record<string, string | number>;
      body: unknown;
    };

type Artist = { name: string; id: string };
type Album = {
  title: string;
  url: string;
  image: string;
  description: string;
  created: Date;
};

const api = new SpotifyWebApi({
  redirectUri: process.env.SPOTIFY_REDIRECT_URI,
  clientId: process.env.SPOTIFY_CLIENT_ID,
  clientSecret: process.env.SPOTIFY_CLIENT_SECRET
});

export const spotifyApi = ({
  tokens: { accessToken, refreshToken },
  onRefreshAccessToken
}: {
  tokens: {
    accessToken: string;
    refreshToken: string;
  };
  onRefreshAccessToken: (token: string) => void;
}) => {
  api.setAccessToken(accessToken);
  api.setRefreshToken(refreshToken);

  const apiCall = async <T>(action: () => Promise<T>): Promise<T> =>
    action().catch(async (err: APIResponse) => {
      if (err.statusCode === 429) {
        const delay = err.headers['retry-after'] as number;

        await new Promise((r) => {
          const logMessage = `apiCall: to much call need to wait ${delay} sec`;
          if (delay > 10) {
            log.error(logMessage);
            throw new Error(`api delay too long ${delay}`);
          }
          log.info(logMessage);

          setTimeout(r, (delay + 1) * 1000);
        });

        return await action();
      }

      if (err.statusCode == 401) {
        return api.refreshAccessToken().then(
          function (data: { body: { access_token: string } }) {
            const accessToken = data.body['access_token'];
            // Save the access token so that it's used in future calls
            api.setAccessToken(accessToken);

            onRefreshAccessToken(accessToken);

            return action();
          },
          function (err: Error) {
            // TODO handle re auth
            // eslint-disable-next-line no-console
            console.error('could not refresh token', err);
            throw err;
          }
        );
      }

      // eslint-disable-next-line no-console
      console.error('FAILED apiCall: ', err);
      throw err;
    });

  const getAllArtists = async () => {
    let artists: Artist[] = [];
    let after: string | undefined = undefined;
    do {
      const result = await apiCall(() =>
        api.getFollowedArtists({ limit: 50, after })
      );
      if (!result) {
        break;
      }
      artists = [...artists, ...result.body.artists.items];
      after = result.body.artists.cursors.after;
      //console.log(artists.length + '/' + result.body.artists.total);

      if (!after) {
        break;
      }
      // eslint-disable-next-line no-constant-condition
    } while (true);

    return artists;
  };

  const getNewAlbums = async (artists: Artist[]) => {
    const limit = new Date(Date.now() - 10 * 24 * 60 * 60 * 1000)
      .toISOString()
      .replace(/T.*/, '');

    let newAlbums: Album[] = [];
    for (const artist of artists) {
      const result = await apiCall(() =>
        api.getArtistAlbums(artist.id, { include_groups: 'album' })
      );
      if (!result.body) {
        //console.log('====>', artist.name, result);
        throw Error('no body');
      }

      for (const item of result.body.items) {
        if (item.release_date < limit) {
          break;
        }

        const url = item.href.replace(
          /^.+\/([^/]+)$/,
          'https://open.spotify.com/album/$1'
        );
        const embed = url.replace(/album\//g, 'embed/album/');

        newAlbums = [
          ...newAlbums,
          {
            title: `${artist.name} - ${item.name}`,
            url,
            image: item.images.length > 0 ? item.images[0].url : '',
            description: `
              <p>${item.release_date}</p>
              <p>
                <iframe
                  style="border-radius:12px"
                  src="${embed}?utm_source=generator&theme=0"
                  width="100%"
                  height="352"
                  frameBorder="0"
                  allowfullscreen=""
                  allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
                  loading="lazy"
                ></iframe>
              </p>
            `,
            created: new Date(item.release_date)
          }
        ];

        //console.log(artist.name, '-', item.name, '-', item.href);
      }
    }

    return newAlbums;
  };

  return {
    getAllArtists,
    getNewAlbums
  };
};
