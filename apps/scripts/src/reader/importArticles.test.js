import flatten from 'lodash/flatten';
import uniq from 'lodash/uniq';
import { resetDb, cleanTables } from 'utils/testing/database';
import factories from 'utils/testing/factories';
import articleModel from 'app/models/article/article';
import notificationsModel from 'app/models/notifications/notifications';
import feedModel from 'app/models/feed/feed';
import subscriptionsModel from 'app/models/subscriptions/subscriptions';
import * as utilsFeed from 'app/controllers/feeds/utils';

import log from '../utils/console';

import command from './importArticles';

const feeds = [1, 2, 3].map((id) => factories.feed({ id }));

const nbItemsByFeed = 3;

const articles = flatten(
  feeds.map(({ id: idFeed }) =>
    Array.from(Array(nbItemsByFeed)).map((_, i) =>
      factories.article({ idFeed, title: `feed: ${idFeed} - index: ${i}` })
    )
  )
);

const spies = ['log', 'warn', 'info'];

describe('commands/import-articles importArticles', () => {
  beforeAll(async () => {
    spies.forEach((spy) => jest.spyOn(log, spy).mockReturnValue(true));
  });

  afterAll(() => {
    spies.forEach((spy) => log[spy].mockRestore());
  });

  beforeEach(async () => {
    await resetDb();

    await feedModel.saveAll(feeds);
  });

  it('when no feed followed nothing to import', async () => {
    await cleanTables([subscriptionsModel.config.table]);

    await command.action({});

    const result = await articleModel.all();
    expect(result).toHaveLength(0);
  });

  it('should not add notif on read articles', async () => {
    await cleanTables([
      articleModel.config.table,
      notificationsModel.config.table,
      subscriptionsModel.config.table
    ]);

    await feedModel.saveAll(feeds);
    const idUser = 1;

    await subscriptionsModel.saveAll([
      // feed 1 both
      { idFeed: 1, idUser }
    ]);

    jest
      .spyOn(utilsFeed, 'parseFeed')
      .mockImplementation(({ id }) =>
        Promise.resolve(articles.filter((v) => v.idFeed === id))
      );

    await command.action({});

    const notifs = await notificationsModel.allByProps({ idUser, unread: 1 });
    expect(notifs).toHaveLength(nbItemsByFeed);

    notificationsModel.save({ ...notifs[0], unread: 0 });

    await command.action({});

    const finalNotifs = await notificationsModel.allByProps({
      idUser,
      unread: 1
    });
    expect(finalNotifs).toHaveLength(nbItemsByFeed - 1);
  });

  describe('on parse error', () => {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    beforeEach(async () => {
      await cleanTables([
        articleModel.config.table,
        notificationsModel.config.table,
        subscriptionsModel.config.table
      ]);

      await feedModel.saveAll(feeds);

      await subscriptionsModel.saveAll([
        // feed 1 both
        { idFeed: 1, idUser: 1 },
        // feed 2 one
        { idFeed: 2, idUser: 1 }
        // feed 3 no subscriptions
      ]);

      jest.spyOn(utilsFeed, 'parseFeed').mockImplementation(({ id }) => {
        if (id === 1) {
          return Promise.resolve(articles.filter((v) => v.idFeed === id));
        }

        return Promise.reject(new Error('parse channel error'));
      });
      await command.action({});
    });

    it('should import other feed', async () => {
      const articles = await articleModel.all();
      expect(articles).toHaveLength(nbItemsByFeed);

      expect(uniq(articles.map((v) => v.idFeed))).toHaveLength(1);
    });
  });

  describe('without config', () => {
    beforeEach(async () => {
      await cleanTables([
        articleModel.config.table,
        notificationsModel.config.table,
        subscriptionsModel.config.table
      ]);

      await feedModel.saveAll(feeds);

      await subscriptionsModel.saveAll([
        // feed 1 both
        { idFeed: 1, idUser: 1 },
        { idFeed: 1, idUser: 2 },
        // feed 2 one
        { idFeed: 2, idUser: 2 }
        // feed 3 no subscriptions
      ]);

      jest
        .spyOn(utilsFeed, 'parseFeed')
        .mockImplementation(({ id }) =>
          Promise.resolve(articles.filter((v) => v.idFeed === id))
        );
    });

    afterEach(() => {
      utilsFeed.parseFeed.mockRestore();
    });

    it('should import all feeds', async () => {
      await command.action({});
      const result = await articleModel.all();
      expect(result).toHaveLength(nbItemsByFeed * 2);
    });

    it('should add notification to user', async () => {
      await command.action({});
      const result = await notificationsModel.all();
      // two for user 2 who follow two feeds
      // one for user 1
      expect(result).toHaveLength(nbItemsByFeed * 3);
    });
  });

  describe('with config filter', () => {
    const filter = 'index: 0';
    beforeEach(async () => {
      await cleanTables([
        articleModel.config.table,
        notificationsModel.config.table,
        subscriptionsModel.config.table
      ]);

      await feedModel.saveAll(feeds);

      await subscriptionsModel.saveAll([
        // feed 1 both
        { idFeed: 1, idUser: 1, filter },
        { idFeed: 1, idUser: 2 }
      ]);

      jest
        .spyOn(utilsFeed, 'parseFeed')
        .mockImplementation(({ id }) =>
          Promise.resolve(articles.filter((v) => v.idFeed === id))
        );
    });

    afterEach(() => {
      utilsFeed.parseFeed.mockRestore();
    });

    it('should import all feeds', async () => {
      await command.action({});
      const result = await articleModel.all();
      expect(result).toHaveLength(nbItemsByFeed);
    });

    it('should add notification to user', async () => {
      await command.action({});
      const allNotifs = await notificationsModel.allByProps({ idUser: 2 });
      expect(allNotifs).toHaveLength(nbItemsByFeed);

      const filtered = await notificationsModel.allByProps({ idUser: 1 });
      expect(filtered).toHaveLength(1);
    });

    it('should not be case sensitive', async () => {
      jest
        .spyOn(utilsFeed, 'parseFeed')
        .mockImplementation(({ id }) =>
          Promise.resolve(
            articles
              .filter((v) => v.idFeed === id)
              .map((v) => ({ ...v, title: v.title.toUpperCase() }))
          )
        );

      await command.action({});
      const allNotifs = await notificationsModel.allByProps({ idUser: 2 });
      expect(allNotifs).toHaveLength(nbItemsByFeed);

      const filtered = await notificationsModel.allByProps({ idUser: 1 });
      expect(filtered).toHaveLength(1);
    });
  });
});
