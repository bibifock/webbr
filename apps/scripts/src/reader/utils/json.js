/**
 * parse json without error
 * @param {string} src
 *
 * @return {object|null}
 */
export const parse = (src) => {
  try {
    return JSON.parse(src);
  } catch (e) {
    return null;
  }
};

/**
 * @param {string} src Json string
 * @return {boolean}
 */
export const isJSONString = (src) => parse(src) !== null;
