import fs from 'fs';

import { feedModel } from '@webbr/reader-db';

import log from './utils/console';


const action = async ({ file }) => {
  if (!file || !fs.existsSync(file)) {
    log.warn('missing "--file <json>" params');
    return;
  }

  const feeds = await feedModel.all();

  const obj = JSON.parse(fs.readFileSync(file, 'utf8'));
  const toUpdate = obj.map((feed) => {
    feed.config = JSON.stringify(feed.config);
    const item = feeds.find((o) => o.type === feed.type && o.url === feed.url);
    if (!item) {
      return feed;
    }
    return { ...item, ...feed };
  });

  await feedModel.saveAll(toUpdate);

  log.info(`${toUpdate.length} added in database`);
};

const usage = 'import:feeds';
const description = 'import feeds list';
const definition = (yargs) =>
  yargs.option('file', {
    alias: 'f',
    describe: 'json source file'
  });

export default {
  action,
  usage,
  definition,
  description
};
