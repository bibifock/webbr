import get from 'lodash/get';
import flatten from 'lodash/flatten';
import chunk from 'lodash/chunk';

import { feedModel, articleModel, notificationsModel } from '@webbr/reader-db';
import { parseFeed } from '@webbr/rss-parser';
import type { Parser, ParserItem } from '@webbr/rss-parser';

import log from '../console';

type Feed = {
  type: Parser;
  id: number;
  name: string;
  subs: Array<{ id: number; filter: string }>;
  config: string;
  url: string;
};

type Article = { url: string; title: string; id?: number };
type Notif = { idUser: number; url: string };

export const feedsSync = async ({ idFeed }: { idFeed?: number }) => {
  const feeds = (await feedModel.withSubscriptions(idFeed)) as Array<Feed>;

  if (!feeds.length) {
    log.info('nothing to import');
    return;
  }

  const results = await Promise.all(
    feeds.map(async (feed: Feed) => {
      const { subs } = feed;
      const userFeeds = subs.reduce<Record<string, number[]>>(
        (o, { id, filter }) => {
          if (!filter) {
            filter = '.';
          }

          o[filter] = [...get(o, filter, []), id];
          return o;
        },
        {}
      );

      const filters = Object.keys(userFeeds).map((raw) => ({
        raw,
        reg: new RegExp(raw, 'i')
      }));
      try {
        const items = await parseFeed(feed);

        type ResultItems = { articles: Article[]; notifs: Notif[] };
        const results = items.reduce<ResultItems>(
          (acc: ResultItems, item: ParserItem) => {
            const { url, title } = item;
            const keys = filters
              .filter(({ reg }) => reg.test(title))
              .map(({ raw }) => raw);

            if (!keys.length) {
              return acc;
            }

            return {
              articles: [...acc.articles, item],
              notifs: [
                ...acc.notifs,
                ...keys.flatMap((k: string) =>
                  userFeeds[k].map((idUser: number) => ({ idUser, url }))
                )
              ]
            };
          },
          {
            articles: [],
            notifs: []
          }
        );

        return results;
      } catch (e: unknown) {
        log.warn(
          `[PARSE FAILED: ${feed.id}. ${feed.name}]`,
          (e as Error).message
        );
        return {
          articles: [],
          notifs: []
        };
      }
    })
  );

  const articlesToSave = chunk(
    flatten(results.map((v) => v.articles)).filter((v) => v),
    20
  );
  let articles: Article[] = [];
  for (const items of articlesToSave) {
    const r = await articleModel.saveAndPopulate(items);
    articles = articles.concat(r);
  }

  const notifications = chunk(
    flatten(results.map((v) => v.notifs).filter((v) => v)).map(
      ({ url, ...rest }) => ({
        ...rest,
        idArticle: articles.find((a) => a.url === url)?.id
      })
    )
  );

  for (const items of notifications) {
    await notificationsModel.saveNew(items);
  }

  log.info(
    `${articles.length} new articles / ${notifications.length} users interested`
  );
};
