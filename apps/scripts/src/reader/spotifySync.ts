import {
  feedModel,
  articleModel,
  notificationsModel,
  userModel
} from '@webbr/reader-db';

import log from '../console';

import { spotifyApi } from './spotifyApi';

type Article = {
  id: number;
  idFeed: number;
  url: string;
  image: string;
  title: string;
  description: string;
  created: string;
  updated: string;
};

const getNewAlbums = async (
  user: {
    id: number;
    spotify: { accessToken: string; refreshToken: string };
  },
  idFeed: number
) => {
  const api = spotifyApi({
    tokens: user.spotify,
    onRefreshAccessToken: async (accessToken: string) => {
      userModel.saveSpotify(user.id, { ...user.spotify, accessToken });
    }
  });

  const artists = await api.getAllArtists();
  log.info(`${artists.length} artists founds`);
  const albums = await api.getNewAlbums(artists);

  if (albums.length === 0) {
    log.info('[spotifySync]', `no new release for user: ${user.id}`);
    return;
  }

  log.info(
    '[spotifySync]',
    `${albums.length} new release found for user: ${user.id}`
  );

  const items: Article[] = await articleModel.saveAndPopulate(
    albums.map((a) => ({ ...a, idFeed }))
  );

  await notificationsModel.saveNew(
    items.map((i) => ({ idArticle: i.id, idUser: user.id }))
  );
};

export const spotifySync = async () => {
  const users = await userModel.getSpotifyAccounts();

  const idFeed = await feedModel.getServiceId('spotify');

  for (const user of users) {
    await getNewAlbums(user, idFeed);
  }
};
