--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE users ADD spotify TEXT NULL;

INSERT INTO feeds (name, type, url, config)
VALUES ('spotify', 'service', 'https://open.spotify.com/', '');

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
ALTER TABLE users RENAME TO _users;

CREATE TABLE users (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email TEXT NOT NULL COLLATE NOCASE,
    name TEXT NULL COLLATE NOCASE,
    password TEXT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastConnection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users (id, email, password, created, updated, lastConnection)
  SELECT id, email, password, created, updated, lastConnection
  FROM _users;

DROP TABLE _users;

DELETE FROM feeds WHERE type='service';
