--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE users (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email TEXT NOT NULL COLLATE NOCASE,
    name TEXT NULL COLLATE NOCASE,
    password TEXT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastConnection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE feeds (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT NULL COLLATE NOCASE,
    type TEXT NOT NULL COLLATE NOCASE,
    url TEXT NOT NULL COLLATE NOCASE,
    config TEXT NULL COLLATE NOCASE
);

CREATE TABLE subscriptions (
    idUser INTEGER NOT NULL,
    idFeed INTEGER NOT NULL,
    filter TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (idUser, idFeed)
);

CREATE TABLE articles (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    idFeed INTEGER NOT NULL,
    url TEXT NOT NULL COLLATE NOCASE,
    image TEXT NOT NULL COLLATE NOCASE,
    title TEXT NOT NULL COLLATE NOCASE,
    description TEXT NOT NULL COLLATE NOCASE,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAUlT CURRENT_TIMESTAMP
);

CREATE TABLE notifications (
    idUser INTEGER NOT NULL,
    idArticle INTEGER NOT NULL,
    unread INTEGER DEFAULT 1,
    PRIMARY KEY (idUser, idArticle)
);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE users;
DROP TABLE feeds;
DROP TABLE subscriptions;
DROP TABLE articles;
DROP TABLE notifications;
