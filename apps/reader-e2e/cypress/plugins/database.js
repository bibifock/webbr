import SQL from 'sql-template-strings';
import bcrypt from 'bcryptjs';

import readerDb from '@webbr/reader-db';

const { database } = readerDb;

/**
 * transform array of items.
 * [1, 2, 3] => SQL'${1}, ${2}, ${3}';
 *
 * @param {array} items
 * @param {func} each function call for each elements (by default add value)
 * @param {func} concat function call for each join action (by default add ', ')
 *
 * @return object sql template string
 */
const concatValues = (
  items,
  each = (item) => SQL`${item}`,
  concat = (index) => (index > 0 ? ', ' : '')
) => {
  const query = SQL``;
  items.forEach((item, index) => query.append(concat(index)).append(each(item)));
  return query;
};

const tableFields = {
  users: ['id', 'email', 'name', 'password'],
  feeds: ['id', 'name', 'type', 'url', 'config'],
  subscriptions: ['idUser', 'idFeed', 'filter'],
  articles: ['id', 'idFeed', 'url', 'image', 'title', 'description'],
  notifications: ['idUser', 'idArticle', 'unread']
};

const reset = async () => {
  const db = await database.connect();

  return Promise.all(
    Object.keys(tableFields).map((table) => db.run(`DELETE FROM ${table} WHERE 1=1`))
  );
};

const addUsers = async (users) => {
  const db = await database.connect();

  users = await Promise.all(
    users.map(async (user) => {
      if (user.password) {
        user.password = await bcrypt.hash(user.password, 10);
      }

      return user;
    })
  );

  const query = SQL`
    INSERT INTO users (id, email, password, name) VALUES
  `.append(
    concatValues(
      users,
      (user) => SQL`(${user.id}, ${user.email}, ${user.password}, ${user.name})`,
      (i) => (i > 0 ? ',' : '')
    )
  );

  return db.run(query);
};

const fillTable = ({ fields, table, data }) => {
  const model = database.model.init({
    fields: fields || tableFields[table],
    table
  });

  return model.saveAll(data);
};

const run = async (queries) => {
  const db = await database.connect();

  return Promise.all(queries.map((query) => db.run(query)));
};

export default { reset, addUsers, fillTable, run };
