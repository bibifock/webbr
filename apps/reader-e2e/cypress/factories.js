import Faker from 'faker/locale/fr';

export const faker = Faker;

const user = (opts = {}) => ({
  email: faker.internet.email(),
  name: faker.person.name(),
  password: faker.lorem.words(),
  ...opts
});

const article = (opts = {}) => ({
  idFeed: faker.datatype.number(),
  url: faker.internet.url(),
  image: faker.image.imageUrl(),
  title: faker.lorem.sentence(),
  description: faker.lorem.paragraphs(),
  ...opts
});

const feed = (opts = {}) => ({
  name: faker.lorem.words(),
  type: 'youtube-channel',
  url: faker.internet.url(),
  ...opts
});

export default {
  user,
  article,
  feed
};
