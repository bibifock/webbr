import { URL_LOGIN } from '../../constants';

describe('login', () => {
  let userFix;
  beforeEach(() => {
    cy.fixture('user').then((user) => {
      userFix = user;
    });

    cy.loadPlaybook();
    cy.visit(URL_LOGIN);

    cy.waitUntilCyIdFound('form-login');
    cy.wait(500);
  });

  it('default render, with login form', () => {
    cy.getCyId('form-login').should('exist');
  });

  describe('login form', () => {
    it('button should be disabled', () => {
      cy.get('button').should('be.disabled');
    });

    it('button should be enable, when form filled', () => {
      cy.get('input[name="login"]').type('login');
      cy.get('input[name="password"]').type('password');

      cy.get('button').should('not.be.disabled');
    });

    describe('login action', () => {
      describe('should failed when', () => {
        // eslint-disable-next-line mocha/no-setup-in-describe
        [
          ['bad name', { login: 'admin 1' }],
          ['bad email', { login: 'failed@mail.com' }],
          ['bad password', { password: '213' }]
        ].forEach(([title, args]) => {
          it(title, () => {
            cy.visit(URL_LOGIN);

            const userAuth = { ...userFix, ...args };

            cy.get('input[type="text"]').type(userAuth.login || userAuth.email);
            cy.get('input[type="password"]').type(userAuth.password);

            cy.get('button').click();

            cy.waitUntilCyIdFound('errors');

            cy.getCyId('errors').should('exist').contains('not possible');
          });
        });
      });

      describe('should work fine when connected', () => {
        it('should display user infos', () => {
          cy.login();

          cy.waitUntilCyIdFound('page-form-articles');
        });
      });
    });
  });
});
