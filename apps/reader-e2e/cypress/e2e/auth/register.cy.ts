import { URL_REGISTER, URL_LOGIN, URL_USER_INDEX } from '../../constants';

describe('register', () => {
  let user;
  beforeEach(() => {
    cy.fixture('user').then((result) => {
      user = result;
    });
    cy.loadPlaybook();

    cy.visit(URL_REGISTER);
    cy.wait(500);
  });

  const registerIt = (args = {}) => {
    const { name, email, password } = { ...user, ...args };
    cy.get('input[type=text]').type(name);
    cy.get('input[type=email]').type(email);
    cy.get('input[type=password]').type(password);

    cy.get('button').click();
  };

  it('should render register form', () => {
    cy.get('[data-cy="form-register"]').should('exist');
  });

  describe('should not working when', () => {
    beforeEach(() => {
      cy.task('db:reset');
      cy.task('db:addUsers', [user]);
    });

    it('user email not present in database', () => {
      registerIt({ email: 'not-existing@mail.co' });

      cy.waitUntilCyIdFound('errors');

      cy.getCyId('errors').contains('not found');
    });

    it('user already have a password', () => {
      registerIt(user);

      cy.get('[data-cy=errors]').should('exist').contains('not possible');
    });

    it('user exist but username already exists', () => {
      const newUser = { email: 'new-user@gmail.com' };
      cy.task('db:addUsers', [newUser]);
      registerIt({ ...newUser, name: user.name });

      cy.get('[data-cy=errors]').should('exist').contains('already taken');
    });
  });

  describe('on form success', () => {
    const newUser = {
      email: 'new-user@mail.com',
      name: 'new-user',
      newPass: '12345678901'
    };

    beforeEach(() => {
      cy.task('db:addUsers', [newUser]);

      registerIt({ ...newUser, password: newUser.newPass });
    });

    it('should redirect user to login page', () => {
      cy.url().should('include', URL_LOGIN);
    });

    it('user can logged', () => {
      cy.get('[data-cy="form-login"]').should('exist');
    });

    it('user should be active', () => {
      cy.login({ ...newUser, password: newUser.newPass });
      cy.waitUntilCyIdFound('page-form-articles');

      cy.visit(URL_USER_INDEX);
      cy.waitUntilCyIdFound('form-me');

      cy.get('input[name=email]').should('have.value', newUser.email);
      cy.get('input[name=name]').should('have.value', newUser.name);
    });
  });
});
