import { URL_USER_INDEX, URL_LOGIN } from '../../constants';

describe('me', () => {
  let userFix;
  beforeEach(() => {
    cy.fixture('user').then((user) => {
      userFix = user;
    });
    cy.loadPlaybook();

    cy.clearCookies();
    cy.wait(500);
    cy.login();

    cy.visit(URL_USER_INDEX);
    cy.wait(500);

    cy.waitUntilCyIdFound('form-me');
  });

  it('should display user information', () => {
    cy.get('input[name=email]').should('have.value', userFix.email);
    cy.get('input[name=name]').should('have.value', userFix.name);
  });

  it('on logout, user should be redirect to login page', () => {
    cy.get('button').click();
    cy.wait(500);

    cy.url().should('include', URL_LOGIN);

    cy.visit(URL_USER_INDEX);
    cy.wait(500);

    cy.url().should('include', URL_LOGIN);
  });
});
