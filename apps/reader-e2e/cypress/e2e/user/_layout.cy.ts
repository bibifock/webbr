import { URL_USER_INDEX, URL_USER_FEEDS, URL_USER_ARTICLES } from '../../constants';

const urls = [URL_USER_INDEX, URL_USER_FEEDS, URL_USER_ARTICLES];

describe('protected routes', () => {
  beforeEach(() => {
    cy.loadPlaybook();
  });

  describe('when not logged should be redirect on login', () => {
    // eslint-disable-next-line mocha/no-setup-in-describe
    urls.forEach((url) => {
      it(url, () => {
        cy.visit(url);

        cy.waitUntilCyIdFound('form-login');
      });
    });
  });
});
