import { URL_USER_ARTICLES } from '../../constants';

import articlesList from '../../fixtures/articles.json';
import feedsList from '../../fixtures/feeds.json';
import user from '../../fixtures/user.json';

const { feeds, subscriptions } = feedsList.slice(0, 2).reduce(
  (o, { config, ...rest }, index) => {
    rest.id = index + 1;
    o.feeds.push(rest);
    o.subscriptions.push({
      config: JSON.stringify(config),
      idUser: user.id,
      idFeed: rest.id
    });

    return o;
  },
  { feeds: [], subscriptions: [] }
);

const { articles, notifications } = articlesList.reduce(
  (o, item, index) => {
    const id = index + 1;
    o.articles.push({
      ...item,
      id,
      idFeed: feeds[id % 2].id
    });

    if (id === articlesList.length) {
      return o;
    }

    o.notifications.push({
      idArticle: id,
      idUser: user.id,
      unread: 1
    });

    return o;
  },
  { articles: [], notifications: [] }
);

const playbook = {
  subscriptions,
  feeds,
  articles,
  notifications
};

const cyForm = 'page-form-articles';
const cyListItem = 'list-item-article';
const cyListItemDetail = 'list-item-article_detail';
const cyFormActionSeeMore = 'list-item_see-more';
const cyFormActionRead = 'list-item-article_actions_read';
const cyFormActionUnread = 'list-item-article_actions_unread';

const visitIt = () => {
  cy.visit(URL_USER_ARTICLES);

  cy.waitUntilCyIdFound(cyForm);
};

describe('articles', () => {
  beforeEach(() => {
    cy.loadPlaybook(playbook);
    cy.login();
  });

  describe('list render', () => {
    beforeEach(() => {
      visitIt();
    });

    it('should only display followed feeds', () => {
      cy.getCyId(cyListItem).should('exist').should('have.length', playbook.notifications.length);
    });

    it('should only display one form at a time', () => {
      cy.getCyId(cyFormActionSeeMore).first().click();

      cy.getCyId(cyListItemDetail, ':visible').should('have.length', 1);

      cy.getCyId(cyFormActionSeeMore).last().click();

      cy.getCyId(cyListItemDetail, ':visible').should('have.length', 1);
    });
  });

  describe('actions read', () => {
    beforeEach(() => {
      cy.loadPlaybook(playbook);
      visitIt();

      cy.getCyId(cyFormActionRead).first().click();

      cy.waitUntilCyIdFound(cyFormActionUnread);
    });

    it('should store it in db', () => {
      visitIt();

      cy.getCyId(cyListItem).should('have.length', notifications.length - 1);
    });

    it('unread action should work', () => {
      cy.getCyId(cyFormActionUnread).click();

      cy.waitUntilCyIdFound(cyListItem, `:first [data-cy="${cyFormActionRead}"]`);

      cy.getCyId(cyFormActionRead).first().click();
    });

    it('should rollback value in database when on read', () => {
      cy.getCyId(cyFormActionUnread).click();

      cy.waitUntilCyIdFound(cyListItem, `:first [data-cy="${cyFormActionRead}"]`);

      visitIt();

      cy.getCyId(cyListItem).should('have.length', notifications.length);
    });
  });
});
