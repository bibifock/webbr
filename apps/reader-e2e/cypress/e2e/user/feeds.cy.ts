import { URL_USER_FEEDS } from '../../constants';
import factories from '../../factories';

import feedsList from '../../fixtures/feeds.json';
import user from '../../fixtures/user.json';

feedsList.sort((a, b) => {
  if (a.name > b.name) return 1;
  if (a.name < b.name) return -1;
  return 0;
});

const { feeds, subscriptions } = feedsList.slice(0, -1).reduce(
  (o, { filter, config, ...rest }, index) => {
    rest.id = index + 1;
    o.feeds.push({ ...rest, config: JSON.stringify(config) });
    if (rest.id === feedsList.length - 1) {
      return o;
    }

    o.subscriptions.push({
      filter,
      idUser: user.id,
      idFeed: rest.id
    });

    return o;
  },
  { feeds: [], subscriptions: [] }
);

const playbook = {
  subscriptions,
  feeds
};

const cyForm = 'page-form-feeds';
const cyListItem = 'list-item-feed';
const cyListItemForm = 'list-item-feed-form';
const cyListItemArticle = 'list-item-article';
const cyFormActionSave = 'list-item-feed_actions-save';
const cyFormActionCancel = 'list-item-feed_actions-cancel';

const getDisplayForm = (id = cyListItemForm) => cy.getCyId(id, ':visible');
const getFormInput = (name, type = 'input') => getDisplayForm().find(`${type}[name="${name}"]`);

const checkFeedDisplay = (feed) => {
  getDisplayForm().should('have.length', 1);
  getFormInput('name').should('have.value', feed.name);
  getFormInput('url').should('have.value', feed.url);
  getFormInput('filter').should('have.value', feed.filter);
  getFormInput('config', 'textarea').should(
    'have.value',
    feed.config ? JSON.stringify(feed.config) : ''
  );
  getFormInput('type', 'select').should('have.value', feed.type);
};

const fillForm = ({ url, filter, name, type, config }) => {
  getFormInput('name').clear().type(name);
  getFormInput('url').clear().type(url);
  getFormInput('type', 'select').select(type);
  getFormInput('filter').clear().type(filter);
  if (config) {
    getFormInput('config', 'textarea')
      .clear()
      .type(JSON.stringify(config), { parseSpecialCharSequences: false });
  }
};

const visitIt = () => {
  cy.visit(URL_USER_FEEDS);
  cy.wait(500);

  cy.waitUntilCyIdFound(cyForm);
};

describe('feeds', () => {
  beforeEach(() => {
    cy.loadPlaybook(playbook);
    cy.login();
  });

  describe('list render', () => {
    beforeEach(() => {
      visitIt();
      cy.wait(500);
    });

    it('should only display followed feeds', () => {
      cy.getCyId(cyListItem).should('exist').should('have.length', playbook.feeds.length);
    });

    it('should only display one form at a time', () => {
      cy.getCyId(cyListItem).first().click();

      getFormInput('name').should('have.value', playbook.feeds[0].name);
      getFormInput('filter').should('have.value', subscriptions[0].filter);

      cy.getCyId(cyListItem).last().click();

      getFormInput('name').should('have.value', playbook.feeds.pop().name);
    });
  });

  describe('add a feed', () => {
    beforeEach(() => {
      cy.loadPlaybook(playbook);

      visitIt();
      cy.wait(500);

      cy.getCyId(cyListItem).should('have.length', playbook.feeds.length);

      cy.getCyId(cyForm, 'button').last().click();

      cy.wait(500);

      cy.waitUntilCyIdFound(cyListItemForm);
    });

    it('should display form', () => {
      getDisplayForm().should('exist');
      getFormInput('name').should('have.value', '');
    });

    it('should disabled add button', () => {
      getDisplayForm(cyForm).find('button:disabled').should('exist');
    });

    describe('preview', () => {
      it('by default preview button is disabled', () => {
        getDisplayForm().find('button:disabled').should('contain', 'preview');
      });

      describe('when url and type filled', () => {
        beforeEach(() => {
          cy.wait(500);
          fillForm(feedsList[0]);
          getFormInput('config', 'textarea').clear();
          getFormInput('filter').clear();
          cy.wait(500);
        });

        it('when url and type selected is enabled', () => {
          getDisplayForm().find('button:enabled').should('contain', 'preview');
        });

        it('preview click should display feed preview', () => {
          getDisplayForm().find('button:enabled').click();

          cy.waitUntilCyIdFound(cyListItemArticle);
        });
      });
    });

    it('on cancel should remove new form', () => {
      cy.getCyId(cyFormActionCancel).click();

      cy.getCyId(cyListItem).should('have.length', playbook.feeds.length);
    });

    it('save button should be enabled when ready', () => {
      cy.getCyId('list-item-feed_actions-save').should('be.disabled');
      fillForm(feedsList[0]);
      cy.getCyId('list-item-feed_actions-save').should('not.be.disabled');
    });

    it('on save, should display error when config is not a json', () => {
      fillForm(feedsList[feedsList.length - 2]);
      getFormInput('config', 'textarea').type('----');
      cy.wait(500);

      cy.get('[data-cy="list-item-feed_actions-save"]:enabled').click();
      cy.wait(500);

      cy.waitUntilCyIdFound('errors');

      cy.getCyId('errors').should('contain', 'json config');
    });

    it('on save, should display error message when feeds already exist', () => {
      fillForm(feedsList[0]);
      cy.get('[data-cy="list-item-feed_actions-save"]:enabled').click();

      cy.waitUntilCyIdFound('errors');
    });

    it.only('on save, should add feed when exist', () => {
      const feed = feedsList[1];
      fillForm(feed);
      cy.get('[data-cy="list-item-feed_actions-save"]:enabled').click();

      cy.waitUntilCyIdFound('list-item_see-more');

      cy.getCyId(cyListItem).first().click();

      checkFeedDisplay(feed);
    });

    it('on save, should save data when feed exist but not follow', () => {
      const feed = feedsList[feedsList.length - 1];
      fillForm(feed);
      cy.get('[data-cy="list-item-feed_actions-save"]:enabled').click();

      cy.waitUntilCyIdFound('list-item_see-more');

      cy.getCyId(cyListItem).first().click();

      checkFeedDisplay(feed);
    });
  });

  describe('update feed', () => {
    let newFeed;
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    beforeEach(() => {
      cy.loadPlaybook(playbook);
      visitIt();

      cy.getCyId(cyListItem).first().click();

      newFeed = factories.feed({
        name: '000_New_Feed',
        filter: 'new',
        config: { test: true }
      });

      fillForm(newFeed);
    });

    it('should display error when config is not a json', () => {
      getFormInput('config', 'textarea').type('----');

      cy.getCyId(cyFormActionSave).click();

      cy.waitUntilCyIdFound('errors');

      cy.getCyId('errors').should('contain', 'json config');
    });

    it('should store new field content', () => {
      cy.getCyId(cyFormActionSave).click();
      cy.wait(500);

      cy.getCyId('errors').should('not.exist');

      visitIt();
      cy.wait(500);

      cy.getCyId(cyListItem).first().click();

      checkFeedDisplay(newFeed);
    });
  });
});
