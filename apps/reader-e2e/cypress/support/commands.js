/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }
//

import 'cypress-wait-until';

import { URL_LOGIN } from '../constants';

Cypress.Commands.add('getCyId', (id, selector = '') => {
  id = `[data-cy="${id}"]`;
  if (selector) {
    id += (/^:/.test(selector) ? '' : ' ') + selector;
  }

  return cy.get(id);
});

Cypress.Commands.add('waitUntilCyIdFound', (id, selector = '', opts = {}) =>
  cy.waitUntil(() => cy.getCyId(id, selector, opts).then((el) => el.length > 0), opts)
);

const login = (user, withName = false) => {
  cy.wait(500);
  cy.get('input[type="text"]').type(withName ? user.name : user.email);
  cy.get('input[type="password"]').type(user.password);

  cy.get('button').click();

  cy.waitUntilCyIdFound('page-form-articles');
};

Cypress.Commands.add('login', (user, withName = false) => {
  cy.visit(URL_LOGIN);

  if (user) {
    return login(user, withName);
  }

  cy.fixture('user').then((u) => login(u, withName));

  cy.waitUntilCyIdFound('page-form-articles');
});

Cypress.Commands.add('loginPersist', (user) => {
  cy.login(user);

  Cypress.Cookies.preserveOnce('connect.sid');
});

Cypress.Commands.add('loadPlaybook', (book) => {
  cy.task('db:reset');
  cy.fixture('user').then((user) => cy.task('db:addUsers', [user]));

  if (!book) {
    return;
  }

  Object.keys(book).map((table) => cy.task('db:fill', { table, data: book[table] }));
});
