import { defineConfig } from 'cypress';
import { deleteSync } from 'del';

import database from './cypress/plugins/database';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:4173/',
    setupNodeEvents(on) {
      // implement node event listeners here
      on('task', {
        'db:reset': database.reset,
        'db:addUsers': database.addUsers,
        'db:fill': database.fillTable,
        'db:run': database.run
      });

      on('after:spec', async (spec, results) => {
        if (results && results.stats.failures === 0 && results.video) {
          // `del()` returns a promise, so it's important to return it to ensure
          // deleting the video is finished before moving on
          await deleteSync(results.video);
        }
      });
    }
  }
});
