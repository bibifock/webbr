import getStory from '$lib/utils/storybook/getStory';

import Component from './YoutubePreview.svelte';

const story = getStory({
  title: 'molecules/youtube-preview',
  Component,
  argTypes: {
    link: { control: 'text' }
  },
  args: {
    link: 'https://www.youtube.com/watch?v=esfJwdSl_7o'
  }
});

export default story.default;

export const StandardLink = story.bind();

export const EmbedLink = story.bindWithArgs({
  link: 'https://www.youtube.com/embed/esfJwdSl_7o'
});

export const Playlist = story.bindWithArgs({
  link: 'https://www.youtube.com/playlist?list=PLibS5rbMkb5QHUnOUA_12lidK5Xx6-tyJ'
});

export const InvalidLink = story.bindWithArgs({
  link: 'http://www.perdu.com/'
});
