import getStory from '$lib/utils/storybook/getStory';
import { nameIconType } from '$lib/ui/atoms/icon/stories.utils';

import Component from './ListItem';

const story = getStory({
  Component,
  title: 'molecules/ListItem',
  argTypes: {
    title: { control: 'text' },
    url: { control: 'text' },
    icon: nameIconType,
    source: { control: 'text' },
    open: { control: 'boolean' },
    hideDetail: { control: 'boolean' },
    onClick: { actions: 'onClick' },
    onSeeMore: { actions: 'onSeeMore' }
  },
  args: {
    title: 'title',
    url: 'https://pinute.org',
    icon: 'youtube',
    source: {
      name: 'source name',
      url: 'https://uku.pinute.org'
    }
  }
});

export default story.default;

export const base = story.bind();

export const inactive = story.bindWithArgs({ inactive: true });

export const open = story.bindWithArgs({ open: true });
