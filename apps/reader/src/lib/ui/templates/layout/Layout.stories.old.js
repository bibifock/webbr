import getStory from '$lib/utils/storybook/getStory';
import Container, { propParser } from '$lib/utils/storybook/StoryContainer';

import Component from './Layout';

const story = getStory({
  Component: Container,
  title: 'templates/Layout',
  parser: {
    props: propParser(Component)
  }
});

export default story.default;

export const base = story.bind({
  parameters: {
    layout: 'padded'
  }
});
