export type Dispatcher<TEvents extends Record<keyof TEvents, CustomEvent<unknown>>> = {
  [Property in keyof TEvents]: TEvents[Property]['detail'];
};
