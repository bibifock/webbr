export const defaultArgs = {
  image: 'https://i2.ytimg.com/vi/mEF7nYU4b_k/hqdefault.jpg',
  feed: {
    type: 'youtube-channel',
    name: 'RT France',
    url: 'http://perdu.com'
  },
  title: "C'EST CASH ! - Licenciements, chômage : est-ce vraiment la faute du Covid ?",
  description: `
      Selon une étude Trendeo pour Le Monde, 35 664 destructions de postes auraient été annoncées depuis le mois de septembre. Mediapart parle de près de 75 000 emplois condamnés ou menacés dans 317 entreprises. Et si certaines régions comme l’Ile-de-France ou l’Auvergne-Rhône-Alpes sont plus lourdement touchées, c’est le pays tout entier qui est concerné par ces vagues de licenciements. Pourtant, après un premier, puis un deuxième confinement avec les retombées économiques que l’on connaît, l’Etat avait semble-t-il pris la mesure du problème en annonçant des plans d’aides massifs aux entreprises afin de les aider à surmonter la crise tout en préservant l’emploi de ses salariés. Mais au vu du nombre important de plans sociaux annoncés, on est en droit de se demander à quoi ces aides ont-elles servi. Le coronavirus ne pourrait-il finalement être qu’un prétexte pour mettre en œuvre des plans de restructurations pensés bien en amont au sein des entreprises ? La crise sanitaire est-elle seule responsable ou est-ce que certaines entreprises profitent d’un effet d’aubaine pour licencier massivement ?
      C’est à ce genre de questions que nous allons essayer de répondre dans ce nouvel épisode, et pour en parler C’est Cash reçoit Laurent Mauduit, écrivain, journaliste cofondateur de Mediapart, spécialisé dans les affaires économiques politiques et sociales.

      Abonnez-vous à la chaîne YouTube de RT France : https://www.youtube.com/rtenfrancais

      RT en français : http://rtfrance.tv
      Facebook : https://www.facebook.com/RTFrance
      Twitter : https://twitter.com/rtenfrancais
      Instagram : https://www.instagram.com/rtfrance/
    `,
  url: 'https://www.youtube.com/watch?v=mEF7nYU4b_k',
  created: 1607450415000,
  unread: true
};
