import getStory from '$lib/utils/storybook/getStory';

import { defaultArgs } from './stories.utils';
import Component from './Article';

const story = getStory({
  Component,
  title: 'organisms/Article',
  argTypes: {
    image: { control: 'text' },
    title: { control: 'text' },
    feed: { control: 'text' },
    description: { control: 'text' },
    url: { control: 'text' },
    created: { control: 'text' },
    unread: { control: 'boolean' },
    open: { control: 'boolean' },
    onSeeMore: { actions: 'onSeeMore' },
    onRead: { actions: 'onRead' }
  },
  parameters: {
    layout: 'padded'
  },
  args: defaultArgs
});

export default story.default;

export const base = story.bind();

export const read = story.bindWithArgs({ unread: false });
export const open = story.bindWithArgs({ open: true });

export const youtube = story.bindWithArgs({
  open: true
});
