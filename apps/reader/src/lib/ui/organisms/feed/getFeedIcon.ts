import type { Feed } from '$lib/types';

export const getFeedIcon = ({ type, name }: Pick<Feed, 'type' | 'name'>): string =>
  // prettier-ignore
  type === 'service' ? name
  : type === 'youtube-channel' ? 'youtube'
  : 'rss';
