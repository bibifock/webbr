import Feed from './Feed.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Feed> = {
  component: Feed,
  title: 'organisms/Feed',
  args: {
    icon: 'youtube',
    name: 'RT France',
    url: 'http://pinute.org',
    config: JSON.stringify({ color: 'red' }),
    follow: false,
    filter: '(test)',
    type: 'youtube-channel',
    types: ['youtube-channel', 'DBM'].map((label) => ({ label, value: label }))
  }
};

export default meta;

type Story = StoryObj<Feed>;

export const Basic: Story = {};

export const FormError: Story = {
  args: {
    errors: 'C’est excessivement énervant !',
    edit: true
  }
};

export const WithPreview: Story = {
  args: {
    edit: true,
    articles: Array.from(Array(12)).map((_, i) => ({
      feed: {
        name: 'RT France',
        type: 'youtube-channel'
      },
      icon: 'youtube',
      title: `title ${i}`,
      url: `https://pinute.org/${i}`,
      created: 1607450415000
    }))
  }
};
