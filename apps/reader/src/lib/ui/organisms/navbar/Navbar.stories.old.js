import getStory from '$lib/utils/storybook/getStory';

import Component from './Navbar';

const story = getStory({
  Component,
  title: 'organisms/Navbar'
});

export default story.default;

export const base = story.bindWithArgs({
  links: [
    {
      href: '#',
      icon: 'contact',
      label: 'username',
      reverse: true
    }
  ]
});
