import { icons } from './Icon';

const options = Object.keys(icons);

export const nameIconType = {
  defaultValue: options[0],
  control: {
    type: 'select',
    options
  }
};

export const sizeType = {
  description: 'props to add to component for change size',
  defaultValue: 'small',
  control: {
    type: 'inline-radio',
    options: ['small', 'medium', 'large']
  }
};

export const sizeParser = ({ size, ...props }) => {
  if (size !== 'small') {
    props[size] = true;
  }
  return props;
};
