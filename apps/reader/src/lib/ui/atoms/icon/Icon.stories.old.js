import getStory from '$lib/utils/storybook/getStory';

import { nameIconType, sizeType, sizeParser } from './stories.utils';
import IconsList from './IconStories';
import Component from './Icon';

const story = getStory({
  title: 'atoms/icon',
  Component,
  argTypes: {
    name: nameIconType,
    size: sizeType,
    noHover: {
      control: 'boolean',
      description: 'remove hover effect'
    }
  },
  parser: {
    props: sizeParser
  }
});

export default story.default;

export const Base = story.bind();

export const NotExist = story.bind({
  args: {
    name: 'icon name that not existing',
    large: true
  }
});

// extra example

const List = getStory({
  Component: IconsList
});

export const All = List.bind({
  layout: 'padded',
  controls: { hideNoControlsWarning: true }
});
