import getStory from '$lib/utils/storybook/getStory';
import Container, { propParser } from '$lib/utils/storybook/StoryContainer';

import Button from './Button';
import ButtonStories, { colors as options } from './ButtonStories';

const story = getStory({
  Component: Container,
  title: 'atoms/Button',
  argTypes: {
    color: {
      defaultValue: 'primary',
      control: {
        options,
        type: 'select'
      }
    },
    loading: { control: 'boolean' },
    outline: { control: 'boolean' },
    disabled: { control: 'boolean' }
  },
  parser: {
    props: propParser(Button, { content: 'string' })
  }
});

export default story.default;

export const base = story.bind();

export const loading = story.bindWithArgs({ loading: true });

const otherStory = getStory({
  Component: ButtonStories,
  argTypes: {
    loading: { control: 'boolean' },
    outline: { control: 'boolean' },
    disabled: { control: 'boolean' }
  }
});

export const all = otherStory.bind();
