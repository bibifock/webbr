import getStory from '$lib/utils/storybook/getStory';
import Container, { propParser } from '$lib/utils/storybook/StoryContainer';

import Component from './Link';

const story = getStory({
  Component: Container,
  title: 'atoms/Link',
  argTypes: {
    href: { control: 'text' },
    target: { control: 'text' },
    blank: { control: 'boolean' }
  },
  args: {
    href: 'https://www.pinute.org',
    blank: true
  },
  parser: {
    props: propParser(Component, { content: 'string' })
  }
});

export default story.default;

export const base = story.bind();

export const withoutHref = story.bindWithArgs({ href: undefined });
