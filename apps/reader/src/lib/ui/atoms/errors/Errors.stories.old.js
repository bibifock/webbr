import getStory from '$lib/utils/storybook/getStory';

import Component from './Errors.svelte';

const story = getStory({
  Component,
  title: 'atoms/errors',
  argTypes: {
    errors: {
      description: '{ key: "html content", key2: "html content" }',
      control: 'object'
    }
  }
});

export default story.default;

export const withObject = story.bindWithArgs({
  errors: {
    'field-name': 'one error',
    system: 'and another <strong>one</strong>'
  }
});

export const withString = story.bindWithArgs({
  errors: 'this is my error'
});

export const withArray = story.bindWithArgs({
  errors: ['error 1', 'error 2', 'error 3']
});
