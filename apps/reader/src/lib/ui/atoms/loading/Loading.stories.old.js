import getStory from '$lib/utils/storybook/getStory';

import Component from './Loading.svelte';

const story = getStory({
  title: 'atoms/loading',
  Component,
  argTypes: {
    color: {
      description: 'bootstrap color name',
      defaultValue: 'secondary',
      control: {
        type: 'select',
        options: [
          'danger',
          'dark',
          'info',
          'light',
          'muted',
          'primary',
          'secondary',
          'success',
          'warning',
          'white'
        ]
      }
    },
    small: { control: 'boolean' }
  }
});

export default story.default;

export const base = story.bind();
