import getStory from '$lib/utils/storybook/getStory';

import Component from './BaseFieldStories';

const story = getStory({
  Component,
  title: 'atoms/fields/Base',
  argTypes: {
    label: { control: 'text' },
    error: { control: 'text' }
  }
});

export default story.default;

export const Base = story.bindWithArgs({ label: 'label' });

export const Error = story.bindWithArgs({
  ...Base.args,
  error: 'my error'
});
