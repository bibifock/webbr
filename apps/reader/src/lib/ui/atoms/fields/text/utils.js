export function getClass({ small, error, right }) {
  let classes = ['form-control'];

  if (small) {
    classes.push('form-control-sm');
  }

  if (error) {
    classes.push('is-invalid');
  }

  if (right) {
    classes.push('text-right');
  }

  return classes.join(' ');
}
