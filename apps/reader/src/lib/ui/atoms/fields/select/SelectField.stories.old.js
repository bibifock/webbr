import getStory from '$lib/utils/storybook/getStory';

import Component from './SelectField';

const story = getStory({
  title: 'atoms/fields/Select',
  Component,
  argTypes: {
    label: { control: 'text' },
    value: { control: 'text' },
    items: {
      description: '[{ label, id }]'
    },
    error: { control: 'text' }
  }
});

export default story.default;

export const Base = story.bindWithArgs({
  label: 'label',
  value: 1,
  items: [1, 2, 3, 4].map((id) => ({ label: `value ${id}`, id }))
});

export const Error = story.bindWithArgs({
  ...Base.args,
  error: 'error message'
});
