import getStory from '$lib/utils/storybook/getStory';
import { colorType } from '$lib/utils/storybook/argTypes';
import { nameIconType, sizeParser, sizeType } from '$lib/ui/atoms/icon/stories.utils';

import Component from './IconButton';

const story = getStory({
  Component,
  title: 'atoms/IconButton',
  argTypes: {
    color: colorType,
    name: nameIconType,
    size: sizeType
  },
  parser: {
    props: sizeParser
  }
});

export default story.default;

export const base = story.bind();
