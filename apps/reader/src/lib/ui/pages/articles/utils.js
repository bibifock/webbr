import get from 'lodash/get.js';

export function searchInProps(obj, search, keys) {
  search = search.toLowerCase();
  for (const key of keys) {
    const value = get(obj, key, '').toLowerCase();

    if (value.indexOf(search) > -1) {
      return true;
    }
  }

  return false;
}
