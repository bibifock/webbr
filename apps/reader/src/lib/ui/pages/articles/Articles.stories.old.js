import getStory from '$lib/utils/storybook/getStory';
import { defaultArgs as article } from '$lib/ui/organisms/article/stories.utils';

import Component from './Articles';

const story = getStory({
  Component,
  title: 'pages/Articles',
  argTypes: {
    articles: {
      description: '[Article](?path=/story/organisms-article)',
      control: 'text'
    },
    loading: { control: 'boolean' },
    onRead: { actions: 'onRead' }
  },
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const base = story.bindWithArgs({
  articles: Array.from(Array(10)).map((_, id) => ({
    ...article,
    title: `${id % 2 ? 'key' : 'clef'} ${id} | ` + article.title,
    id,
    unread: id % 2
  }))
});
