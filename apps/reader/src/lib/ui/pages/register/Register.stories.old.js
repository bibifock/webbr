import getStory from '$lib/utils/storybook/getStory';

import Component from './Register';

const story = getStory({
  Component,
  title: 'pages/Register',
  argTypes: {
    errors: { control: 'array' },
    password: { control: 'text' },
    email: { control: 'text' },
    name: { control: 'text' },
    onSubmit: { action: 'onSubmit' },
    loading: { control: 'boolean' }
  }
});

export default story.default;

export const base = story.bind();

export const filled = story.bindWithArgs({
  password: 'password',
  email: 'admin@mail.com',
  name: 'jean-michel'
});
