import Component from './Feeds.svelte';

export default {
  Component,
  title: 'pages/Feeds',
  argTypes: {
    types: { control: 'text ' },
    feeds: { control: 'text' },
    onSave: { action: 'saved' },
    onPreview: { action: 'previewed' },
    onFollow: { action: 'follow' },
    onSpotifyConnect: { action: 'spotifyConnect' }
  },
  args: {
    types: ['DBM', 'youtube-channel'].map((value) => ({ value, label: value }))
  }
};

export const Base = {
  render: () => ({
    Component,
    props: {
      feeds: Array.from(Array(6)).map((_, id) => ({
        id: id + 1,
        name: 'RT France',
        url: 'http://pinute.org',
        source: 'http://pinute.org/rss.xml',
        config: JSON.stringify({ filter: '(test)' }),
        type: id % 2 ? 'youtube-channel' : 'rss-feed',
        articles: Array(6).fill({
          icon: 'youtube',
          title: 'title',
          url: 'https://pinute.org',
          created: 1607450415000
        })
      }))
    }
  })
};
