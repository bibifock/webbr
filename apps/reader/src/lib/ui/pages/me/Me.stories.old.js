import getStory from '$lib/utils/storybook/getStory';

import Component from './Me';

const story = getStory({
  Component,
  title: 'pages/Me',
  argTypes: {
    email: { control: 'text' },
    name: { control: 'text' },
    onLogout: { action: 'onLogout' },
    loading: { control: 'boolean' }
  }
});

export default story.default;

const title = 'vos paramètres';

export const base = story.bindWithArgs({
  title
});

export const filled = story.bindWithArgs({
  title,
  email: 'name@mail.ru',
  name: 'my-username'
});
