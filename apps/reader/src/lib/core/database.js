import VegeData from 'vege-data';

import { DB_SOURCE } from './config';

const vegeData = VegeData({
  filename: DB_SOURCE
  // log every model requests
  // queryLogger: ({ db, action, query }) => console.log({ db, action, query })
});

export default vegeData;
