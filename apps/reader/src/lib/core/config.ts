export const APP_SECRET = import.meta.env.VITE_PUBLIC_APP_SECRET;
export const PORT = import.meta.env.VITE_PUBLIC_PORT;
