import { error } from '@sveltejs/kit';

import { isJSONString } from '$lib/utils/json';

/**
 * @param {object} params
 *
 * @param {func}
 */
export const validate = async ({ request }) => {
  const { url, name, config } = await request.clone().json();

  if (!url || !name) {
    throw error(400, 'missing required params');
  }

  if (config && !isJSONString(config)) {
    throw error(400, "config isn't a valid json config");
  }

  return true;
};
