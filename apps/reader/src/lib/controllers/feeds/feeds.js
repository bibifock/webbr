import { json, error } from '@sveltejs/kit';

import { feedModel, subscriptionsModel } from '@webbr/reader-db';
import { parsers } from '@webbr/rss-parser';

import { parse } from '$lib/utils/json';

import { validate } from './validate';

const update = async ({ locals, request }) => {
  const user = locals.session.data;
  const { id, url, filter, ...rest } = await request.json();

  let feedData;
  if (id) {
    feedData = await feedModel.get(id);
  } else {
    feedData = await feedModel.getByProps({ url });
  }

  if (!feedData) {
    throw error(400, 'feed not found');
  }

  const feed = { ...feedData, ...rest, url };
  await feedModel.save(feed);
  await subscriptionsModel.save({
    idUser: user.id,
    idFeed: feed.id,
    filter
  });

  return json({
    ...feed,
    follow: true,
    filter
  });
};

update.validate = validate;

const create = async ({ locals, request }) => {
  const user = locals.session.data;
  const { url, filter, ...rest } = await request.json();

  // /////////////////////
  // check params part
  let feed = await feedModel.getByProps({ url });

  if (feed) {
    const alreadyFollow = await subscriptionsModel.getByProps({
      idFeed: feed.id,
      idUser: user.id
    });

    if (alreadyFollow) {
      throw error(400, 'feeds already followed');
    }
  }

  // //////////////////
  // save part
  if (!feed) {
    await feedModel.save({ ...rest, url });
    feed = await feedModel.getByProps({ url });
  }

  await subscriptionsModel.save({
    idFeed: feed.id,
    idUser: user.id,
    filter
  });

  return json({
    ...feed,
    follow: true,
    filter
  });
};

create.validate = validate;

const preview = async ({ request }) => {
  const { type, config, filter, ...feed } = await request.json();

  if (!parsers[type]) {
    throw error(400, `parser type: ${type}, not found`);
  }

  try {
    let result = await parsers[type]({
      ...feed,
      config: parse(config)
    });

    if (filter) {
      const reg = new RegExp(filter, 'i');
      result = result.filter((i) => reg.test(i.title));
    }

    return json(result.map((a) => ({ ...a, feed })));
  } catch (e) {
    throw error(500, e.message);
  }
};
preview.validate = validate;

export default {
  update,
  create,
  preview
};
