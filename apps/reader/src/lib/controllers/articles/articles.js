import { json, error } from '@sveltejs/kit';

import { articleModel, notificationsModel } from '@webbr/reader-db';

export const getAll = async ({ locals }) => {
  const user = locals.session.data;

  const articles = await articleModel.getByUser(user, { unread: true });

  return json({
    articles: articles.map(({ feed, ...a }) => ({
      ...a,
      feed: {
        ...feed,
        source: feed.url,
        url: `/user/feeds/${feed.id}`
      }
    }))
  });
};

export const read = async ({ locals, request }) => {
  const user = locals.session.data;
  const { article } = await request.json();

  if (!article.id) {
    throw error(400, { error: 'missing param' });
  }

  const found = await articleModel.get(article.id);
  if (!found) {
    throw error(400, { error: 'article not found' });
  }

  const unread = parseInt(article.unread) ? 0 : 1;
  await notificationsModel.save({
    idArticle: found.id,
    idUser: user.id,
    unread
  });

  return json({ ...found, unread });
};

export default {
  getAll,
  read
};
