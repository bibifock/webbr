const apiUrl = '/api';

// auth part
const authUrl = '/auth';
export const URL_LOGIN = `${authUrl}/login`;
export const URL_REGISTER = `${authUrl}/register`;

// user part
const userUrl = '/user/';

export const URL_USER_INDEX = userUrl;
export const URL_USER_FEEDS = `${userUrl}feeds`;
export const URL_USER_ARTICLES = `${userUrl}news`;

export const URL_API_USER_FEEDS = `${apiUrl}${userUrl}feeds.json`;
export const URL_API_USER_FEED_PREVIEW = `${apiUrl}${userUrl}feed-preview.json`;
export const URL_API_USER_ARTICLES = `${apiUrl}${userUrl}articles.json`;

export const URL_API_SPOTIFY = `${apiUrl}/spotify.json`;
