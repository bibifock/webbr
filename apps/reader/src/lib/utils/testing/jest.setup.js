import fs from 'fs';

import database from '$lib/core/database';

const dbSource = process.env.DB_SOURCE;

export default async () => {
  if (fs.existsSync(dbSource)) {
    fs.unlinkSync(dbSource);
  }

  await database.migrate();
};
