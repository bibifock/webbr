import { userModel, database } from '@webbr/reader-db';

export const user = {
  id: 1,
  email: 'admin@mail.com',
  name: 'admin',
  dateOfBirth: '11/11/2011'
};

/**
 * return all apps tables
 */
const getTablesLists = () =>
  database
    .connect()
    .then((db) =>
      db.all(`
      SELECT name
      FROM sqlite_master
      WHERE type ='table' AND name NOT LIKE 'sqlite_%' AND name != 'migrations'
    `)
    )
    .then((r) => r.map(({ name }) => name));

/**
 * clean all database table
 * @param {array} _.tables
 *
 * @return {promise}
 */
export const cleanTables = async ({ tables } = {}) => {
  if (!tables || tables.length) {
    tables = await getTablesLists();
  }

  const db = await database.connect();

  return Promise.all(tables.map((t) => db.run(`DELETE FROM ${t} WHERE 1=1`)));
};

/**
 * reset all database content
 */
export const resetDb = async () => {
  await cleanTables();

  userModel.save(user);
};
