import get from 'lodash/get.js';
import mapValues from 'lodash/mapValues.js';

/**
 * action handlers
 * @param {func} action
 *
 * @return {func} func who called action with rigth defailt param
 */
const handleAction = (action) => (e) => action(get(e, 'detail') || e);

/**
 * get story template
 * @param {component} _.Component
 * @param {func} props parser
 *
 * @return {func} Template
 */
const defineTemplate =
  (Component, parser = {}) =>
  (args) => {
    const { props, on } = Object.keys(args).reduce(
      (o, k) => {
        if (!/^on[A-Z].*/.test(k)) {
          o.props[k] = args[k];
          return o;
        }

        const eventName = k.replace(/^on/, '', k).replace(/^\w/, (l) => l.toLowerCase());

        o.on[eventName] = handleAction(args[k]);

        return o;
      },
      { props: {}, on: {} }
    );

    return {
      Component,
      props: parser.props ? parser.props(props) : props,
      on
    };
  };

/**
 * story helpers
 * @param {component} _.Component
 * @param {string} _.title
 * @param {object} _.parser
 *
 *
 * @return {object} { bind(args) => Story, default }
 */
const getStory = ({ Component, title, parser = {}, argTypes = {}, ...rest }) => {
  const Template = defineTemplate(Component, parser);

  const bind = (props = {}) => {
    const T = Template.bind({});
    return Object.assign(T, props);
  };

  argTypes = mapValues(argTypes, ({ table, ...arg }) => ({
    ...arg,
    table: {
      ...table,
      category: arg.actions ? 'events' : get(arg, 'table.category', 'props')
    }
  }));

  return {
    default: {
      component: Component,
      title,
      argTypes,
      ...rest
    },
    bind,
    bindWithArgs: (args = {}) => bind({ args })
  };
};

export default getStory;
