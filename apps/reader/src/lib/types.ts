export type Article = {
  id: number;
  image: string;
  feed: Feed;
  title: string;
  description: string;
  url: string;
  created: string;
  unread?: boolean;
  // only ui
  open?: boolean;
  loading?: boolean;
};

export type Feed = {
  id: number;
  name: string;
  type: string;
  url: string;
  config: string;
  idFeed: number;
  filter: string;
  follow: boolean;
};

export type EditableFeed = Feed & {
  edit?: boolean;
  errors?: string;
  articles?: Article[];
  loading?: boolean;
  loadingPreview?: boolean;
};
