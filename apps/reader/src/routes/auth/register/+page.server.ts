import { fail } from '@sveltejs/kit';
import validator from 'email-validator';

import { userModel } from '@webbr/reader-db';

import type { Actions } from '@sveltejs/kit';

const checkPassword = (password?: string) => password && password.length > 10;
const checkEmail = (email?: string) => email && validator.validate(email);

const getDataEntry = (data: FormData, key: string): string | undefined => {
  const entry = data.get(key);
  if (typeof entry === 'string') {
    return entry;
  }
  return undefined;
};

const getFormData = (data: FormData) => {
  return {
    password: getDataEntry(data, 'password'),
    email: getDataEntry(data, 'email'),
    name: getDataEntry(data, 'name')
  };
};

export const actions = {
  default: async ({ request }) => {
    const data = await request.formData();
    const { password, email, name } = await getFormData(data);

    const errors: Record<string, string> = {};
    if (!name) {
      errors.name = 'should not be empty';
    }

    if (!checkPassword(password)) {
      errors.password = 'need at least 11 characters... sorry...';
    }

    if (!checkEmail(email)) {
      errors.email = 'please enter a valid email';
    }

    if (Object.keys(errors).length) {
      return fail(400, { errors });
    }

    return userModel
      .register({
        email,
        password,
        name
      })
      .then(() => {
        true;
      })
      .catch((e: Error) => {
        // eslint-disable-next-line no-console
        console.error('[REGISTER] failed', e.message);

        return fail(500, { errors: e.message });
      });
  }
} satisfies Actions;
