import { redirect } from '@sveltejs/kit';

import { articleModel, feedModel } from '@webbr/reader-db';

import type { Article, Feed } from '$lib/types';

import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();
  const idFeed = Number(params.id);

  if (!session?.id && !idFeed) {
    throw redirect(302, '/');
  }

  const feed = (await feedModel.get(idFeed)) as Feed;
  const items = (await articleModel.getByFeed(idFeed, session.id)) as Article[];

  return {
    ...feed,
    items: items.map((i) => ({ ...i, feed }))
  };
}) satisfies PageServerLoad;
