import { URL_API_USER_FEEDS } from '$lib/views/constants';
import type { Feed } from '$lib/types';

import type { PageLoad } from './$types';

export const load = (async ({ fetch }) => {
  const res = await fetch(URL_API_USER_FEEDS);
  const json = await res.json();

  return json;
}) satisfies PageLoad<{
  types: string[];
  feeds: Feed[];
  spotify: boolean;
}>;
