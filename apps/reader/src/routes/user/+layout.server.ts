import { redirect } from '@sveltejs/kit';

import { URL_LOGIN } from '$lib/views/constants';

import type { LayoutServerLoad } from './$types';

export const load = (async ({ parent }) => {
  const { session } = await parent();
  if (session?.id === undefined) {
    throw redirect(302, URL_LOGIN);
  }
}) satisfies LayoutServerLoad;
