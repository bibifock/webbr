import { error } from '@sveltejs/kit';
import SpotifyWebApi from 'spotify-web-api-node';

import { feedModel, subscriptionsModel, userModel } from '@webbr/reader-db';

import { URL_USER_FEEDS } from '$lib/views/constants';

import type { RequestHandler } from '@sveltejs/kit';

type Params = undefined | { code: string } | { access_token: string; refresh_token: string };

const scopes = [
  'ugc-image-upload',
  'user-modify-playback-state',
  'user-read-playback-state',
  'user-read-currently-playing',
  'user-follow-modify',
  'user-follow-read',
  'user-read-recently-played',
  'user-read-playback-position',
  'user-top-read',
  'playlist-read-collaborative',
  'playlist-modify-public',
  'playlist-read-private',
  'playlist-modify-private',
  'app-remote-control',
  'streaming',
  'user-read-email',
  'user-read-private',
  'user-library-modify',
  'user-library-read'
];

const config = {
  redirectUri: process.env.SPOTIFY_REDIRECT_URI,
  clientId: process.env.SPOTIFY_CLIENT_ID,
  clientSecret: process.env.SPOTIFY_CLIENT_SECRET
};

const getParams = (url: URL): Params => {
  const code = url.searchParams.get('code');

  if (code) {
    return { code };
  }

  const access_token = url.searchParams.get('access_token');
  const refresh_token = url.searchParams.get('refresh_token') ?? '';
  if (access_token) {
    return {
      access_token,
      refresh_token
    };
  }

  return undefined;
};

export const GET = (async ({ locals, url }) => {
  const userId = locals.session.data?.id;
  if (!userId) {
    throw error(403, 'Forbidden');
  }

  const api = new SpotifyWebApi(config);

  const params = getParams(url);
  if (params === undefined) {
    const authorizeURL = api.createAuthorizeURL(scopes, ' ');

    return new Response(undefined, {
      status: 301,
      headers: {
        Location: authorizeURL
      }
    });
  }

  if ('code' in params) {
    const { body } = await api.authorizationCodeGrant(params.code);
    const searchParams = new URLSearchParams(body as unknown as Record<string, string>);

    return new Response(undefined, {
      status: 301,
      headers: {
        Location: config.redirectUri + '?' + searchParams.toString()
      }
    });
  }

  await userModel.saveSpotify(userId, {
    accessToken: params.access_token,
    refreshToken: params.refresh_token
  });

  const idFeed = await feedModel.getServiceId('spotify');

  await subscriptionsModel.save({
    idUser: userId,
    idFeed
  });

  return new Response(undefined, {
    status: 301,
    headers: {
      Location: URL_USER_FEEDS
    }
  });

  //const api = buildAPI({ access_token, refresh_token });

  //const artists = await api.getAllArtists();
  //const body = await api.getNewSongs(artists);

  //return json(body);

  //const api = new SpotifyWebApi(config);
  //const authorizeURL = api.createAuthorizeURL(scopes, ' ');
  //const data = await api.getFollowedArtists({ limit : 1 });

  //console.log('This user is following ', data.body.artists.total, ' artists!');
  //console.log(authorizeURL, config);

  //res.succ({ success: true,  rest });
}) satisfies RequestHandler;
