import { json, error } from '@sveltejs/kit';
import validator from 'email-validator';

import { userModel } from '@webbr/reader-db';

import type { RequestHandler } from '@sveltejs/kit';

export const PUT = (async ({ request, locals }) => {
  const body = await request.json();

  try {
    const user = await userModel.login(body);

    await locals.session.set(user);

    return json({ user });
  } catch {
    throw error(400, 'sorry, this is not possible');
  }
}) satisfies RequestHandler;

export const DELETE = (async ({ locals }) => {
  await locals.session.destroy();

  return json({ ok: true });
}) satisfies RequestHandler;

const checkPassword = (password?: string) => password && password.length > 10;
const checkEmail = (email: string) => validator.validate(email);

type RegisterRequest = {
  password: string;
  email: string;
  name: string;
};

export const POST = (async ({ request }) => {
  const { password, email, name } = await request.json();

  const errors: string[] = [];
  if (!name) {
    errors.push('should not be empty');
  }

  if (!checkPassword(password)) {
    errors.push('need at least 11 characters... sorry...');
  }

  if (!checkEmail(email)) {
    errors.push('please enter a valid email');
  }

  if (errors.length) {
    throw error(400, errors.join(';'));
  }

  return userModel
    .register({
      email,
      password,
      name
    })
    .then(() => json({ success: true }))
    .catch((e: Error) => {
      throw error(400, e.message);
    });
}) satisfies RequestHandler<RegisterRequest>;
