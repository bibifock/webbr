import http from 'https';

const targetURL = 'https://www.arte.tv/fr/videos/RC-015950/quand-l-histoire-fait-dates/';

export const GET = (req, res) => {
  http.get(targetURL).on('response', function (response) {
    let body = '';
    response.on('data', function (chunk) {
      body += chunk;
    });
    response.on('end', function () {
      //const result = body.match(/window\.__INITIAL_STATE__ = \(?<state>.+\)$/)
      const { groups } = body.match(/window\.__INITIAL_STATE__ = (?<state>.+)/);
      const state = JSON.parse(groups.state.replace(/;$/, ''));

      const list = state.pages.list['RC-015950_fr_{}'].zones
        .filter(({ displayOptions }) => displayOptions.zoneLayout === 'verticalFirstHighlighted')
        .map(({ data }) => data);

      const videos = list[0]
        .sort((a, b) => {
          if (a.availability.upcomingDate > b.availability.upcomingDate) return -1;
          if (a.availability.upcomingDate < b.availability.upcomingDate) return 1;
          return 0;
        })
        .map(
          ({
            images,
            url,
            subtitle,
            shortDescription,
            availability,
            availability: { label },
            ...rest
          }) => ({
            label,
            title: subtitle,
            description: shortDescription,
            url,
            image: {
              src: images.landscape.resolutions[0].url,
              alt: images.landscape.caption
            },
            other: {
              ...rest,
              images,
              availability
            }
          })
        );

      res.succ({ videos });
    });
  });
};
