import { error } from '@sveltejs/kit';

export const needAuth = (action) => async (args) => {
  const { locals, ...rest } = args;
  if (!locals.session.data?.id) {
    throw error(403, 'Forbidden');
  }

  if (action.validate) {
    await action.validate({ ...rest, locals });
  }

  return action(args);
};
