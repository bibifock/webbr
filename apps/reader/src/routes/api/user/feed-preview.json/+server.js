import feedsController from '$lib/controllers/feeds/feeds';

import { needAuth } from '../_needAuth';

export const POST = needAuth(feedsController.preview);
