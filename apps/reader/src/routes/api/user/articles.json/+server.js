import articlesController from '$lib/controllers/articles/articles';

import { needAuth } from '../_needAuth';

export const GET = needAuth(articlesController.getAll);

export const PUT = needAuth(articlesController.read);
