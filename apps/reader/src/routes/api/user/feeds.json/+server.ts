import { json, error } from '@sveltejs/kit';

import { feedModel, userModel } from '@webbr/reader-db';
import { parsers } from '@webbr/rss-parser';

import feedsController from '$lib/controllers/feeds/feeds';
import type { Feed } from '$lib/types';

import { needAuth } from '../_needAuth';

import type { RequestHandler } from '@sveltejs/kit';

type FeedByUser = Feed & {
  idUser: number;
};

export const GET = (async ({ locals }) => {
  if (!locals.session.data?.id) {
    throw error(403, 'Forbidden');
  }

  const user = await userModel.get(locals.session.data.id);
  const feeds: FeedByUser[] = await feedModel.getByUser(user);

  const types = Object.keys(parsers);

  return json({
    spotify: !!user?.spotify,
    feeds: feeds.map(({ idUser, ...rest }) => ({ ...rest, follow: !!idUser })),
    types
  });
}) satisfies RequestHandler;

export const POST = needAuth(feedsController.create);

export const PUT = needAuth(feedsController.update);
