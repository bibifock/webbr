import { handleSession } from 'svelte-kit-cookie-session';

import { APP_SECRET } from '$lib/core/config';

// You can do it like this, without passing a own handle function
export const handle = handleSession({
  secret: APP_SECRET
});
