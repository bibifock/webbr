const apiUrl = '/api';

// auth part
const authUrl = '/auth/';
export const URL_LOGIN = `${authUrl}login`;
export const URL_REGISTER = `${authUrl}register`;

export const URL_API_LOGOUT = `${apiUrl}${authUrl}logout.json`;
export const URL_API_LOGIN = `${apiUrl}${authUrl}login.json`;
export const URL_API_REGISTER = `${apiUrl}${authUrl}register.json`;

// search part
export const URL_SEARCH = '/search';
export const URL_SEARCH_QUERY = '/search?s=#QUERY#';

export const URL_API_SEARCH = `${apiUrl}/search.json`;

// user part
const userUrl = '/user/';

export const URL_USER_INDEX = userUrl;
export const URL_USER_STACK = `${userUrl}stack`;

export const URL_API_USER = `${apiUrl}${userUrl}user.json`;
export const URL_API_USER_STACK = `${apiUrl}${userUrl}stack.json`;
export const URL_API_USER_IMPORT = `${apiUrl}${userUrl}import.json`;

// saga
export const URL_API_SAGA_VOLUME = `${apiUrl}/saga/#ID#/#TOME#.json`;
// novel
export const URL_API_NOVEL_STATUS = `${apiUrl}/novel.json`;
