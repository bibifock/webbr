import { URL_USER_STACK } from '../../constants';

import stack from '../../fixtures/stack';

const cyNovel = 'organisms-novel';

describe('stack', () => {
  beforeEach(() => {
    cy.loadPlaybook(stack);
    cy.clearCookies();
    cy.login();

    cy.getCyId('navbar-link-stack').click();

    cy.waitUntilCyIdFound('stack-content');
  });

  it('should redirect to right url', () => {
    cy.url().should('include', URL_USER_STACK);
  });

  it('should display user selected content', () => {
    cy.getCyId(cyNovel).should('have.length', 2);
  });

  describe('saga', () => {
    const cySagaTomes = 'organisms_saga-tomes';
    const cyNovelTitle = 'molecules_novel-header_title';

    it('should display asked saga', () => {
      cy.getCyId(cySagaTomes, '> span.active').should('have.length', 1).should('have.html', '1');

      const { title } = stack.novels.find((n) => n.sagaVolume === 1);

      cy.getCyId(cyNovelTitle).last().should('have.html', title);
    });

    it('should load all tome added tomes from a saga', () => {
      cy.getCyId(cySagaTomes, '> span.loaded').should('have.length', 2);
    });

    it('on click on loaded tome change content', () => {
      cy.getCyId(cySagaTomes, ' > span.loaded').last().click();

      cy.getCyId(cySagaTomes, '> span.active').should('have.length', 1).should('have.html', '2');

      const { title } = stack.novels.find((n) => n.sagaVolume === 2);

      cy.getCyId(cyNovelTitle).last().should('have.html', title);
    });

    it('on click should loaded missing tome', () => {
      cy.getCyId(cySagaTomes, ' > span').last().click();

      cy.getCyId(cySagaTomes, '> span.active').should('have.length', 1).should('have.html', '3');

      const { title } = stack.novels.find((n) => n.sagaVolume === 3);

      cy.getCyId(cyNovelTitle).last().should('have.html', title);
    });

    it('on status click should change status', () => {
      cy.getCyId('molecules_novelStatus')
        .last()
        .invoke('attr', 'data-value')
        .should('equal', 'STATUS_TO_READ');

      cy.get('.novel-status button').last().click();

      cy.getCyId('molecules_novelStatus_values').eq(1).click();

      cy.getCyId('molecules_novelStatus')
        .last()
        .invoke('attr', 'data-value')
        .should('equal', 'STATUS_IS_READING');
    });
  });
});
