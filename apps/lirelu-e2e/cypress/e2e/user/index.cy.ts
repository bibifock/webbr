import { URL_USER_INDEX, URL_LOGIN } from '../../constants';

describe('me', () => {
  let userFix;
  beforeEach(() => {
    cy.fixture('user').then((user) => {
      userFix = user;
      cy.task('db:reset');
      cy.task('db:addUsers', [user]);
    });
  });

  describe('when logged', () => {
    beforeEach(() => {
      cy.clearCookies();
      cy.login();

      cy.getCyId('navbar-link-user').click();

      cy.get('[data-cy="form-me"]').should('exist');
    });

    it('should display user information', () => {
      cy.get('input[name=email]').should('have.value', userFix.email);
      cy.get('input[name=name]').should('have.value', userFix.name);
    });

    describe('on logout', () => {
      beforeEach(() => {
        cy.getCyId('me-form_actions_logout').click();
      });

      it('logout form should work', () => {
        cy.waitUntilCyIdFound('form-login');
        cy.url().should('include', URL_LOGIN);
      });

      it('should redirect to login url', () => {
        cy.visit(URL_USER_INDEX);
        cy.wait(50);

        cy.url().should('include', URL_LOGIN);
      });
    });
  });
});
