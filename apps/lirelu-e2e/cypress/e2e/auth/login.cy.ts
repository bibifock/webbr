import { URL_LOGIN, URL_SEARCH } from '../../constants';

describe('login', () => {
  let userFix;
  beforeEach(() => {
    cy.fixture('user').then((user) => {
      userFix = user;
      cy.task('db:reset');
      cy.task('db:addUsers', [user]);
    });
  });

  it('default render, with login form', () => {
    cy.visit(URL_LOGIN);
    cy.wait(500);
    cy.get('[data-cy="form-login"]').should('exist');
  });

  describe('login form', () => {
    it('button should be disabled', () => {
      cy.visit(URL_LOGIN);
      cy.wait(500);
      cy.get('button').should('be.disabled');
    });

    it('button should be enable, when form filled', () => {
      cy.visit(URL_LOGIN);
      cy.wait(500);
      cy.get('input[name="login"]').type('login');
      cy.get('input[name="password"]').type('password');

      cy.get('button').should('not.be.disabled');
    });

    describe('login action', () => {
      describe('should failed when', () => {
        // eslint-disable-next-line mocha/no-setup-in-describe
        [
          ['bad name', { login: 'admin 1' }],
          ['bad email', { login: 'failed@mail.com' }],
          ['bad password', { password: '213' }]
        ].forEach(([title, args]) => {
          it(title, () => {
            cy.visit(URL_LOGIN);
            cy.wait(500);

            const userAuth = { ...userFix, ...args };

            cy.get('input[type="text"]').type(userAuth.login || userAuth.email);
            cy.get('input[type="password"]').type(userAuth.password);

            cy.get('button').click();

            cy.waitUntilCyIdFound('errors');

            cy.getCyId('errors').should('exist').contains('not possible');
          });
        });
      });

      describe('when connected should redirect', () => {
        it('to home by default', () => {
          cy.login();

          cy.getCyId('home').should('exist');
        });
      });

      it('to redirect params when exist', () => {
        cy.visit(URL_LOGIN + '?r=' + encodeURIComponent(URL_SEARCH));
        cy.wait(500);

        cy.get('input[type="text"]').type(userFix.email);
        cy.get('input[type="password"]').type(userFix.password);

        cy.get('button').click();

        cy.url().should('include', URL_SEARCH);
      });
    });
  });
});
