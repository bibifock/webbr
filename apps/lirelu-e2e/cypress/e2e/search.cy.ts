import { URL_SEARCH_QUERY } from '../constants';

const getURL = (query) => URL_SEARCH_QUERY.replace('#QUERY#', query);

const baseSearch = 'kosigan';
const visitIt = (query = baseSearch) => {
  cy.visit(getURL(query));

  cy.waitUntilCyIdFound('page-search');
};

describe('search', () => {
  beforeEach(() => {
    cy.loadPlaybook();
    visitIt();
  });

  describe('default render', () => {
    it('with search input filled', () => {
      cy.getCyId('template-search', 'input').should('have.value', baseSearch);
    });

    it('with novels result', () => {
      cy.getCyId('organisms-novel').should('exist');
    });
  });

  describe('action click', () => {
    beforeEach(() => {
      visitIt();

      cy.waitUntilCyIdFound('organisms-novel');
    });
    describe('when not logged', () => {
      beforeEach(() => {
        cy.getCyId('organisms-novel-action').first().click();
        cy.getCyId('molecules_novelStatus_values').first().click();

        cy.waitUntilCyIdFound('form-login');
      });

      it('should redirect to login page', () => {
        cy.getCyId('form-login').should('exist');
      });

      it('after log in user should be redirect to previous page', () => {
        cy.fillLogin();

        cy.waitUntilCyIdFound('page-search');
      });
    });
  });
});
