/// <reference types="cypress" />
import 'cypress-wait-until';

import { URL_LOGIN } from '../constants';

Cypress.Commands.add('getCyId', (id, selector = '') =>
  cy.get(`[data-cy="${id}"]${selector ? ' ' + selector : ''}`)
);

Cypress.Commands.add('waitUntilCyIdFound', (id, selector = '') =>
  cy.getCyId(id, selector).then((el) => el.length > 0)
);

const login = (user, withName = false) => {
  cy.wait(500);
  cy.getCyId('form-login', 'input[type="text"]').type(withName ? user.name : user.email);
  cy.getCyId('form-login', 'input[type="password"]').type(user.password);
  cy.wait(50);

  cy.get('button').click();
};

Cypress.Commands.add('fillLogin', () => cy.fixture('user').then(login));

Cypress.Commands.add('login', (user, withName = false) => {
  cy.visit(URL_LOGIN);

  if (user) {
    login(user, withName);
  } else {
    cy.fixture('user').then((u) => login(u, withName));
  }

  cy.waitUntil(() => cy.getCyId('home').then((el) => el.length > 0));
});

Cypress.Commands.add('loadPlaybook', (book) => {
  cy.task('db:reset');
  cy.fixture('user').then((user) => cy.task('db:addUsers', [user]));

  if (!book) {
    return;
  }

  Object.keys(book).map((table) => cy.task('db:fill', { table, data: book[table] }));
});
