// ***********************************************************
// This example support/e2e.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')
/// <reference types="cypress" />
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      /**
       * Custom command to select DOM element by data-cy attribute.
       * @example cy.dataCy('greeting')
       */
      getCyId(value: string, selector?: string): Chainable<Element>;
      waitUntilCyIdFound(id: string, selector?: string): Chainable<Element>;
      login(user: { name: string; email: string; password: string }, withName?: boolean): void;
      fillLogin(): Chainable<Element>;
      loadPlaybook(book?: unknown): void;
    }
  }
}
