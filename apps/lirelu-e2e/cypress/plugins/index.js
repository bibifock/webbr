/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const path = require('path');

const webpack = require('@cypress/webpack-preprocessor');

const database = require('./database');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  const options = {
    webpackOptions: {
      resolve: {
        alias: {
          app: path.resolve(__dirname, '../../src/app'),
          utils: path.resolve(__dirname, '../../src/utils')
        }
      }
    }
  };

  on('file:preprocessor', webpack(options));

  on('task', {
    'db:reset': database.reset,
    'db:addUsers': database.addUsers,
    'db:fill': database.fillTable,
    'db:run': database.run
  });

  const configWithDotenv = require('dotenv').config();
  if (configWithDotenv.error) {
    throw configWithDotenv.error;
  }
  config.env = { ...config.env, ...configWithDotenv.parsed };
  return config;
};
