import SQL from 'sql-template-strings';
import bcrypt from 'bcryptjs';
import lireluDb from '@webbr/lirelu-db';

const { database } = lireluDb;

/**
 * transform array of items.
 * [1, 2, 3] => SQL'${1}, ${2}, ${3}';
 *
 * @param {array} items
 * @param {func} each function call for each elements (by default add value)
 * @param {func} concat function call for each join action (by default add ', ')
 *
 * @return object sql template string
 */
const concatValues = (
  items,
  each = (item) => SQL`${item}`,
  concat = (index) => (index > 0 ? ', ' : '')
) => {
  const query = SQL``;
  items.forEach((item, index) => query.append(concat(index)).append(each(item)));
  return query;
};

const tableFields = {
  saga: ['id', 'title', 'nbNovels'],
  authors: ['id', 'name'],
  libraries: ['id', 'name', 'url'],
  novelsStack: ['idUser', 'idNovel', 'idStatus', 'remark'],
  novels: ['id', 'title', 'description', 'idAuthor', 'idSaga', 'sagaVolume', 'created'],
  books: ['id', 'idNovel', 'isbn', 'edition', 'url', 'image', 'details'],
  bookcase: ['idLibrary', 'idBook', 'isAvailable', 'isReservable', 'whenBack', 'details'],
  loans: ['idUser', 'idBook', 'idLibrary', 'whenBack', 'details']
};

const reset = async () => {
  const db = await database.connect();

  //await db.migrate({ force: 'last' });

  return Promise.all([
    db.run(SQL`DELETE FROM users WHERE 1=1`),
    db.run(SQL`DELETE FROM saga WHERE 1=1`),
    db.run(SQL`DELETE FROM authors WHERE 1=1`),
    db.run(SQL`DELETE FROM libraries WHERE 1=1`),
    db.run(SQL`DELETE FROM novelsStack WHERE 1=1`),
    db.run(SQL`DELETE FROM novels WHERE 1=1`),
    db.run(SQL`DELETE FROM books WHERE 1=1`),
    db.run(SQL`DELETE FROM bookcase WHERE 1=1`),
    db.run(SQL`DELETE FROM loans WHERE 1=1`)
  ]);
};

const addUsers = async (users) => {
  const db = await database.connect();

  users = await Promise.all(
    users.map(async (user) => {
      if (user.password) {
        user.password = await bcrypt.hash(user.password, 10);
      }

      return user;
    })
  );

  const query = SQL`
    INSERT INTO users (id, email, password, name) VALUES
  `.append(
    concatValues(
      users,
      (user) => SQL`(${user.id}, ${user.email}, ${user.password}, ${user.name})`,
      (i) => (i > 0 ? ',' : '')
    )
  );

  return db.run(query);
};

const fillTable = ({ fields, table, data }) => {
  const model = database.model.init({
    fields: fields || tableFields[table],
    table
  });

  return model.saveAll(data);
};

const run = async (queries) => {
  const db = await database.connect();

  return Promise.all(queries.map((query) => db.run(query)));
};

export default { reset, addUsers, fillTable, run };
