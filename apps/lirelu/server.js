import polka from 'polka';
import sirv from 'sirv';

import { handler } from './build/handler.js';

const { PORT = 3000 } = process.env;
polka()
  .use(
    sirv('static', {
      // no cache
      dev: true
    })
  )
  .use(handler)
  .listen(PORT, () => {
     
    console.log(`listening on port ${PORT}`);
  });
