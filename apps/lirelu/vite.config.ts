import { resolve } from 'path';

import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig, type UserConfig } from 'vite';

export default defineConfig({
  optimizeDeps: {
    include: ['@webbr/lirelu-db', '@webbr/api-bibli'],
    exclude: ['@mapbox/node-pre-gyp']
  },
  //ssr: {
  //noExternal: ['lodash']
  //},
  resolve: {
    //dedupe: ['lodash'],
    alias: {
      $controllers: resolve('./src/controllers'),
      $core: resolve('./src/core'),
      $ui: resolve('./src/ui'),
      $utils: resolve('./src/utils'),
      $views: resolve('./src/views')
    }
  },
  plugins: [sveltekit()]
});
