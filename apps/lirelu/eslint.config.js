import { fileURLToPath } from 'node:url';

import prettier from 'eslint-config-prettier';
import js from '@eslint/js';
import { includeIgnoreFile } from '@eslint/compat';
import svelte from 'eslint-plugin-svelte';
import globals from 'globals';
import eslintPluginImportX from 'eslint-plugin-import-x';
import ts from 'typescript-eslint';

const gitignorePath = fileURLToPath(new URL('./.gitignore', import.meta.url));

export default ts.config(
  includeIgnoreFile(gitignorePath),
  js.configs.recommended,
  ...ts.configs.recommended,
  ...svelte.configs['flat/recommended'],
  prettier,
  ...svelte.configs['flat/prettier'],
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      }
    }
  },
  {
    files: ['**/*.svelte'],
    languageOptions: {
      parserOptions: {
        parser: ts.parser
      }
    }
  },
  eslintPluginImportX.flatConfigs.recommended,
  eslintPluginImportX.flatConfigs.typescript,
  {
    settings: {
      'import/resolver': {
        typescript: {
          project: './tsconfig.json', // Point to your tsconfig.json for correct paths
        },
      },
    }
  },
  {
    rules: {
      '@typescript-eslint/no-unused-vars': 'error',
      'import-x/order': [
        'error',
        {
          'newlines-between': 'always',
          pathGroups: [
            {
              pattern: '$app/**',
              group: 'external',
              position: 'after'
            },
            {
              pattern: '@webbr/**',
              group: 'external',
              position: 'after'
            }, {
              pattern: `$(controllers|core|lib|services|ui|utils|views)/**`,
              group: 'internal',
            }
          ],
          pathGroupsExcludedImportTypes: [],
          groups: [
            'builtin',
            'external',
            'internal',
            'parent',
            'sibling',
            'index',
            'object',
            'type'
          ]
        }
      ],
      'no-console': 'error'
    }
  }
);
