import fs from 'fs';

import { database } from '@webbr/lirelu-db';

const dbSource = process.env.DB_SOURCE;

export default async () => {
  if (fs.existsSync(dbSource)) {
    fs.unlinkSync(dbSource);
  }

  await database.migrate();
};
