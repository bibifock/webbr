/**
 * send a post and parse json result
 * @param {string} endpoint
 * @param {object} data
 * @param {object} options = {}
 *
 * @return {Promise}
 */
export function post(endpoint, data, options = {}) {
  let isOk;
  return fetch(endpoint, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
    .then((r) => {
      isOk = r.ok;
      return r.json();
    })
    .then((r) => {
      if (isOk) {
        return r;
      }
      if ('message' in r) {
        throw new Error(r.message);
      }

      throw r.error;
    });
}

/**
 * send a get and parse json result
 * @param {string} endpoint
 *
 * @return {Promise}
 */
export function get(endpoint) {
  return fetch(endpoint).then((r) => r.json());
}

/**
 * send a put and parse json result
 * @param {string} endpoint
 * @param {object} data
 *
 * @return {Promise}
 */
export function put(endpoint, data) {
  return post(endpoint, data, { method: 'PUT' });
}

/**
 * send a del and parse json result
 * @param {string} endpoint
 * @param {object} data
 *
 * @return {Promise}
 */
export function del(endpoint, data) {
  return post(endpoint, data, { method: 'DELETE' });
}

export default {
  post,
  get,
  put,
  del
};
