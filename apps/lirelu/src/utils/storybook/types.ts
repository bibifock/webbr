import type { Meta, StoryObj } from '@storybook/svelte';
import type {
  ComponentProps,
  SvelteComponent,
  Component as ComponentSvelte
} from 'svelte';

type BaseComponent = SvelteComponent | ComponentSvelte<any, any>;

export type CustomMeta<
  Component extends BaseComponent,
  Props extends Record<string, unknown> = ComponentProps<Component>
> = Omit<Meta<Component>, 'args' | 'component'> & {
  component: ComponentSvelte<Props>;
  args?: Partial<Props>;
};

export type CustomStoryObj<
  CustomMetaType = Record<string, unknown>,
  Component extends BaseComponent = SvelteComponent
> = StoryObj<CustomMetaType> & {
  args?: CustomMetaType extends {
    args: Record<string, unknown>;
  }
    ? CustomMetaType['args']
    : ComponentProps<Component>;
};
