export const colorType = {
  control: {
    type: 'select',
    options: [
      'primary',
      'secondary',
      'success',
      'danger',
      'warning',
      'info',
      'light',
      'dark',
      'link'
    ]
  }
};
