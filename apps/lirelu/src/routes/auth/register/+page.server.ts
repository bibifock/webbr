import { fail } from '@sveltejs/kit';
import validator from 'email-validator';

import { userModel } from '@webbr/lirelu-db';

import { getFormDataString } from '$utils/getFormDataString';

import type { Actions } from '@sveltejs/kit';

const checkPassword = (password?: string) => password && password.length > 10;
const checkEmail = (email?: string) => email && validator.validate(email);

const getFormData = (data: FormData) => {
  return {
    password: getFormDataString(data, 'password'),
    email: getFormDataString(data, 'email'),
    name: getFormDataString(data, 'name')
  };
};

export const actions = {
  default: async ({ request }) => {
    const data = await request.formData();
    const { password, email, name } = await getFormData(data);

    const errors: Record<string, string> = {};
    if (!name) {
      errors.name = 'should not be empty';
    }

    if (!checkPassword(password)) {
      errors.password = 'need at least 11 characters... sorry...';
    }

    if (!checkEmail(email)) {
      errors.email = 'please enter a valid email';
    }

    if (Object.keys(errors).length) {
      return fail(400, { errors });
    }

    return userModel
      .register({
        email,
        password,
        name
      })
      .then(() => {
        true;
      })
      .catch((e: Error) => {
         
        console.error('[REGISTER] failed', e.message);

        return fail(500, { errors: e.message });
      });
  }
} satisfies Actions;
