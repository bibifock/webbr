import { fail } from '@sveltejs/kit';

import { userModel } from '@webbr/lirelu-db';

import type { Actions } from '@sveltejs/kit';

export const actions = {
  default: async ({ request, locals }) => {
    const data = await request.formData();

    try {
      const user = await userModel.login({
        login: data.get('login'),
        password: data.get('password')
      });

      await locals.session.set(user);

      return { user };
    } catch (e: unknown) {
      if (e instanceof Error) {
         
        console.error('[LOGIN FAILED]', e?.message);
      }
      return fail(400, { incorrect: true });
    }
  }
} satisfies Actions;
