import { redirect } from '@sveltejs/kit';

import { URL_USER_INDEX } from '$views/constants';

import type { LayoutServerLoad } from './$types';

export const load = (async ({ parent }) => {
  const { session } = await parent();
  if (session?.id !== undefined) {
    redirect(307, URL_USER_INDEX);
  }
}) satisfies LayoutServerLoad;
