import { fail } from '@sveltejs/kit';
import { z } from 'zod';

import { parisBibli } from '@webbr/api-bibli';
import { userModel, loanModel } from '@webbr/lirelu-db';

import type { Action } from '@sveltejs/kit';

const { renewLoan, getLoans } = parisBibli;

const RenewLoanSchema = z.object({
  renewData: z.object({
    HoldingId: z.string().nonempty(),
    RecordId: z.string().nonempty(),
    Id: z.string().nonempty()
  }),
  idBook: z.coerce.number().positive(),
  idLibrary: z.coerce.number().positive()
});

type RenewLoan = z.infer<typeof RenewLoanSchema>;

const parseLoanData = async (request: Request): Promise<false | RenewLoan> => {
  const data = await request.formData();
  const renewLoan = {
    renewData: {
      HoldingId: data.get('HoldingId'),
      RecordId: data.get('RecordId'),
      Id: data.get('Id')
    },
    idBook: data.get('idBook'),
    idLibrary: data.get('idLibrary')
  };

  const result = RenewLoanSchema.safeParse(renewLoan);

  return result.success === true ? result.data : false;
};

export const renewLoanAction: Action = async ({ request, locals }) => {
  const data = await parseLoanData(request);

  if (data === false) {
    return fail(400, { incorrect: true });
  }

  const user = await userModel.get(locals.session.data.id);
  if (!user) {
    return fail(400, { error: 'user not found' });
  }

  const userLogin = {
    username: user.email,
    password: user.dateOfBirth
  };

  try {
    await renewLoan(userLogin, data.renewData);

    const result = await getLoans({ ...userLogin, logged: true });
    const loanFound = result.find((v) => v.searchId === data.renewData.Id);
    if (!loanFound) {
      throw Error("[getLoans] can't find wanted loan");
    }

    const loan = {
      idBook: data.idBook,
      idLibrary: data.idLibrary,
      idUser: user.id,
      whenBack: loanFound.whenBack,
      details: JSON.stringify(loanFound)
    };
    await loanModel.save(loan);

    return { success: true, loan: loan };
  } catch (e) {
    console.error('[stack.actions.renewLoan]', e);
    return fail(500, { incorrect: true, e });
  }
};
