import { URL_API_USER_STACK } from '$views/constants';

import { renewLoanAction } from './renewLoan.action';

import type { Actions } from '@sveltejs/kit';
import type { PageServerLoad } from '../$types';

export const load: PageServerLoad = async ({ fetch }) => {
  const res = await fetch(URL_API_USER_STACK);

  return { stack: await res.json() };
};

export const actions = {
  renewLoan: renewLoanAction
} satisfies Actions;
