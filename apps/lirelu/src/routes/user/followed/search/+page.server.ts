import { redirect } from '@sveltejs/kit';

import { searchModel } from '@webbr/lirelu-db';

import { URL_LOGIN } from '$views/constants';
import { DEPENDS_ENUM } from '$lib/types';
import { searchUnfollow } from '$lib/actions';

import type { Actions, PageServerLoad } from './$types';

export const load: PageServerLoad = ({ locals, depends }) => {
  const user = locals.session.data;
  if (!user?.id) {
    redirect(307, URL_LOGIN);
  }

  depends(DEPENDS_ENUM.userFollowedSearch);

  const searchs = searchModel.getActiveSearch(user);

  return {
    streamed: {
      searchs
    }
  };
};

export const actions = {
  searchUnfollow
} satisfies Actions;
