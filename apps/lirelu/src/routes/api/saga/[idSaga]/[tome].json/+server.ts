import { json } from '@sveltejs/kit';
import get from 'lodash/get';

import { novelModel } from '@webbr/lirelu-db';

import { makeResponseTree } from '$controllers/novels-stack/utils';

import type { RequestHandler } from './$types';

export const GET = (async ({ params }) => {
  const { tome: sagaVolume, idSaga } = params;

  const result = await novelModel.getFilledByProps({ sagaVolume, idSaga });
  const output = makeResponseTree(result);

  return json(get(output, 0));
}) satisfies RequestHandler;
