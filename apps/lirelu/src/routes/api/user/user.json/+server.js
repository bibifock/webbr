import userController from '$controllers/user/user';

export const GET = userController.get;
export const PUT = userController.update;
