import get from 'lodash/get';
import { error, json } from '@sveltejs/kit';

import {
  novelsStackModel,
  novelModel,
  STATUSES,
  isStatus,
  libraryModel
} from '@webbr/lirelu-db';

import { importNovel } from '$controllers/novels-stack/importNovel';
import { sortStackItem } from '$controllers/novels-stack/sortStackItem';

import type { RequestHandler, RequestEvent } from './$types';

const getConnectedUserOrThrow = (locals: RequestEvent['locals']) => {
  const user = locals.session.data;

  if (!user?.id) {
    error(403, 'Forbidden');
  }

  return user;
};

export const GET = (async ({ locals }) => {
  const user = getConnectedUserOrThrow(locals);

  const [result, libraries] = await Promise.all([
    novelsStackModel.allByUser(user),
    libraryModel.all()
  ]);

  return json({ items: result.sort(sortStackItem), libraries });
}) satisfies RequestHandler;

export const POST = (async ({ request, locals }) => {
  const user = getConnectedUserOrThrow(locals);

  const { novel } = await request.json();

  try {
    const result = await importNovel(novel, user);
    return json(result);
  } catch (e) {
    console.error('[POST:stack.json]', e);
    error(500, 'this is a failed');
  }
}) satisfies RequestHandler;

export const PUT = (async ({ request, locals }) => {
  const { novel } = await request.json();
  const user = getConnectedUserOrThrow(locals);

  const { status, id } = novel;

  if (!isStatus(status)) {
    error(400, { message: `status value: "${status}" not found` });
  }

  const result = await novelModel.get(id);
  if (!result) {
    error(400, { message: 'Novel not found' });
  }

  await novelsStackModel.save({
    idUser: user.id,
    idNovel: result.id,
    idStatus: STATUSES[status]
  });

  return json({ status });
}) satisfies RequestHandler;

export const DELETE = (async ({ request, locals }) => {
  const user = getConnectedUserOrThrow(locals);
  const { novel } = await request.json();

  const idNovel = get(novel, 'id');

  if (!idNovel) {
    error(400, { message: 'missing novel id' });
  }

  await novelsStackModel.delete({ idUser: user.id, idNovel });

  return json({ success: true });
}) satisfies RequestHandler;
