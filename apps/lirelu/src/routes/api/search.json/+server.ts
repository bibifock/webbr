import { json, error } from '@sveltejs/kit';

import {
  novelsStackModel,
  searchModel,
  STATUSES_VALUES
} from '@webbr/lirelu-db';
import { parisBibli } from '@webbr/api-bibli';
import type { APINovel } from '@webbr/api-bibli';

import {
  type NovelStatus,
  type Novel,
  type Saga,
  type SagaNovel,
  isSaga,
  isSagaNovel
} from '$ui/types';

import type { RequestHandler } from './$types';

const populateResult = async ({
  user,
  result
}: {
  user: { id: number; name: string };
  result: APINovel[];
}): Promise<Array<Novel | Saga>> => {
  const isbns = result.map((i) => i.isbn);
  const books = await novelsStackModel.allByISBN({ isbns, user });

  const novels: Array<APINovel & { status?: NovelStatus }> = result.map(
    (apiNovel) => {
      const { idStatus, idNovel } =
        books.find((v) => v.isbn === apiNovel.isbn) ?? {};

      return {
        ...apiNovel,
        id: idNovel,
        status: idStatus
          ? (STATUSES_VALUES[idStatus] as NovelStatus)
          : undefined
      };
    }
  );

  const items = novels.reduce<Array<Novel | Saga>>(
    (acc, item): Array<Novel | Saga> => {
      const novel: Novel = {
        ...item,
        books: item.books.map(({ thumb, holding, ...b }) => ({
          ...b,
          holding,
          details: JSON.stringify({ holding }),
          image: thumb,
          bookcases: []
        }))
      };

      if (item?.saga === undefined) {
        return [...acc, novel];
      }
      const { saga: apiSaga } = item;

      const sagaNovel: SagaNovel = {
        ...novel,
        sagaVolume: apiSaga.tome,
        saga: {
          ...apiSaga,
          href: novel.url
        }
      };

      const sagaIndex = acc.findIndex(
        (v) =>
          isSaga(v) && v.title.toLowerCase() === apiSaga.title.toLowerCase()
      );

      if (sagaIndex === -1) {
        const saga: Saga = {
          ...apiSaga,
          volumes: [apiSaga.tome],
          tome: apiSaga.tome,
          novels: [sagaNovel]
        };
        return [...acc, saga];
      }

      const saga = acc[sagaIndex] as Saga;
      const sagaNovelIndex = saga.novels.findIndex(
        (n) => n.sagaVolume === sagaNovel.sagaVolume
      );

      if (sagaNovelIndex === -1) {
        acc[sagaIndex] = {
          ...saga,
          volumes: [...saga.volumes, sagaNovel.saga.tome],
          novels: [...saga.novels, sagaNovel]
        };
        return acc;
      }

      saga.novels[sagaNovelIndex].books = [
        ...saga.novels[sagaNovelIndex].books,
        ...novel.books
      ];

      return acc;
    },
    [] as Array<Saga | Novel>
  );

  return items;
};

export const POST = (async ({ locals, request }) => {
  const user = locals.session.data;
  const { search } = await request.json();

  try {
    const result = await parisBibli.search(search);

    if (user?.id === undefined) {
      return json({ items: result });
    }

    const followed = await searchModel.isSearchFollowedByUser({ search, user });
    const items = await populateResult({ user, result });

    return json({ items, followed });
  } catch (e) {
    console.error('[POST search.json]', e);
    error(500, 'sorry, this is not possible');
  }
}) satisfies RequestHandler;
