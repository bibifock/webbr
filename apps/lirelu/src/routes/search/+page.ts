import type { PageLoad } from './$types';

export const load: PageLoad = async ({ url, parent }) => {
  const { session } = await parent();
  return {
    search: url.searchParams.get('s') ?? undefined,
    logged: session?.id !== undefined
  };
};
