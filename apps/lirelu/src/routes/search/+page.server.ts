import { getBookBookcases, searchFollow, searchUnfollow } from '$lib/actions';

import type { Actions } from './$types';

export const actions = {
  searchFollow,
  searchUnfollow,
  getBookBookcases
} satisfies Actions;
