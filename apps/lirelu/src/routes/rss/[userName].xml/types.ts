import type { Novel as NovelDb, SearchFollowed } from '@webbr/lirelu-db';

export enum FeedItemTypeEnum {
  Search = 'Search',
  Novel = 'Novel',
  Loan = 'Loan'
}

export type FeedSearchFollowedItem = SearchFollowed & {
  type: FeedItemTypeEnum.Search;
  updated: Date;
};
export type FeedNovelItem = NovelDb & {
  type: FeedItemTypeEnum.Novel;
  updated: Date;
};
export type FeedLoanItem = NovelDb & {
  type: FeedItemTypeEnum.Loan;
  updated: Date;
};

export type FeedItem = FeedSearchFollowedItem | FeedNovelItem | FeedLoanItem;
