import loGet from 'lodash/get';

import type { Novel, User } from '@webbr/lirelu-db';

import { HOSTNAME } from '$core/config';

import { FeedItemTypeEnum } from './types';

import type {
  FeedItem,
  FeedLoanItem,
  FeedNovelItem,
  FeedSearchFollowedItem
} from './types';

function toRFC3339(date: Date): string {
  function pad(n: number) {
    return n < 10 ? '0' + n : n;
  }

  function timezoneOffset(offset: number) {
    if (offset === 0) {
      return 'Z';
    }
    const sign = offset > 0 ? '-' : '+';
    offset = Math.abs(offset);
    return sign + pad(Math.floor(offset / 60)) + ':' + pad(offset % 60);
  }

  return (
    date.getFullYear() +
    '-' +
    pad(date.getMonth() + 1) +
    '-' +
    pad(date.getDate()) +
    'T' +
    pad(date.getHours()) +
    ':' +
    pad(date.getMinutes()) +
    ':' +
    pad(date.getSeconds()) +
    timezoneOffset(date.getTimezoneOffset())
  );
}

function replaceSpecialChars(value: string) {
  return value
    .replaceAll('>', '&gt;')
    .replaceAll('<', '&lt;')
    .replaceAll("'", '&apos;')
    .replaceAll('"', '&quot;')
    .replaceAll('&', '&amp;');
}

function renderNovelTitle({ novel, saga }: Novel) {
  const title = saga
    ? `${saga.title} #${novel.sagaVolume} - ${novel.title}`
    : novel.title;

  return replaceSpecialChars(title);
}

function renderLoanTitle({ loan, ...rest }: Novel) {
  const title = renderNovelTitle(rest);
  if (!loan) {
    return title;
  }

  if (loan.isActive) {
    return `${title} - must be return ${loan.whenBack} at ${loan.library.name}`;
  }

  return `${title} - was returned at ${loan.library.name}`;
}

type RenderXmlAtomFeedProps = {
  user: User;
  feedItems: FeedItem[];
};

const renderFeedNovelItem = (item: FeedNovelItem | FeedLoanItem) => `
  <item>
  <title>${
    item.type === FeedItemTypeEnum.Loan
      ? renderLoanTitle(item)
      : renderNovelTitle(item)
  }</title>
    <id>${item.novel.id}</id>
    <link>${`${HOSTNAME}novel/${item.novel.id}`}</link>
    <image>${`${HOSTNAME}/img/book/${item.novel.id}.jpg`}</image>
    <date>${toRFC3339(item.updated)}</date>
    <pubDate>${toRFC3339(item.updated)}</pubDate>
    <description>
      <div
        xmlns="http://www.w3.org/1999/xhtml"
        style="display: flex; align-items: center;"
      >
        <div style='display: flex; flex-direction: column;'>
          <div>
            <span>${renderNovelTitle(item)}</span>
            <strong style='margin-left: 10px'>${item.author.name}</strong>
          </div>
          <br />
          <span>${replaceSpecialChars(
            loGet(item, 'novel.description', '')
          )}</span>
        </div>
      </div>
    </description>
  </item>
`;
const renderFeedSearchItem = (item: FeedSearchFollowedItem) => {
  const params = new URLSearchParams({
    s: item.search
  });
  const random = item.lastUpdate?.replace(/[^0-9]*/g, '');

  return `
  <item>
    <title>${item.search} have ${item.nbResults} new results</title>
    <id>${item.id}</id>
    <link>${`${HOSTNAME}search?${params.toString()}#${random}`}</link>
    <date>${item.lastUpdate}</date>
    <pubDate>${item.lastUpdate}</pubDate>
    <description></description>
  </item>
`;
};

export function renderXmlAtomFeed({ user, feedItems }: RenderXmlAtomFeedProps) {
  return `<?xml version="1.0" encoding="utf-8"?>
    <rss version="2.0">
      <channel xmlns="http://www.w3.org/2005/Atom">
        <title>${user.name} watched novels</title>
        <link rel='self' href='${HOSTNAME}' />
        <updated>${toRFC3339(new Date())}</updated>
        ${feedItems
          .map((feedItem) => {
            if (feedItem.type === FeedItemTypeEnum.Search) {
              return renderFeedSearchItem(feedItem);
            }

            return renderFeedNovelItem(feedItem);
          })
          .join('\n')}
      </channel>
    </rss>`;
}
