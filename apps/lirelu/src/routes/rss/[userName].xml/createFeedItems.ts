import type { Novel as NovelDb, SearchFollowed } from '@webbr/lirelu-db';

import { FeedItemTypeEnum } from './types';

import type {
  FeedItem,
  FeedLoanItem,
  FeedNovelItem,
  FeedSearchFollowedItem
} from './types';

const getFeedSearchItem = (
  searchFollowed: SearchFollowed
): FeedSearchFollowedItem => ({
  ...searchFollowed,
  updated: new Date(searchFollowed.lastUpdate),
  type: FeedItemTypeEnum.Search
});
const getFeedNovelItem = (novel: NovelDb): FeedNovelItem => ({
  ...novel,
  updated: new Date(novel.novel.created),
  type: FeedItemTypeEnum.Novel
});

const getFeedLoanItem = (novel: NovelDb): FeedLoanItem => ({
  ...novel,
  loan: novel.loan
    ? {
        ...novel.loan,
        whenBack: new Date(parseInt(novel.loan.whenBack))
          .toUTCString()
          .replace(/ \d{2}:.+/, '')
      }
    : undefined,
  updated: new Date(novel?.loan?.updated ?? ''),
  type: FeedItemTypeEnum.Loan
});

export const createFeedItems = ({
  novels,
  searchs,
  loans
}: {
  novels: NovelDb[];
  searchs: SearchFollowed[];
  loans: NovelDb[];
}): FeedItem[] => [
  ...[
    ...novels.map((n) => getFeedNovelItem(n)),
    ...loans.map((l) => getFeedLoanItem(l))
  ].sort((a, b) => {
    if (a.updated > b.updated) return -1;
    if (a.updated < b.updated) return 1;
    return 0;
  }),
  ...searchs.map((s) => getFeedSearchItem(s))
];
