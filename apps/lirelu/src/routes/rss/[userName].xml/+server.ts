import { error } from '@sveltejs/kit';

import {
  novelsStackModel,
  userModel,
  searchFollowedModel,
  loanModel
} from '@webbr/lirelu-db';

import { createFeedItems } from './createFeedItems';
import { renderXmlAtomFeed } from './renderXmlAtomFeed';

import type { RequestHandler } from '@sveltejs/kit';

export const GET = (async ({ params }) => {
  const { userName } = params;

  const user = await userModel.getByProps({ name: userName });

  if (!user) {
    error(
      400,
      'Merci Dolorès, mais je ne me servirai que de la brosse à dents...'
    );
  }

  const novels = await novelsStackModel.getNewNovelsForUser(user);
  const loans = await loanModel.getLoansForUser(user);
  const searchs = await searchFollowedModel.getFollowedNewResult(user);
  const feedItems = createFeedItems({ novels, searchs, loans });
  const feed = renderXmlAtomFeed({ user, feedItems });

  const headers = {
    'Cache-Control': `public, max-age=0, must-revalidate`,
    'Content-Type': 'application/xml'
  };

  return new Response(feed, { headers });
}) satisfies RequestHandler;
