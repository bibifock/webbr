import { error, json } from '@sveltejs/kit';

import { userModel } from '@webbr/lirelu-db';

import type { User } from '$ui/types';
import type { RequestEvent } from '@sveltejs/kit';

const getUser = async (user: User) => {
  const result = await userModel.get(user.id);

  if (!result) {
    throw Error('user not found');
  }

  return result;
};

/**
 * get session user
 * @param {object} req
 *
 * @return {promise}
 */
const get = ({ locals }: RequestEvent) => {
  const user = locals.session.data;

  return getUser(user)
    .then((res) => json({ user: res }))
    .catch(() => {
      error(400, { message: 'user not found' });
    });
};

/**
 * update user information
 * @param {object} req
 * @param {object} res
 *
 * @return {Promise}
 */
const update = async ({ request }: RequestEvent) => {
  const body = await request.json();
  const { id, email, name } = body;
  if (!id || !email || !name) {
    error(400, { message: 'missing user informations' });
  }

  try {
    await getUser({ id, name });

    await userModel.save(body);

    return json({});
  } catch (e) {
    // TODO improve typing
    error(500, e as { message: string });
  }
};

export default {
  get,
  update
};
