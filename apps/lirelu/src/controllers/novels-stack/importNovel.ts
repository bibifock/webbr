import { parisBibli } from '@webbr/api-bibli';
import { saveNovel } from '@webbr/lirelu-db';
import type { APINovel } from '@webbr/api-bibli';

import { UPLOAD_BOOKS_PATH, UPLOAD_DIR } from '$core/config';


/**
 * import novel and saga from paris bibli in db and user stack
 * @param {object} novel
 * @param {objetc} user
 *
 * @return {promise}
 */
export const importNovel = async (novel: APINovel, user = undefined) => {
  const booksInfos = await parisBibli.extractBooksInfos(novel);

  if (!booksInfos) {
    console.warn('importNovel', 'nothing found for this novel', novel);
    return null;
  }

  return saveNovel(booksInfos, user, {
    booksPath: UPLOAD_BOOKS_PATH,
    uploadDir: UPLOAD_DIR,
    getBookcases: parisBibli.getBookcases
  });
};
