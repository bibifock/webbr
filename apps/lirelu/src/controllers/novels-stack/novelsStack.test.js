import { resetDb, cleanTables, user } from 'utils/testing/database';
import * as downloadFile from 'utils/downloadFile';

import { parisBibli } from '@webbr/api-bibli';
import {
  authorModel,
  STATUS_TO_READ,
  bookModel,
  bookcaseModel,
  libraryModel,
  novelModel,
  novelsStackModel,
  sagaModel
} from '@webbr/lirelu-db';

import novelsStack from './novelsStack';
import * as utils from './utils';
import { basket, novel, holdings } from './test.utils';

const { getBookImg } = utils;

describe('$controllers/novels-stack', () => {
  describe('add', () => {
    const req = {
      session: { user },
      body: { novel }
    };

    let res;
    beforeEach(async () => {
      res = {
        succ: jest.fn()
      };

      jest.spyOn(parisBibli, 'search').mockResolvedValue([
        novel,
        {
          ...novel,
          title: 'Le fou prend le roi',
          saga: {
            ...novel.saga,
            tome: 2
          },
          books: [
            {
              ...novel.books[0],
              isbn: '201',
              holding: {
                id: '1010152',
                base: 'SYRACUSE'
              }
            }
          ]
        }
      ]);

      jest.spyOn(parisBibli, 'getHoldings').mockResolvedValue(holdings);

      jest.spyOn(downloadFile, 'default').mockResolvedValue(true);
    });

    afterEach(() => {
      downloadFile.default.mockRestore();
      parisBibli.getHoldings.mockRestore();
      parisBibli.search.mockRestore();
    });

    describe('when book not in db should import', () => {
      beforeEach(async () => {
        await resetDb();

        await novelsStack.add(req, res);
      });

      describe('novel infos', () => {
        it('author', async () => {
          const results = await authorModel.allByProps({ name: novel.author });
          expect(results).toHaveLength(1);
          expect(results[0].name).toEqual(novel.author);
        });

        it('saga', async () => {
          const results = await sagaModel.allByProps({
            title: novel.saga.title
          });
          expect(results).toHaveLength(1);

          expect(results[0].title).toEqual(novel.saga.title);
        });

        it('novel', async () => {
          const { id: idAuthor } = await authorModel.getByProps({
            name: novel.author
          });
          const { id: idSaga } = await sagaModel.getByProps({
            title: novel.saga.title
          });

          const results = await novelModel.allByProps({
            idSaga,
            idAuthor,
            title: novel.title
          });

          expect(results).toHaveLength(1);

          expect(results[0].sagaVolume).toEqual(parseInt(novel.saga.tome));
        });
      });

      describe('all books informations', () => {
        it('book', async () => {
          const { id: idNovel } = await novelModel.getByProps({
            title: novel.title
          });
          const books = await bookModel.allByProps({ idNovel });

          expect(books).toHaveLength(novel.books.length);

          books.forEach((book) => {
            const src = novel.books.find(({ isbn }) => isbn === book.isbn);
            expect(book).toEqual(
              expect.objectContaining({
                image: getBookImg(book, '.jpg'),
                edition: src.edition,
                details: JSON.stringify({ holding: src.holding })
              })
            );
          });
        });

        it('library', async () => {
          const libraries = await libraryModel.all();
          expect(libraries).toHaveLength(holdings.length);

          holdings.forEach(({ site: name, siteLink: url }) => {
            expect(libraries).toContainEqual(
              expect.objectContaining({ name, url })
            );
          });
        });

        it('bookcase', async () => {
          const libraries = await libraryModel.all();
          const { id: isAvailableCase } = libraries.find(
            ({ name }) => holdings[0].site === name
          );

          const books = await bookModel.all();

          for (const book of books) {
            const bookcases = await bookcaseModel.allByProps({
              idBook: book.id
            });
            expect(bookcases).toHaveLength(2);

            bookcases.forEach(({ idLibrary, ...bc }) => {
              const holding = holdings[idLibrary === isAvailableCase ? 0 : 1];
              expect(bc).toEqual({
                idBook: book.id,
                isAvailable: holding.isAvailable ? 1 : 0,
                isReservable: holding.isReservable ? 1 : 0,
                whenBack: holding.whenBack || null,
                details: JSON.stringify({
                  cote: holding.cote,
                  status: holding.status
                })
              });
            });
          }
        });
      });

      it('should add novel in user basket', async () => {
        const { id: idNovel } = await novelModel.getByProps({
          title: novel.title
        });
        const inStack = await novelsStackModel.allByProps({ idUser: user.id });

        expect(inStack).toHaveLength(1);

        expect(inStack[0]).toEqual(
          expect.objectContaining({
            idUser: user.id,
            idNovel,
            idStatus: STATUS_TO_READ
          })
        );
      });
    });

    describe('when book already in db', () => {
      describe.each([
        ['without saga', { saga: null }],
        ['with saga', {}]
      ])('%s', (testTitle, novelExtraValues) => {
        const request = {
          ...req,
          body: {
            novel: { ...novel, ...novelExtraValues }
          }
        };

        beforeEach(async () => {
          await resetDb();

          await novelsStack.add(request, res);
          await cleanTables([novelsStackModel.config.table]);
          await novelsStack.add(request, res);
        });

        it('should not add novel', async () => {
          const results = await novelModel.allByProps({ title: novel.title });
          expect(results).toHaveLength(1);
        });

        it('should add novel in user basket', async () => {
          const { id: idNovel } = await novelModel.getByProps({
            title: novel.title
          });
          const inStack = await novelsStackModel.allByProps({
            idUser: user.id
          });

          expect(inStack).toHaveLength(1);

          expect(inStack[0]).toEqual(
            expect.objectContaining({
              idUser: user.id,
              idNovel,
              idStatus: STATUS_TO_READ
            })
          );
        });
      });
    });

    it('response should include new book infos', async () => {
      const succ = jest.fn();
      await novelsStack.add(req, { succ });
      const result = await novelModel.getByProps({ title: novel.title });
      const books = await bookModel.allByProps({ idNovel: result.id });

      expect(succ).toHaveBeenCalledWith(
        expect.objectContaining({
          ...result,
          books
        })
      );
    });
  });

  describe('del', () => {
    const makeRequest = async (body) => {
      const succ = jest.fn();
      const err = jest.fn();

      await novelsStack.del(
        {
          session: { user },
          body
        },
        { succ, err }
      );

      return { succ, err };
    };

    describe('should return an error when missing id novel', () => {
      it.each([undefined, null, {}, { id: null }])('%p', async (params) => {
        const { err, succ } = await makeRequest({ novel: params });

        expect(succ).not.toHaveBeenCalled();
        expect(err).toHaveBeenCalledWith('missing novel id');
      });
    });

    it('should return a success', async () => {
      const idNovel = 66;
      await novelsStackModel.save({
        idNovel,
        idUser: user.id,
        idStatus: STATUS_TO_READ
      });

      const { succ } = await makeRequest({ novel: { id: idNovel } });

      expect(succ).toHaveBeenCalled();

      const results = await novelsStackModel.allByProps({ idNovel });
      expect(results).toHaveLength(0);
    });
  });

  describe('importStack', () => {
    beforeEach(async () => {
      await resetDb();

      jest.spyOn(parisBibli, 'getLoggedRequest').mockResolvedValue({});
      jest.spyOn(parisBibli, 'getBasket').mockResolvedValue(basket);
      jest.spyOn(parisBibli, 'getLoans').mockResolvedValue([]);
      jest.spyOn(utils, 'importNovel').mockResolvedValue(true);
    });

    afterEach(() => {
      parisBibli.getBasket.mockRestore();
      parisBibli.getLoans.mockRestore();
      parisBibli.getLoggedRequest.mockRestore();
      utils.importNovel.mockRestore();
    });

    describe('should send an error when', () => {
      describe('missing user information', () => {
        let body, res;

        beforeEach(() => {
          body = {
            ...user,
            dateOfBirth: '12122032'
          };

          res = { err: jest.fn() };
        });

        it.each(['id', 'email', 'dateOfBirth'])('%p', async (field) => {
          delete body[field];
          await novelsStack.importStack({ body }, res);

          expect(res.err).toHaveBeenCalledWith(
            expect.stringContaining('missing bibli')
          );
        });

        it('user not found in db', async () => {
          body.id = 1664;

          await novelsStack.importStack({ body }, res);

          expect(res.err).toHaveBeenCalledWith(
            expect.stringContaining('user not found')
          );
        });
      });
    });

    describe('get basket content query', () => {
      let succ = jest.fn();
      beforeEach(async () => {
        const req = {
          body: {
            ...user,
            dateOfBirth: '12122032'
          }
        };

        const res = { succ };

        await novelsStack.importStack(req, res);
      });

      it('should import every book in basket', () => {
        basket.forEach((novel) =>
          expect(utils.importNovel).toHaveBeenCalledWith(
            novel,
            expect.objectContaining(user)
          )
        );
      });

      it('should return nb novel found', () => {
        expect(succ).toHaveBeenCalledWith({ nbNovels: basket.length });
      });
    });
  });
});
