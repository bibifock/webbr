import get from 'lodash/get';
import set from 'lodash/set';

import { STATUSES_VALUES } from '@webbr/lirelu-db';


import { parse } from '$utils/json';

import { sortStackItem } from './sortStackItem';

import type {
  Novel,
  Book,
  Library,
  Loan,
  Bookcase,
  Saga,
  NovelStatusEnum
} from '$ui/types';


const findById = (v: { id: number }) => (i: { id: number }) => i.id === v.id;
const statusValues = STATUSES_VALUES;

/**
 * @param {object} row
 * @param {array} libraries
 *
 * @return {object} loan informations
 */
const extractLoan = (
  row: { loan?: Loan & Bookcase },
  libraries: Library[]
): (Loan & { library: Library }) | undefined => {
  const loan = get(row, 'loan');
  if (!loan) {
    return undefined;
  }

  const { details, whenBack, ...rest } = loan;
  return {
    ...rest,
    // TODO improve this shit
    whenBack: `${parseInt(whenBack)}`,
    ...parse(details),
    library: libraries.find((l: Library) => l.id === loan.idLibrary) as Library
  };
};

// TODO improve it
type ReponseTree = ((Saga & { isSaga: boolean }) | Novel) & { id: number };

/**
 * parse all rows to make response
 * @param {array} rows
 *
 * @return {array}
 */
export const makeResponseTree = (
  result: Array<{
    saga: Saga & { id: number };
    stack?: Record<string, string>;
    author: string;
    library: Library;
    bookcase: Bookcase;
    loan?: Loan & Bookcase;
    book: Book & { id: number };
    novel: Novel & { id: number; library: Library };
  }>,
  libraries: Library[]
) =>
  result
    .reduce<ReponseTree[]>((arr, row) => {
      const { saga, stack = {}, author, library, bookcase: bookcaseRaw } = row;

      const loan: (Loan & { library: Library }) | undefined = extractLoan(
        row,
        libraries
      );

      const bookcase: Bookcase | undefined =
        bookcaseRaw && bookcaseRaw.details
          ? {
              ...bookcaseRaw,
              ...parse(bookcaseRaw.details),
              library
            }
          : undefined;

      const book: Book & { id: number } = { ...row.book };

      if (bookcase) {
        book.bookcases = [
          {
            ...bookcase,
            isAvailable: bookcase.status === 'En rayon'
          }
        ];
      }

      const novel = {
        ...row.novel,
        author: get(author, 'name') ?? '',
        thumb: book.image,
        status: get(statusValues, stack.idStatus) as NovelStatusEnum,
        books: [book],
        loan
      };

      const isSaga = !!saga;

      const finder = isSaga
        ? (el: Novel | (Saga & { isSaga: boolean })) =>
            'isSaga' in el && el?.isSaga && saga.id === el.id
        : findById(row.novel);
      const index = arr.findIndex(finder);

      if (index < 0) {
        arr.push(
          isSaga
            ? ({
                ...saga,
                novels: [novel],
                isSaga
              } as unknown as ReponseTree)
            : (novel as ReponseTree)
        );
        return arr;
      }
      let key: string = `${index}`;

      if (isSaga) {
        key += '.novels';
        const indexNovel = get(arr, key).findIndex(findById(novel));
        if (indexNovel < 0) {
          set(arr, key, get(arr, key).concat(novel));
          return arr;
        }
        // current novel index
        key += '.' + indexNovel;
      }

      // add loan if there wasn't already defined
      if (loan && !get(arr, key + '.loan')) {
        set(arr, key + '.loan', loan);
      }

      key += '.books';
      const indexBook = get(arr, key).findIndex(findById(book));
      if (indexBook < 0) {
        set(arr, key, get(arr, key).concat(book));
        return arr;
      }
      key += '.' + indexBook + '.bookcases';

      set(arr, key, get(arr, key).concat(bookcase));
      return arr;
    }, [])
    .sort(sortStackItem);
