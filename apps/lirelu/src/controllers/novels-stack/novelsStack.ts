import get from 'lodash/get';
import { json, error } from '@sveltejs/kit';

import {
  novelsStackModel,
  novelModel,
  userModel,
  STATUSES
} from '@webbr/lirelu-db';
import { parisBibli } from '@webbr/api-bibli';

import { importNovel } from './importNovel';

import type { RequestEvent } from '@sveltejs/kit';

/**
 * update novel status
 * @param {object} req
 *
 * @return {promise}
 */
const update = async ({ request, locals }: RequestEvent) => {
  const { novel } = await request.json();
  const user = locals.session.data;

  const { status, id } = novel;
  if (!STATUSES[status]) {
    error(400, { error: `status value: "${status}" not found` });
  }

  const result = await novelModel.get(id);
  if (!result) {
    error(400, { error: 'Novel not found' });
  }

  await novelsStackModel.save({
    idUser: user.id,
    idNovel: result.id,
    idStatus: STATUSES[status]
  });

  return json({ status });
};

/**
 * remove novel from user list
 * @param {object} req
 *
 * @return {promise}
 */
const del = async ({ request, locals }: RequestEvent) => {
  const user = locals.session.data;
  const { novel } = await request.json();

  const idNovel = get(novel, 'id');

  if (!idNovel) {
    error(400, { error: 'missing novel id' });
    return;
  }

  await novelsStackModel.delete({ idUser: user.id, idNovel });

  return json({ success: true });
};

/**
 * import all books from bibli basket
 *  @param {object} req
 *  @param {object} res
 *
 *  @return {promise}
 */
const importStack = async ({ request }) => {
  const { id, email, dateOfBirth } = await request.json();

  if (!id || !email || !dateOfBirth) {
    error(400, { error: 'missing bibli paris auth informations' });
  }

  const user = await userModel.get(id);
  if (!user) {
    error(400, { error: 'user not found' });
  }

  const basket = await parisBibli.getUserBasket({
    username: email,
    password: dateOfBirth
  });

  for (const item of basket) {
    await importNovel(item, user);
  }

  return json({ nbNovels: basket.length });
};

export default {
  update,
  del,
  importStack
};
