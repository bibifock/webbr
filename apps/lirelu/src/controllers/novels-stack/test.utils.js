export const basket = [
  {
    _sorted: 'chevauche-brumes',
    author: 'Thibaud Latil-Nicolas',
    title: 'Chevauche-Brumes',
    uid: 1125341,
    description:
      "Les aventures d'une troupe de mercenaires, les lansquenets, qui au sortir d'une énième bataille se voient missionner par leur suzerain pour enquêter sur un phénomène magique appelé la Brume d'encre. Cet orage qui, telle une montagne, bouche l'horizon à la frontière nordique, a fait naître en effet des créatures monstrueuses menaçant la sécurité de l'Etat. ©Electre 2019<br/><br/>",
    isbn: '9782354087104',
    thumb:
      'https://covers.archimed.fr/Cover/VPCO/MONO/baJB4os-i0IgD9jlrpufBA2/978-2-35408-710-4/MEDIUM',
    books: [
      {
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/baJB4os-i0IgD9jlrpufBA2/978-2-35408-710-4/MEDIUM',
        isbn: '9782354087104',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1201758/chevauche-brumes',
        edition: 'Icares',
        holding: {
          id: '1201758',
          base: 'SYRACUSE'
        }
      }
    ]
  },
  {
    _sorted: 'chevauche-brumes2les flots sombres',
    author: 'Thibaud Latil-Nicolas',
    title: 'Les flots sombres',
    uid: 1178818,
    saga: {
      query: 'SeriesTitle_exact:"Chevauche-Brumes"',
      title: 'Chevauche-Brumes',
      tome: '2',
      href: '/search?s=SeriesTitle_exact:"Chevauche-Brumes"'
    },
    description:
      'Les Chevauche-Brumes ont quitté les légions royales du Bleu-Royaume pour traquer les créatures maléfiques issues du brouillard noir. Ces dernières sont désormais dispersées aux quatre coins du monde où elles attaquent les populations civiles sans défense. ©Electre 2020<br/><br/>',
    isbn: '9782354087708',
    thumb:
      'https://covers.archimed.fr/Cover/VPCO/MONO/Tb-wr0kaxOrabg8EdR9WHg2/978-2-35408-770-8/MEDIUM',
    books: [
      {
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/Tb-wr0kaxOrabg8EdR9WHg2/978-2-35408-770-8/MEDIUM',
        isbn: '9782354087708',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1240689/les-flots-sombres',
        edition: 'Icares',
        holding: {
          id: '1240689',
          base: 'SYRACUSE'
        }
      }
    ]
  },
  {
    _sorted: 'chronique du tueur de roi2la peur du sage. seconde partie',
    author: 'Patrick Rothfuss ',
    title: 'La peur du sage. Seconde partie',
    uid: 581923,
    saga: {
      query: 'SeriesTitle_exact:"Chronique du tueur de roi"',
      title: 'Chronique du tueur de roi',
      tome: '2',
      href: '/search?s=SeriesTitle_exact:"Chronique du tueur de roi"'
    },
    description:
      'Kvothe poursuit son récit. Il raconte les intrigues politiques à la cour de Maer, sa vie de mercenaire sur les routes à la poursuite des bandits, sa rencontre avec Felurian, la reine des Faes, et son apprentissage douloureux chez les Adems.<br/><br/>',
    isbn: '9782352946588',
    thumb:
      'https://covers.archimed.fr/Cover/VPCO/MONO/2tTRK5YTFiJ9jAbmP0Qz4w2/978-2-35294-658-8/MEDIUM',
    books: [
      {
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/2tTRK5YTFiJ9jAbmP0Qz4w2/978-2-35294-658-8/MEDIUM',
        isbn: '9782352946588',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/917696/la-peur-du-sage-seconde-partie',
        edition: 'Collection dirigée par Stéphane Marsan',
        holding: {
          id: '917696',
          base: 'SYRACUSE'
        }
      }
    ]
  },
  {
    _sorted:
      'comment faire tomber un dictateur quand on est seul, tout petit, et sans armes',
    author: 'Srđa Popović ',
    title:
      'Comment faire tomber un dictateur quand on est seul, tout petit, et sans armes',
    uid: 783778,
    description:
      "Apôtre de la lutte non violente, l'auteur, qui fit tomber Milosevic avec le mouvement Otpor !, explique ce qui fonctionne ou non dans les mouvements de masse. Défendant l'efficacité de l'humour, il affirme qu'il ne suffit pas de protester ou de faire la révolution, mais qu'il est essentiel d'avoir une vision claire du bon usage de la liberté. ©Electre 2015<br/><br/>",
    isbn: '9782228913751',
    thumb:
      'https://covers.archimed.fr/Cover/VPCO/MONO/XurIAIIv8wN4VChLi5fWQw2/978-2-228-91375-1/MEDIUM',
    books: [
      {
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/XurIAIIv8wN4VChLi5fWQw2/978-2-228-91375-1/MEDIUM',
        isbn: '9782228913751',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1037599/comment-faire-tomber-un-dictateur-quand-on-est-seul-tout-petit-et-sans-armes',
        holding: {
          id: '1037599',
          base: 'SYRACUSE'
        }
      }
    ]
  }
];

export const novel = {
  _sorted: "le bâtard de kosigan1l'ombre du pouvoir",
  author: 'Fabien Cerutti',
  title: "L'ombre du pouvoir",
  uid: 106102,
  saga: {
    query: 'SeriesTitle_exact:%22Le+b%C3%A2tard+de+Kosigan%22',
    title: 'Le bâtard de Kosigan',
    tome: '1',
    href: '/search?s=SeriesTitle_exact:%22Le+b%C3%A2tard+de+Kosigan%22'
  },
  description:
    "En 1339 Pierre Cordwain de Kosigan, dit le bâtard, est doté de pouvoirs étranges. A la tête d'une compagnie de mercenaires d'élite, il est chargé de s'emparer de la Champagne des princesses elfiques.<br/><br/>",
  isbn: '101',
  thumb:
    'https://covers.archimed.fr/Cover/VPCO/MONO/d22sXOSPos-Xp4UVRtA-BA2/978-2-35408-172-0/MEDIUM',
  books: [
    {
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/gMGM4YOrCYx_k-UaqWh4_g2/978-2-35408-172-0/MEDIUM',
      isbn: '101',
      url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/956281/l-ombre-du-pouvoir',
      edition: 'Icares (Paris)',
      holding: {
        id: '956281',
        base: 'SYRACUSE'
      }
    },
    {
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/ABiElbdBNz7oxak9oOrn1g2/978-2-07-079280-1/MEDIUM',
      isbn: '102',
      url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1111738/l-ombre-du-pouvoir',
      edition: 'Folio. Science-fiction',
      holding: {
        id: '1111738',
        base: 'SYRACUSE'
      }
    }
  ]
};

export const holdings = [
  {
    isAvailable: true,
    isReservable: true,
    site: 'bibli-1',
    siteLink: 'https://bibli-1',
    status: 'en rayon',
    whenBack: undefined,
    cote: 'SF En haut à gauche'
  },
  {
    isAvailable: false,
    isReservable: false,
    site: 'bibli-2',
    siteLink: 'https://bibli-2',
    status: 'impossible à avoir',
    whenBack: 'never ever',
    cote: "aucune utilité car tu ne l'auras jamais"
  }
];

export const novelWithLoanIssue = {
  result: [
    {
      author: {
        id: 69,
        name: 'Terry Goodkind'
      },
      book: {
        id: 169,
        idNovel: 126,
        isbn: '9782811211189',
        edition: "L'Ã©pÃ©e de vÃ©ritÃ©",
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1054040/la-premiere-lecon-du-sorcier',
        image: '/img/book/169.jpg',
        details: '{"holding":{"id":"1054040","base":"SYRACUSE"}}'
      },
      bookcase: {
        idLibrary: 8,
        idBook: 169,
        whenBack: '04/09/2021',
        details: '{"cote":"ROMAN FANTASY GOO","status":"EmpruntÃ©"}'
      },
      library: {
        id: 8,
        name: '75005 - Rainer Maria Rilke',
        url: 'https://www.paris.fr/equipements/bibliotheque-rainer-maria-rilke-1672'
      },
      novel: {
        id: 126,
        title: 'La premiÃ¨re leÃ§on du sorcier',
        idAuthor: 69,
        idSaga: 24,
        sagaVolume: 1,
        created: '2021-08-16 19:33:38'
      },
      saga: {
        id: 24,
        title: "L'Ã©pÃ©e de vÃ©ritÃ©",
        nbNovels: 15
      },
      stack: {
        idUser: 1,
        idNovel: 126,
        idStatus: 1
      }
    },
    {
      author: {
        id: 69,
        name: 'Terry Goodkind'
      },
      book: {
        id: 168,
        idNovel: 126,
        isbn: '9782352944652',
        edition: 'Collection dirigÃ©e par StÃ©phane Marsan',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/799379/la-premiere-lecon-du-sorcier',
        image: '/img/book/168.jpg',
        details: '{"holding":{"id":"799379","base":"SYRACUSE"}}'
      },
      bookcase: {
        idLibrary: 4,
        idBook: 168,
        isAvailable: 1,
        details: '{"cote":"SF GOO EPEE DE VERITE 1","status":"En rayon"}'
      },
      library: {
        id: 4,
        name: '75003 - Marguerite Audoux',
        url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
      },
      novel: {
        id: 126,
        title: 'La premiÃ¨re leÃ§on du sorcier',
        idAuthor: 69,
        idSaga: 24,
        sagaVolume: 1,
        created: '2021-08-16 19:33:38'
      },
      saga: {
        id: 24,
        title: "L'Ã©pÃ©e de vÃ©ritÃ©",
        nbNovels: 15
      },
      stack: {
        idUser: 1,
        idNovel: 126,
        idStatus: 1
      },
      loan: {
        idUser: 1,
        idBook: 168,
        idLibrary: 17,
        whenBack: '1630965600000.0',
        details:
          '{"renewData":{"HoldingId":"32272114740894","RecordId":"799379","Id":"32272114740894"},"canRenew":false,"cannotRenewReason":"Exemplaire dÃ©jÃ  prolongÃ© aujourd\'hui","isLate":false,"isSoonLate":false,"status":"En cours"}'
      }
    }
  ],
  libraries: [
    {
      id: 1,
      name: '75000 - RÃ©serve centrale',
      url: 'https://bibliotheques.paris.fr/la-reserve-centrale.aspx'
    },
    {
      id: 2,
      name: '75001 - CanopÃ©e la fontaine',
      url: 'https://www.paris.fr/equipements/mediatheque-de-la-canopee-la-fontaine-16634'
    },
    {
      id: 3,
      name: '75002 - Charlotte Delbo',
      url: 'https://www.paris.fr/equipements/bibliotheque-charlotte-delbo-1664'
    },
    {
      id: 4,
      name: '75003 - Marguerite Audoux',
      url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
    },
    {
      id: 56,
      name: '75003 - Marguerite Audoux (fermÃ©e)',
      url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
    },
    {
      id: 5,
      name: '75004 - Arthur Rimbaud',
      url: 'https://www.paris.fr/equipements/bibliotheque-arthur-rimbaud-3'
    },
    {
      id: 6,
      name: '75005 - Buffon',
      url: 'https://www.paris.fr/equipements/bibliotheque-buffon-1682'
    },
    {
      id: 54,
      name: '75005 - Heure joyeuse',
      url: 'https://www.paris.fr/equipements/bibliotheque-l-heure-joyeuse-2882'
    },
    {
      id: 7,
      name: '75005 - Mohammed Arkoun',
      url: 'https://www.paris.fr/equipements/bibliotheque-mohammed-arkoun-1668'
    },
    {
      id: 8,
      name: '75005 - Rainer Maria Rilke',
      url: 'https://www.paris.fr/equipements/bibliotheque-rainer-maria-rilke-1672'
    },
    {
      id: 9,
      name: '75006 - AndrÃ© Malraux',
      url: 'https://www.paris.fr/equipements/bibliotheque-andre-malraux-1691'
    },
    {
      id: 10,
      name: '75007 - AmÃ©lie',
      url: 'https://www.paris.fr/equipements/bibliotheque-amelie-1692'
    },
    {
      id: 11,
      name: '75007 - Saint-Simon',
      url: 'https://www.paris.fr/equipements/bibliotheque-saint-simon-1693'
    },
    {
      id: 12,
      name: '75008 - Europe',
      url: 'https://www.paris.fr/equipements/bibliotheque-europe-1704'
    },
    {
      id: 13,
      name: '75009 - Drouot',
      url: 'https://www.paris.fr/equipements/bibliotheque-drouot-1719'
    },
    {
      id: 14,
      name: '75009 - Louise Walser-Gaillard (ex Chaptal)',
      url: 'https://www.paris.fr/equipements/bibliotheque-louise-walser-gaillard-ex-chaptal-6094'
    },
    {
      id: 15,
      name: '75009 - Valeyre',
      url: 'https://www.paris.fr/equipements/bibliotheque-valeyre-1711'
    },
    {
      id: 16,
      name: '75010 - FranÃ§ois Villon',
      url: 'https://www.paris.fr/equipements/bibliotheque-francois-villon-1722'
    },
    {
      id: 17,
      name: '75010 - FranÃ§oise Sagan',
      url: 'https://www.paris.fr/equipements/mediatheque-francoise-sagan-8695'
    },
    {
      id: 18,
      name: '75010 - Lancry',
      url: 'https://www.paris.fr/equipements/bibliotheque-lancry-1721'
    },
    {
      id: 19,
      name: '75011 - Parmentier',
      url: 'https://www.paris.fr/equipements/bibliotheque-parmentier-1723'
    },
    {
      id: 20,
      name: '75011 - Violette Leduc ex Faidherbe',
      url: 'https://www.paris.fr/equipements/mediatheque-violette-leduc-ex-bibliotheque-faidherbe-1724'
    },
    {
      id: 21,
      name: '75012 - HÃ©lÃ¨ne Berr',
      url: 'https://www.paris.fr/equipements/mediatheque-helene-berr-1726'
    },
    {
      id: 22,
      name: '75012 - Saint-Eloi',
      url: 'https://www.paris.fr/equipements/bibliotheque-saint-eloi-1725'
    },
    {
      id: 23,
      name: '75013 - GlaciÃ¨re Marina TsvetaÃ¯eva',
      url: 'https://www.paris.fr/equipements/bibliotheque-glaciere-marina-tsvetaieva-1730'
    },
    {
      id: 24,
      name: '75013 - Italie',
      url: 'https://www.paris.fr/equipements/bibliotheque-italie-1729'
    },
    {
      id: 25,
      name: '75013 - Jean-Pierre Melville',
      url: 'https://www.paris.fr/equipements/mediatheque-jean-pierre-melville-1728'
    },
    {
      id: 26,
      name: '75014 - AimÃ© CÃ©saire',
      url: 'https://www.paris.fr/equipements/bibliotheque-aime-cesaire-1732'
    },
    {
      id: 27,
      name: '75014 - BenoÃ®te Groult',
      url: 'https://www.paris.fr/equipements/bibliotheque-benoite-groult-19532'
    },
    {
      id: 28,
      name: '75014 - Georges Brassens',
      url: 'https://www.paris.fr/equipements/bibliotheque-georges-brassens-1733'
    },
    {
      id: 29,
      name: '75015 - AndrÃ©e Chedid',
      url: 'https://www.paris.fr/equipements/bibliotheque-andree-chedid-1734'
    },
    {
      id: 30,
      name: '75015 - Gutenberg',
      url: 'https://www.paris.fr/equipements/bibliotheque-gutenberg-1736'
    },
    {
      id: 31,
      name: '75015 - Marguerite Yourcenar',
      url: 'https://www.paris.fr/equipements/mediatheque-marguerite-yourcenar-6218'
    },
    {
      id: 32,
      name: '75015 - Vaugirard',
      url: 'https://www.paris.fr/equipements/bibliotheque-vaugirard-1735'
    },
    {
      id: 33,
      name: '75016 - Germaine Tillion',
      url: 'https://www.paris.fr/equipements/bibliotheque-germaine-tillion-1737'
    },
    {
      id: 34,
      name: '75016 - Musset',
      url: 'https://www.paris.fr/equipements/bibliotheque-musset-1738'
    },
    {
      id: 35,
      name: '75017 - Batignolles',
      url: 'https://www.paris.fr/equipements/bibliotheque-batignolles-1739'
    },
    {
      id: 36,
      name: '75017 - Colette Vivier',
      url: 'https://www.paris.fr/equipements/bibliotheque-colette-vivier-1740'
    },
    {
      id: 37,
      name: '75017 - Edmond Rostand',
      url: 'https://www.paris.fr/equipements/mediatheque-edmond-rostand-1741'
    },
    {
      id: 38,
      name: "75018 - Goutte d'or",
      url: 'https://www.paris.fr/equipements/bibliotheque-goutte-d-or-1744'
    },
    {
      id: 39,
      name: '75018 - Jacqueline de Romilly',
      url: 'https://www.paris.fr/equipements/bibliotheque-jacqueline-de-romilly-1745'
    },
    {
      id: 53,
      name: '75018 - Maurice Genevoix',
      url: 'https://www.paris.fr/equipements/bibliotheque-maurice-genevoix-1743'
    },
    {
      id: 40,
      name: '75018 - Robert Sabatier',
      url: 'https://www.paris.fr/equipements/bibliotheque-robert-sabatier-1742'
    },
    {
      id: 41,
      name: '75018 - Vaclav Havel',
      url: 'https://www.paris.fr/equipements/bibliotheque-vaclav-havel-8693'
    },
    {
      id: 55,
      name: '75019 - Benjamin Rabier',
      url: 'https://www.paris.fr/equipements/bibliotheque-benjamin-rabier-6277'
    },
    {
      id: 42,
      name: '75019 - Claude LÃ©vi-Strauss',
      url: 'https://www.paris.fr/equipements/bibliotheque-claude-levi-strauss-6280'
    },
    {
      id: 43,
      name: '75019 - CrimÃ©e',
      url: 'https://www.paris.fr/equipements/bibliotheque-crimee-1749'
    },
    {
      id: 44,
      name: '75019 - Fessart',
      url: 'https://www.paris.fr/equipements/bibliotheque-fessart-1748'
    },
    {
      id: 45,
      name: '75019 - HergÃ©',
      url: 'https://www.paris.fr/equipements/bibliotheque-discotheque-herge-1747'
    },
    {
      id: 46,
      name: '75019 - Place des fÃªtes',
      url: 'https://www.paris.fr/equipements/bibliotheque-place-des-fetes-1746'
    },
    {
      id: 47,
      name: '75020 - Assia Djebar',
      url: 'https://www.paris.fr/equipements/bibliotheque-assia-djebar-19114'
    },
    {
      id: 48,
      name: '75020 - Couronnes-Naguib Mahfouz',
      url: 'https://www.paris.fr/equipements/bibliotheque-couronnes-naguib-mahfouz-1750'
    },
    {
      id: 52,
      name: '75020 - Louise Michel',
      url: 'https://www.paris.fr/equipements/bibliotheque-louise-michel-6320'
    },
    {
      id: 49,
      name: '75020 - Marguerite Duras',
      url: 'https://www.paris.fr/equipements/mediatheque-marguerite-duras-1752'
    },
    {
      id: 50,
      name: '75020 - Oscar Wilde',
      url: 'https://www.paris.fr/equipements/bibliotheque-oscar-wilde-1754'
    },
    {
      id: 51,
      name: '75020 - Sorbier',
      url: 'https://www.paris.fr/equipements/bibliotheque-sorbier-1753'
    }
  ]
};
