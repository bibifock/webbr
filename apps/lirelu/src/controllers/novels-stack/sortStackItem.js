import get from 'lodash/get';

const getLoan = ({ isSaga, loan, novels }) => {
  if (loan) {
    return loan;
  }

  if (!isSaga) {
    return;
  }

  const loans = novels
    .filter((n) => n.loan)
    .sort((a, b) => sortItWith(a.warn, b.whenBack));

  return get(loans, 0);
};

const sortItWith = (a, b) => (a < b ? -1 : 1);
const sortByTitle = (a, b) =>
  sortItWith(a.title.toLowerCase(), b.title.toLowerCase());

export const sortStackItem = (a, b) => {
  const aLoan = getLoan(a);
  const bLoan = getLoan(b);
  if (!aLoan && !bLoan) {
    return sortByTitle(a, b);
  }

  if (aLoan && bLoan) {
    if (aLoan.whenBack === bLoan.whenBack) {
      return sortByTitle(a, b);
    }

    return sortItWith(aLoan.whenBack, bLoan.whenBack);
  }

  return bLoan ? 1 : -1;
};
