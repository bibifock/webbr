import { novelWithLoanIssue } from './test.utils';
import { makeResponseTree } from './utils';

describe('$controllers/novel-stack/utils', () => {
  describe('makeResponseTree', () => {
    it('should work with that', () => {
      const { result, libraries } = novelWithLoanIssue;
      const items = makeResponseTree(result, libraries);

      expect(items[0].novels[0].loan).toBeDefined();
    });
  });
});
