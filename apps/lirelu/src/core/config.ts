export const APP_SECRET = import.meta.env.VITE_PUBLIC_APP_SECRET;
export const PORT = import.meta.env.VITE_PUBLIC_PORT;
export const DB_SOURCE = import.meta.env.VITE_PUBLIC_DB_SOURCE;
export const HOSTNAME = import.meta.env.VITE_PUBLIC_HOSTNAME;

// uploads
export const UPLOAD_DIR = process.env.UPLOAD_DIR;
export const UPLOAD_BOOKS_PATH = process.env.UPLOAD_BOOKS_PATH;
