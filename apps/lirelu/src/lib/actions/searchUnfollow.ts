import { fail } from '@sveltejs/kit';

import { searchModel } from '@webbr/lirelu-db';

import { getFormDataString } from '$utils/getFormDataString';

import { SearchFollowParamsSchema } from './searchFollow';

import type { Action } from '@sveltejs/kit';

export const searchUnfollow: Action = async ({ request, locals }) => {
  try {
    const data = await request.formData();

    const parsed = SearchFollowParamsSchema.safeParse({
      search: getFormDataString(data, 'search'),
      nbResults: getFormDataString(data, 'nbResults')
    });

    if (!parsed.success) {
      return fail(400, { message: parsed.error, missing: true });
    }
    const search = parsed.data;
    const user = locals.session.data;

    await searchModel.unfollow({
      user,
      search
    });

    return { success: true };
  } catch (e) {
    return fail(500, { message: (e as Error)?.message, incorrect: true });
  }
};
