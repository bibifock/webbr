import { fail } from '@sveltejs/kit';
import { z } from 'zod';

import { parisBibli } from '@webbr/api-bibli';

import { getFormDataString } from '$utils/getFormDataString';

import type { Action } from '@sveltejs/kit';

export const GetBookcasesParamsSchema = z.object({
  details: z.string(),
  id: z.string()
});

export const getBookBookcases: Action = async ({ request }) => {
  try {
    const formDataParams = await request.formData();

    const parsed = GetBookcasesParamsSchema.safeParse({
      details: getFormDataString(formDataParams, 'details'),
      id: getFormDataString(formDataParams, 'id')
    });

    if (!parsed.success) {
      return fail(400, { message: parsed.error, missing: true });
    }
    const { details, id } = parsed.data;

    const result = await parisBibli.getBookcases([{ details, id }]);
    if (!result.length || !result?.[0]) {
      throw new Error('no bookcases found');
    }

    const { bookcases, libraries } = result[0];

    return {
      success: true,
      bookcases: bookcases.map((bookcase) => ({
        ...bookcase,
        library: libraries.find(
          (library) => library.url === bookcase.libraryURL
        )
      }))
    };
  } catch (e) {
    return fail(500, { message: (e as Error)?.message, incorrect: true });
  }
};
