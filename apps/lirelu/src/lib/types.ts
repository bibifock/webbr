import type {
  SvelteComponent,
  ComponentProps,
  Component as ComponentSvelte
} from 'svelte';

export enum DEPENDS_ENUM {
  userFollowedSearch = 'user::followed::search'
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type DefaultComponentSvelte = SvelteComponent | ComponentSvelte<any, any>;

export type ComponentEventArgs<
  Component extends DefaultComponentSvelte,
  EventName extends keyof ComponentProps<Component>
> = Parameters<ComponentProps<Component>[EventName]>[0];
