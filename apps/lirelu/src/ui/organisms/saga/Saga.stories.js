import getStory from 'utils/storybook/getStory';

import Component from './Saga';

import { fullStack } from '$ui/pages/stack/stories.utils.svelte';




const saga = fullStack.find((v) => v.isSaga && v.novels.length > 1);

const story = getStory({
  Component,
  title: 'ui/organisms/Saga',
  argTypes: {
    isSaga: { control: 'boolean' },
    title: { control: 'text' },
    nbNovels: { control: 'number' },
    tome: { control: 'number' },
    onTomeChange: { actions: 'onTomeChange' },
    onStatusChange: { actions: 'onStatusChange' }
  },
  args: saga,
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const base = story.bind();

export const open = story.bindWithArgs({ open: true });
