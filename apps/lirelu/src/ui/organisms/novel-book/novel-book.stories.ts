import { fullStack } from '$ui/pages/stack/stories.utils';

import Component from './novel-book.svelte';

import type { Book, Novel as NovelType } from '$ui/types';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';



const meta: CustomMeta<typeof Component> = {
  title: 'ui/organisms/NovelBook',
  component: Component,
  tags: ['autodocs']
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

type NovelWithBooks = Omit<NovelType, 'books'> & { books: Book[] };

const novel: NovelWithBooks = fullStack.find(
  (v) => !v.isSaga // && v.books && v.books.length > 1 //&& !v.loan
) as unknown as NovelWithBooks;

export const Basic: Story = {
  args: {
    books: novel.books
  }
};

export const WithoutBookcase: Story = {
  args: {
    books: novel.books.map((book) => ({ ...book, bookcases: [] }))
  }
};
