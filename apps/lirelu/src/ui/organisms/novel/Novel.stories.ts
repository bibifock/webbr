import { omit } from 'lodash';

import { args } from '$ui/molecules/novel-header/stories.utils';
import { fullStack } from '$ui/pages/stack/stories.utils';

import Component from './Novel.svelte';

import type { Book, Novel as NovelType, Loan as LoanType } from '$ui/types';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';



const meta: CustomMeta<typeof Component> = {
  title: 'ui/organisms/Novel',
  component: Component,
  tags: ['autodocs'],
  args
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

type NovelWithBooks = Omit<NovelType, 'books'> & { books: Book[] };

const novel: NovelWithBooks = fullStack.find(
  (v) => !v.isSaga // && v.books && v.books.length > 1 //&& !v.loan
) as unknown as NovelWithBooks;

export const Basic: Story = {
  args: {
    ...novel,
    books: novel.books.map((book) => ({ ...book, bookcases: [] }))
  }
};

export const FromStack: Story = {
  args: {
    ...novel,
    open: true
  }
};

export const Loading: Story = { args: { loading: true } };

export const Library: Story = {
  args: {
    ...novel,
    open: true,
    library: novel?.books?.[0]?.bookcases?.[1]?.library?.id,
    books: [{ ...novel?.books?.[0], available: true }],
    book: novel.books?.[0],
    bookcase: novel.books?.[0]?.bookcases?.[1]
  }
};

type NovelWithLoan = Omit<NovelWithBooks, 'loan'> & { loan: LoanType };

const novelLoaned = fullStack.find(
  (v) => !v.isSaga && v.books && v.books.length && v.loan
) as unknown as NovelWithLoan;

export const Loan: Story = {
  args: {
    ...novelLoaned,
    loan: {
      ...omit(novelLoaned.loan, ['cannotRenewReason']),
      whenBack: new Date().toString()
    }
  }
};

export const LoanWithLibrary: Story = {
  args: {
    ...novelLoaned,
    open: true,
    library: novelLoaned.loan.library.id,
    books: [{ ...novelLoaned.books[0], available: true }],
    book: novelLoaned.books[0],
    bookcase: novel.books[0].bookcases[0],
    loan: {
      ...novelLoaned.loan,
      whenBack: new Date().toString()
    }
  }
};
