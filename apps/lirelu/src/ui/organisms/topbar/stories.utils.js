const makeAction = (actions, description, summary = 'no args') => ({
  actions,
  description,
  table: {
    type: {
      summary
    }
  }
});

export const argTypes = {
  open: { control: 'boolean' },
  hideFilter: { control: 'boolean' },
  onSearch: makeAction('onSearch', 'fire on search icon click'),
  onUser: makeAction('onUser', 'fire on user click'),
  onLogIn: makeAction('onLogIn', 'fire on login click'),
  onUserStack: makeAction('onUserStack', 'fire on user stack click')
};
