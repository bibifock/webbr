export const getItems = ({ username }: { username?: string }) => {
  const search = {
    icon: 'search',
    href: '/search'
  };

  if (!username) {
    return [
      search,
      {
        icon: 'power',
        href: '/auth/login',
        cyId: 'navbar-link-user'
      }
    ];
  }

  return [
    search,
    {
      icon: 'basket',
      cyId: 'navbar-link-stack',
      href: '/user/stack'
    },
    {
      icon: 'contact',
      cyId: 'navbar-link-user',
      href: '/user'
    }
  ];
};
