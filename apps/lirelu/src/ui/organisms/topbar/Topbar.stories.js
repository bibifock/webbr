import getStory from 'utils/storybook/getStory';

import { argTypes } from './stories.utils';
import Component from './Topbar';

const story = getStory({
  Component,
  title: 'ui/organisms/Topbar',
  argTypes
});

export default story.default;

export const base = story.bind();

export const logged = story.bindWithArgs({
  username: 'username'
});
