import getStory from 'utils/storybook/getStory';

import Component from './StackFilter';

import { STATUSES } from '$ui/molecules/novel-status-list/utils.svelte';




const story = getStory({
  Component,
  title: 'ui/organisms/Stack Filter',
  argTypes: {
    library: { control: 'text' },
    libraries: { control: 'array' },
    status: { control: 'array' },
    search: { control: 'text' },
    open: { control: 'boolean' },
    onSearch: { actions: 'onSearch' }
  },
  args: {
    libraries: Array.from(Array(6)).map((_, value) => ({
      value,
      label: `value-${value}`
    })),
    open: true
  }
});

export default story.default;

export const base = story.bind();

export const filled = story.bindWithArgs({
  status: [STATUSES[0].value],
  search: 'search value'
});

export const filledWithSeveralStatus = story.bindWithArgs({
  status: STATUSES.map((s) => s.value),
  search: 'search value'
});

export const closed = story.bindWithArgs({
  open: false
});
