import getStory from 'utils/storybook/getStory';
import Container, { propParser } from 'utils/storybook/StoryContainer';

import Component from './Default';

const story = getStory({
  Component: Container,
  title: 'ui/templates/Default',
  parser: {
    props: propParser(Component)
  }
});

export default story.default;

export const base = story.bind({
  parameters: {
    layout: 'padded'
  }
});

export const loading = story.bindWithArgs({
  loading: true
});
