import getStory from 'utils/storybook/getStory';
import Container, { propParser } from 'utils/storybook/StoryContainer';

import Component from './Layout';

import { argTypes } from '$ui/organisms/topbar/stories.utils.svelte';




const story = getStory({
  Component: Container,
  title: 'ui/templates/Layout',
  argTypes: {
    ...argTypes,
    onSearch: {
      actions: 'onSearch',
      description: 'fire when icon search clicked',
      table: {
        type: {
          summary: 'no args'
        }
      }
    }
  },
  parser: {
    props: propParser(Component, { big: true })
  }
});

export default story.default;

export const base = story.bind();

export const logged = story.bindWithArgs({
  user: {
    name: 'paul'
  }
});
