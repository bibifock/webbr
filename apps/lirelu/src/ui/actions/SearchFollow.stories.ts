import Component from './SearchFollow.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Component> = {
  component: Component,
  title: 'ui/actions/SeachFollow'
};

export default meta;

type Story = StoryObj<Component>;

export const Empty: Story = {};
export const Loading: Story = { args: { following: true } };
