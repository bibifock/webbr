import Component from './Bookcase.svelte';

import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';


const meta: CustomMeta<typeof Component> = {
  title: 'ui/molecules/Bookcase',
  component: Component,
  tags: ['autodocs'],
  argTypes: {
    active: { control: 'boolean' },
    whenBack: { control: 'text' },
    image: { control: 'text' },
    bookcases: { control: 'object' }
  },
  args: {
    status: 'En rayon',
    isAvailable: true,
    cote: 'COTE BOOK',
    library: {
      url: 'https://pinute.org',
      name: "Bibliothèque de la goutte d'or"
    }
  },
  parameters: {
    layout: 'padded'
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const available: Story = {};

export const borrow: Story = {
  args: {
    isAvailable: false,
    status: 'Emprunté',
    whenBack: '12/12/2020'
  }
};

export const active: Story = {
  args: {
    active: true,
    isAvailable: false,
    status: 'Emprunté',
    whenBack: '12/12/2020'
  }
};
