export const argTypes = {
  saga: { control: 'object' },
  url: { control: 'text' },
  author: { control: 'text' },
  title: { control: 'text' },
  isbn: { control: 'text' }
};

export const args = {
  url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1160014/le-testament-d-involution',
  author: 'Fabien Cerutti. Auteur',
  title: "Le testament d'involution",
  isbn: '9782354086466'
};
