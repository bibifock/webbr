import getStory from 'utils/storybook/getStory';

import Component from './NovelHeader';
import { argTypes, args } from './stories.utils';

const story = getStory({
  Component,
  title: 'ui/molecules/Novel Header',
  argTypes,
  args
});

export default story.default;

export const base = story.bind();

export const saga = story.bindWithArgs({
  saga: {
    href: '#test',
    title: 'Le bâtard de Kosigan',
    tome: '4'
  }
});
