import { STATUSES } from '$ui/molecules/novel-status-list/utils';

import type { StatusOption } from '$ui/molecules/novel-status-list/utils';
import type { Color } from '$ui/types';

type AddItem = { color: Color; icon: string };
export const addItem: AddItem = {
  icon: 'addCircle',
  color: 'secondary'
};

export const getValueItem = (value?: string): StatusOption | AddItem => {
  if (value === undefined) {
    return addItem;
  }
  const item = STATUSES.find((v) => v.value === value);

  return item || addItem;
};
