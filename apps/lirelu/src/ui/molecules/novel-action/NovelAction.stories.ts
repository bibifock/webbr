import NovelAction from './NovelAction.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<NovelAction> = {
  component: NovelAction,
  args: {
    value: 'STATUS_IS_READING'
  },
  parameters: {
    layout: 'centered'
  }
};

export default meta;

type Story = StoryObj<NovelAction>;

export const Basic: Story = {};
export const Loading: Story = { args: { loading: true } };
export const Open: Story = { args: { open: true } };
export const WithoutAction: Story = { args: { value: '' } };
export const CanRemove: Story = { args: { canRemove: true, open: true } };
