import getStory from 'utils/storybook/getStory';

import Component from './Navbar';

const story = getStory({
  Component,
  title: 'ui/molecules/Navbar',
  argTypes: {
    items: {
      control: 'array',
      description: 'list of #atoms/nav-item',
      table: {
        type: {
          summary: '{ click, icon, cyId }'
        }
      }
    },
    reverse: { control: 'boolean' }
  },
  args: {
    items: ['search', 'basket', 'contact'].map((icon) => ({
      icon,
      click: () => alert('icon clicked'),
      cyId: `cy-id_${icon}`
    }))
  }
});

export default story.default;

export const base = story.bind();

export const reverse = story.bindWithArgs({ reverse: true });
