import getStory from 'utils/storybook/getStory';

import Component from './SearchField';

const story = getStory({
  Component,
  title: 'ui/molecules/SearchField',
  argTypes: {
    value: { control: 'text' },
    placeholder: { control: 'text' },
    onSearch: {
      actions: 'onSearch',
      description: 'fire when a valable search value is typed',
      table: {
        type: {
          summary: '({string} search)'
        }
      }
    }
  }
});

export default story.default;

export const base = story.bind();
