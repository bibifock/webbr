import getStory from 'utils/storybook/getStory';

import Component from './Book';

const story = getStory({
  Component,
  title: 'ui/molecules/Book',
  argTypes: {
    edition: { control: 'text' },
    url: { control: 'text' },
    active: { control: 'boolean' },
    available: { control: 'boolean' },
    onClick: { actions: 'onClick' }
  },
  args: {
    edition: 'Lettres anglo-américaines',
    url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1142416/4-3-2-1-roman'
  }
});

export default story.default;

export const base = story.bind();

export const active = story.bindWithArgs({ active: true });

export const available = story.bindWithArgs({ available: true });
