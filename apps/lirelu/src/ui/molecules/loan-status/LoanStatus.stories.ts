import LoanStatus from './LoanStatus.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<LoanStatus> = {
  component: LoanStatus,
  args: {
    whenBack: new Date().toString(),
    library: {
      id: 4,
      name: '75003 - Marguerite Audoux',
      url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
    }
  }
};

export default meta;
type Story = StoryObj<LoanStatus>;

export const Basic: Story = {};

export const IsSoonLate: Story = { args: { isSoonLate: true } };

export const IsLate: Story = {
  args: { isLate: true, cannotRenewReason: 'Réservé pour un autre usager' }
};
