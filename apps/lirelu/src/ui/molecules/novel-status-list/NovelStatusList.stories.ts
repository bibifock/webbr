import { STATUSES } from './utils';
import NovelStatusList from './NovelStatusList.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<NovelStatusList> = {
  component: NovelStatusList
};

export default meta;

type Story = StoryObj<NovelStatusList>;

export const Basic: Story = {};

export const Selected: Story = {
  args: {
    values: STATUSES.slice(0, -1).map((s) => s.value)
  }
};
