import { type NovelStatus, type Color, NovelStatusEnum } from '$ui/types';

export type StatusOption = {
  label: string;
  value: NovelStatus;
  color: Color;
  icon: string;
};

export const STATUSES: StatusOption[] = [
  {
    label: 'to read',
    value: NovelStatusEnum.statusToRead,
    color: 'info',
    icon: NovelStatusEnum.statusToRead
  },
  {
    label: 'reading',
    color: 'primary',
    value: NovelStatusEnum.statusIsReading,
    icon: NovelStatusEnum.statusIsReading
  },
  {
    label: 'finished',
    color: 'success',
    value: NovelStatusEnum.statusFinished,
    icon: NovelStatusEnum.statusFinished
  }
];
