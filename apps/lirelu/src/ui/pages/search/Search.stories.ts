import { args } from '$ui/molecules/novel-header/stories.utils';

import Search from './Search.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Search> = {
  component: Search,
  title: 'ui/pages/Search'
};

export default meta;

type Story = StoryObj<Search>;

export const Empty: Story = {};

export const Loading: Story = { args: { loading: true } };

export const NoResult: Story = { args: { search: 'Kosigan' } };

const filledArgs = {
  search: 'kosigan ombre',
  items: Array(6).fill({
    ...args,
    description:
      'Il habite au nord… au nord… bien plus au nord… allez hue hue galope… galope… get on up… get on up…'
  })
};
export const Filled: Story = {
  args: filledArgs
};

export const Followed: Story = {
  args: { ...filledArgs, followed: true }
};
