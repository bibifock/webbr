import getStory from 'utils/storybook/getStory';

import Component from './Me';

const story = getStory({
  Component,
  title: 'ui/pages/Me',
  argTypes: {
    email: { control: 'text' },
    name: { control: 'text' },
    dateOfBirth: { control: 'text' },
    loading: { control: 'boolean' },
    saving: { control: 'boolean' },
    importing: { control: 'boolean' },
    syncLibAccount: { control: 'boolean' },
    onLogout: { action: 'onLogout' },
    onImport: { action: 'onImport' },
    onSave: { action: 'onSave' }
  }
});

export default story.default;

const title = 'vos paramètres';

export const base = story.bindWithArgs({
  title
});

export const filled = story.bindWithArgs({
  title,
  email: 'name@mail.ru',
  name: 'my-username',
  dateOfBirth: '13122012',
  syncLibAccount: true
});
