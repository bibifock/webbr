import { fullStack, libraries } from './stories.utils';
import Stack from './Stack.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Stack> = {
  component: Stack,
  title: 'ui/pages/Stack'
};

export default meta;

type Story = StoryObj<Stack>;

export const Empty: Story = {};

export const Full = {
  args: {
    items: fullStack,
    libraries
  }
};

export const Open = {
  args: {
    items: fullStack,
    libraries,
    open: true
  }
};
