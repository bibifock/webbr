import get from 'lodash/get';

import { isSaga, NovelStatusEnum } from '$ui/types';

import type { Novel, NovelStatus, Saga, Book } from '$ui/types';

export type Item = Novel | Saga;

/**
 * @param {array} _.items
 * @param {string} _.search
 * @param {array} _.status
 *
 * @return {array}
 */
export const filterNovels = ({
  items,
  search,
  status: statusList = []
}: {
  items: Item[];
  search?: string;
  status: NovelStatus[];
}) => {
  if (!search && !get(statusList, 'length')) {
    return items;
  }

  const checkStatus = (v: Novel) => {
    return (
      (v.status && statusList.includes(v.status)) ||
      (statusList.includes(NovelStatusEnum.statusIsReading) && v.loan)
    );
  };

  const isValid = (v: string) =>
    search && v.toLowerCase().indexOf(search.toLowerCase()) > -1;
  return items.filter((item: Item) => {
    if (isValid(item.title)) {
      return true;
    }

    if (!isSaga(item)) {
      return isValid(item.author) || checkStatus(item);
      // ///////////////////
      // TODO do this really useless?
      // } else {
      // if (checkStatus(rest)) {
      // // if this is a loan and reading status asked
      // return true;
      // }
    }

    const { novels } = item;
    const result = novels.findIndex(
      (novel) =>
        isValid(novel.title) || isValid(novel.author) || checkStatus(novel)
    );

    return result > -1;
  });
};

/**
 * @param {number} library
 *
 * @return {func} (novel) => {array}{ book, bookcase }
 */
const findBookcase =
  (idLibrary: number) =>
  ({ books }: { books: Book[] }) => {
    let bookcase;
    const book = books.find(({ bookcases = [] }) => {
      const index = bookcases.findIndex((b) => b.library.id === idLibrary);

      if (index < 0) {
        return false;
      }

      bookcase = bookcases[index];

      return bookcase;
    });

    return { book, bookcase };
  };

/**
 * @param {array} _.items
 * @param {string} _.search
 * @param {number} _.library
 * @param {array} _.status
 *
 * @return {array}
 */
export const getItems = ({
  items: itemsRaw,
  search,
  library,
  status
}: {
  items: Item[];
  search?: string;
  library?: number;
  status: NovelStatus[];
}): Item[] => {
  const items = filterNovels({ items: itemsRaw, search, status });

  if (!library) {
    return items;
  }

  const findBookAvailable = findBookcase(library);

  return items
    .map((item) => {
      if (!isSaga(item)) {
        return {
          ...item,
          ...findBookAvailable(item)
        };
      }

      let available;
      const novels = item.novels.map((n) => {
        const result = findBookAvailable(n);
        available = !!result.book;

        return {
          ...n,
          ...result
        };
      });

      return {
        ...item,
        novels,
        available
      };
    })
    .filter((item) => (isSaga(item) ? item.available : !!item.book));
};
