import flattenDeep from 'lodash/flattenDeep';
import uniqBy from 'lodash/uniqBy';

export const fullStack = [
  {
    id: 1,
    title: '4 3 2 1 : roman',
    description:
      "Isaac Reznikoff, devenu Ferguson en posant le pied à Ellis Island, incarne toutes les figures du destin proposées à l'individu par le monde et l'Amérique des années 1950, de l'enfance à l'âge adulte. ©Electre 2018<br/><br/>",
    idAuthor: 1,
    author: 'Paul Auster ',
    thumb: '/img/book/1.jpg',
    status: 'STATUS_TO_READ',
    loan: {
      idUser: 1,
      idBook: 1,
      idLibrary: 4,
      whenBack: 1612998000000,
      renewData: {
        HoldingId: '32272134880860',
        RecordId: '1142416',
        Id: '32272134880860'
      },
      canRenew: false,
      cannotRenewReason: 'Réservé pour un autre usager',
      isLate: false,
      isSoonLate: false,
      status: 'En cours',
      library: {
        id: 4,
        name: '75003 - Marguerite Audoux',
        url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
      }
    },
    books: [
      {
        id: 1,
        idNovel: 1,
        isbn: '9782330090517',
        edition: 'Lettres anglo-américaines',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1142416/4-3-2-1-roman',
        image: '/img/book/1.jpg',
        details: '{"holding":{"id":"1142416","base":"SYRACUSE"}}',
        bookcases: [
          {
            id: 1,
            idLibrary: 1,
            idBook: 1,
            cote: 'AUS',
            status: 'Indisponible',
            library: {
              id: 1,
              name: '75000 - Réserve centrale',
              url: 'https://bibliotheques.paris.fr/la-reserve-centrale.aspx'
            },
            isAvailable: false
          },
          {
            id: 2,
            idLibrary: 2,
            idBook: 1,
            isAvailable: true,
            cote: 'ROMAN AUS',
            status: 'En rayon',
            library: {
              id: 2,
              name: '75001 - Canopée la fontaine',
              url: 'https://www.paris.fr/equipements/mediatheque-de-la-canopee-la-fontaine-16634'
            }
          },
          {
            id: 3,
            idLibrary: 3,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 3,
              name: '75002 - Charlotte Delbo',
              url: 'https://www.paris.fr/equipements/bibliotheque-charlotte-delbo-1664'
            }
          },
          {
            id: 4,
            idLibrary: 4,
            idBook: 1,
            whenBack: '28/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 4,
              name: '75003 - Marguerite Audoux',
              url: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665'
            },
            isAvailable: false
          },
          {
            id: 5,
            idLibrary: 5,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 5,
              name: '75004 - Arthur Rimbaud',
              url: 'https://www.paris.fr/equipements/bibliotheque-arthur-rimbaud-3'
            }
          },
          {
            id: 6,
            idLibrary: 6,
            idBook: 1,
            cote: 'ROMAN AUS',
            status: 'Aucun exemplaire disponible',
            library: {
              id: 6,
              name: '75005 - Buffon',
              url: 'https://www.paris.fr/equipements/bibliotheque-buffon-1682'
            },
            isAvailable: false
          },
          {
            id: 7,
            idLibrary: 7,
            idBook: 1,
            whenBack: '21/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 7,
              name: '75005 - Mohammed Arkoun',
              url: 'https://www.paris.fr/equipements/bibliotheque-mohammed-arkoun-1668'
            },
            isAvailable: false
          },
          {
            id: 8,
            idLibrary: 8,
            idBook: 1,
            whenBack: '14/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 8,
              name: '75005 - Rainer Maria Rilke',
              url: 'https://www.paris.fr/equipements/bibliotheque-rainer-maria-rilke-1672'
            },
            isAvailable: false
          },
          {
            id: 9,
            idLibrary: 9,
            idBook: 1,
            whenBack: '29/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 9,
              name: '75006 - André Malraux',
              url: 'https://www.paris.fr/equipements/bibliotheque-andre-malraux-1691'
            },
            isAvailable: false
          },
          {
            id: 10,
            idLibrary: 10,
            idBook: 1,
            whenBack: '26/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 10,
              name: '75007 - Amélie',
              url: 'https://www.paris.fr/equipements/bibliotheque-amelie-1692'
            },
            isAvailable: false
          },
          {
            id: 11,
            idLibrary: 11,
            idBook: 1,
            whenBack: '13/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 11,
              name: '75007 - Saint-Simon',
              url: 'https://www.paris.fr/equipements/bibliotheque-saint-simon-1693'
            },
            isAvailable: false
          },
          {
            id: 12,
            idLibrary: 12,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 12,
              name: '75008 - Europe',
              url: 'https://www.paris.fr/equipements/bibliotheque-europe-1704'
            }
          },
          {
            id: 13,
            idLibrary: 13,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 13,
              name: '75009 - Drouot',
              url: 'https://www.paris.fr/equipements/bibliotheque-drouot-1719'
            }
          },
          {
            id: 14,
            idLibrary: 14,
            idBook: 1,
            whenBack: '08/01/2021',
            cote: 'AUS (RDC)',
            status: 'Emprunté',
            library: {
              id: 14,
              name: '75009 - Louise Walser-Gaillard (ex Chaptal)',
              url: 'https://www.paris.fr/equipements/bibliotheque-louise-walser-gaillard-ex-chaptal-6094'
            },
            isAvailable: false
          },
          {
            id: 15,
            idLibrary: 15,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 15,
              name: '75009 - Valeyre',
              url: 'https://www.paris.fr/equipements/bibliotheque-valeyre-1711'
            }
          },
          {
            id: 16,
            idLibrary: 16,
            idBook: 1,
            cote: 'AUS',
            status: 'Indisponible',
            library: {
              id: 16,
              name: '75010 - François Villon',
              url: 'https://www.paris.fr/equipements/bibliotheque-francois-villon-1722'
            },
            isAvailable: false
          },
          {
            id: 17,
            idLibrary: 17,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 17,
              name: '75010 - Françoise Sagan',
              url: 'https://www.paris.fr/equipements/mediatheque-francoise-sagan-8695'
            }
          },
          {
            id: 18,
            idLibrary: 18,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 18,
              name: '75010 - Lancry',
              url: 'https://www.paris.fr/equipements/bibliotheque-lancry-1721'
            }
          },
          {
            id: 19,
            idLibrary: 19,
            idBook: 1,
            whenBack: '13/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 19,
              name: '75011 - Parmentier',
              url: 'https://www.paris.fr/equipements/bibliotheque-parmentier-1723'
            },
            isAvailable: false
          },
          {
            id: 20,
            idLibrary: 20,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 20,
              name: '75011 - Violette Leduc ex Faidherbe',
              url: 'https://www.paris.fr/equipements/mediatheque-violette-leduc-ex-bibliotheque-faidherbe-1724'
            }
          },
          {
            id: 21,
            idLibrary: 21,
            idBook: 1,
            whenBack: '19/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 21,
              name: '75012 - Hélène Berr',
              url: 'https://www.paris.fr/equipements/mediatheque-helene-berr-1726'
            },
            isAvailable: false
          },
          {
            id: 22,
            idLibrary: 22,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 22,
              name: '75012 - Saint-Eloi',
              url: 'https://www.paris.fr/equipements/bibliotheque-saint-eloi-1725'
            }
          },
          {
            id: 23,
            idLibrary: 23,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 23,
              name: '75013 - Glacière Marina Tsvetaïeva',
              url: 'https://www.paris.fr/equipements/bibliotheque-glaciere-marina-tsvetaieva-1730'
            }
          },
          {
            id: 24,
            idLibrary: 24,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 24,
              name: '75013 - Italie',
              url: 'https://www.paris.fr/equipements/bibliotheque-italie-1729'
            }
          },
          {
            id: 25,
            idLibrary: 25,
            idBook: 1,
            isAvailable: true,
            cote: 'AUST',
            status: 'En rayon',
            library: {
              id: 25,
              name: '75013 - Jean-Pierre Melville',
              url: 'https://www.paris.fr/equipements/mediatheque-jean-pierre-melville-1728'
            }
          },
          {
            id: 26,
            idLibrary: 26,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 26,
              name: '75014 - Aimé Césaire',
              url: 'https://www.paris.fr/equipements/bibliotheque-aime-cesaire-1732'
            }
          },
          {
            id: 27,
            idLibrary: 27,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 27,
              name: '75014 - Benoîte Groult',
              url: 'https://www.paris.fr/equipements/bibliotheque-benoite-groult-19532'
            }
          },
          {
            id: 28,
            idLibrary: 28,
            idBook: 1,
            whenBack: '26/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 28,
              name: '75014 - Georges Brassens',
              url: 'https://www.paris.fr/equipements/bibliotheque-georges-brassens-1733'
            },
            isAvailable: false
          },
          {
            id: 29,
            idLibrary: 29,
            idBook: 1,
            whenBack: '09/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 29,
              name: '75015 - Andrée Chedid',
              url: 'https://www.paris.fr/equipements/bibliotheque-andree-chedid-1734'
            },
            isAvailable: false
          },
          {
            id: 30,
            idLibrary: 30,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 30,
              name: '75015 - Gutenberg',
              url: 'https://www.paris.fr/equipements/bibliotheque-gutenberg-1736'
            }
          },
          {
            id: 31,
            idLibrary: 31,
            idBook: 1,
            isAvailable: true,
            cote: 'AUST',
            status: 'En rayon',
            library: {
              id: 31,
              name: '75015 - Marguerite Yourcenar',
              url: 'https://www.paris.fr/equipements/mediatheque-marguerite-yourcenar-6218'
            }
          },
          {
            id: 32,
            idLibrary: 32,
            idBook: 1,
            cote: 'AUS',
            status: 'Aucun exemplaire disponible',
            library: {
              id: 32,
              name: '75015 - Vaugirard',
              url: 'https://www.paris.fr/equipements/bibliotheque-vaugirard-1735'
            },
            isAvailable: false
          },
          {
            id: 33,
            idLibrary: 33,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 33,
              name: '75016 - Germaine Tillion',
              url: 'https://www.paris.fr/equipements/bibliotheque-germaine-tillion-1737'
            }
          },
          {
            id: 34,
            idLibrary: 34,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 34,
              name: '75016 - Musset',
              url: 'https://www.paris.fr/equipements/bibliotheque-musset-1738'
            }
          },
          {
            id: 35,
            idLibrary: 35,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 35,
              name: '75017 - Batignolles',
              url: 'https://www.paris.fr/equipements/bibliotheque-batignolles-1739'
            }
          },
          {
            id: 36,
            idLibrary: 36,
            idBook: 1,
            whenBack: '21/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 36,
              name: '75017 - Colette Vivier',
              url: 'https://www.paris.fr/equipements/bibliotheque-colette-vivier-1740'
            },
            isAvailable: false
          },
          {
            id: 37,
            idLibrary: 37,
            idBook: 1,
            whenBack: '29/12/2020',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 37,
              name: '75017 - Edmond Rostand',
              url: 'https://www.paris.fr/equipements/mediatheque-edmond-rostand-1741'
            },
            isAvailable: false
          },
          {
            id: 38,
            idLibrary: 38,
            idBook: 1,
            whenBack: '30/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 38,
              name: "75018 - Goutte d'or",
              url: 'https://www.paris.fr/equipements/bibliotheque-goutte-d-or-1744'
            },
            isAvailable: false
          },
          {
            id: 39,
            idLibrary: 39,
            idBook: 1,
            whenBack: '26/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 39,
              name: '75018 - Jacqueline de Romilly',
              url: 'https://www.paris.fr/equipements/bibliotheque-jacqueline-de-romilly-1745'
            },
            isAvailable: false
          },
          {
            id: 40,
            idLibrary: 40,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 40,
              name: '75018 - Robert Sabatier',
              url: 'https://www.paris.fr/equipements/bibliotheque-robert-sabatier-1742'
            }
          },
          {
            id: 41,
            idLibrary: 41,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 41,
              name: '75018 - Vaclav Havel',
              url: 'https://www.paris.fr/equipements/bibliotheque-vaclav-havel-8693'
            }
          },
          {
            id: 42,
            idLibrary: 42,
            idBook: 1,
            whenBack: '08/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 42,
              name: '75019 - Claude Lévi-Strauss',
              url: 'https://www.paris.fr/equipements/bibliotheque-claude-levi-strauss-6280'
            },
            isAvailable: false
          },
          {
            id: 43,
            idLibrary: 43,
            idBook: 1,
            whenBack: '28/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 43,
              name: '75019 - Crimée',
              url: 'https://www.paris.fr/equipements/bibliotheque-crimee-1749'
            },
            isAvailable: false
          },
          {
            id: 44,
            idLibrary: 44,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 44,
              name: '75019 - Fessart',
              url: 'https://www.paris.fr/equipements/bibliotheque-fessart-1748'
            }
          },
          {
            id: 45,
            idLibrary: 45,
            idBook: 1,
            whenBack: '08/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 45,
              name: '75019 - Hergé',
              url: 'https://www.paris.fr/equipements/bibliotheque-discotheque-herge-1747'
            },
            isAvailable: false
          },
          {
            id: 46,
            idLibrary: 46,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 46,
              name: '75019 - Place des fêtes',
              url: 'https://www.paris.fr/equipements/bibliotheque-place-des-fetes-1746'
            }
          },
          {
            id: 47,
            idLibrary: 47,
            idBook: 1,
            isAvailable: true,
            cote: 'R AUS',
            status: 'En rayon',
            library: {
              id: 47,
              name: '75020 - Assia Djebar',
              url: 'https://www.paris.fr/equipements/bibliotheque-assia-djebar-19114'
            }
          },
          {
            id: 48,
            idLibrary: 48,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 48,
              name: '75020 - Couronnes-Naguib Mahfouz',
              url: 'https://www.paris.fr/equipements/bibliotheque-couronnes-naguib-mahfouz-1750'
            }
          },
          {
            id: 49,
            idLibrary: 49,
            idBook: 1,
            whenBack: '20/01/2021',
            cote: 'AUS',
            status: 'Emprunté',
            library: {
              id: 49,
              name: '75020 - Marguerite Duras',
              url: 'https://www.paris.fr/equipements/mediatheque-marguerite-duras-1752'
            },
            isAvailable: false
          },
          {
            id: 50,
            idLibrary: 50,
            idBook: 1,
            isAvailable: true,
            cote: 'AUS',
            status: 'En rayon',
            library: {
              id: 50,
              name: '75020 - Oscar Wilde',
              url: 'https://www.paris.fr/equipements/bibliotheque-oscar-wilde-1754'
            }
          }
        ]
      }
    ]
  },
  {
    id: 2,
    title: 'Chevauche-Brumes',
    description:
      "Les aventures d'une troupe de mercenaires, les lansquenets, qui au sortir d'une énième bataille se voient missionner par leur suzerain pour enquêter sur un phénomène magique appelé la Brume d'encre. Cet orage qui, telle une montagne, bouche l'horizon à la frontière nordique, a fait naître en effet des créatures monstrueuses menaçant la sécurité de l'Etat. ©Electre 2019<br/><br/>",
    idAuthor: 2,
    author: 'Thibaud Latil-Nicolas',
    status: 'STATUS_IS_READING',
    books: [
      {
        id: 2,
        idNovel: 2,
        isbn: '9782354087104',
        edition: 'Icares',
        url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1201758/chevauche-brumes',
        details: '{"holding":{"id":"1201758","base":"SYRACUSE"}}',
        bookcases: [
          {
            id: 2,
            idLibrary: 2,
            idBook: 2,
            isAvailable: true,
            cote: 'FANTASY LAT',
            status: 'En rayon',
            library: {
              id: 2,
              name: '75001 - Canopée la fontaine',
              url: 'https://www.paris.fr/equipements/mediatheque-de-la-canopee-la-fontaine-16634'
            }
          },
          {
            id: 8,
            idLibrary: 8,
            idBook: 2,
            whenBack: '29/01/2021',
            cote: 'ROMAN FANTASY LAT',
            status: 'Emprunté',
            library: {
              id: 8,
              name: '75005 - Rainer Maria Rilke',
              url: 'https://www.paris.fr/equipements/bibliotheque-rainer-maria-rilke-1672'
            },
            isAvailable: false
          },
          {
            id: 12,
            idLibrary: 12,
            idBook: 2,
            cote: 'FANTASY LAT',
            status: 'Aucun exemplaire disponible',
            library: {
              id: 12,
              name: '75008 - Europe',
              url: 'https://www.paris.fr/equipements/bibliotheque-europe-1704'
            },
            isAvailable: false
          },
          {
            id: 13,
            idLibrary: 13,
            idBook: 2,
            isAvailable: true,
            cote: 'FANTASY LAT',
            status: 'En rayon',
            library: {
              id: 13,
              name: '75009 - Drouot',
              url: 'https://www.paris.fr/equipements/bibliotheque-drouot-1719'
            }
          },
          {
            id: 15,
            idLibrary: 15,
            idBook: 2,
            isAvailable: true,
            cote: 'R SFANTASTIQUE / LAT (ADO)',
            status: 'En rayon',
            library: {
              id: 15,
              name: '75009 - Valeyre',
              url: 'https://www.paris.fr/equipements/bibliotheque-valeyre-1711'
            }
          },
          {
            id: 16,
            idLibrary: 16,
            idBook: 2,
            cote: 'SF LAT',
            status: 'Indisponible',
            library: {
              id: 16,
              name: '75010 - François Villon',
              url: 'https://www.paris.fr/equipements/bibliotheque-francois-villon-1722'
            },
            isAvailable: false
          },
          {
            id: 17,
            idLibrary: 17,
            idBook: 2,
            isAvailable: true,
            cote: 'FANTASY LAT',
            status: 'En rayon',
            library: {
              id: 17,
              name: '75010 - Françoise Sagan',
              url: 'https://www.paris.fr/equipements/mediatheque-francoise-sagan-8695'
            }
          },
          {
            id: 20,
            idLibrary: 20,
            idBook: 2,
            isAvailable: true,
            cote: 'FANTASY LAT',
            status: 'En rayon',
            library: {
              id: 20,
              name: '75011 - Violette Leduc ex Faidherbe',
              url: 'https://www.paris.fr/equipements/mediatheque-violette-leduc-ex-bibliotheque-faidherbe-1724'
            }
          },
          {
            id: 28,
            idLibrary: 28,
            idBook: 2,
            whenBack: '26/01/2021',
            cote: 'LAT',
            status: 'Emprunté',
            library: {
              id: 28,
              name: '75014 - Georges Brassens',
              url: 'https://www.paris.fr/equipements/bibliotheque-georges-brassens-1733'
            },
            isAvailable: false
          },
          {
            id: 31,
            idLibrary: 31,
            idBook: 2,
            cote: 'SF LATI T.1',
            status: 'Indisponible',
            library: {
              id: 31,
              name: '75015 - Marguerite Yourcenar',
              url: 'https://www.paris.fr/equipements/mediatheque-marguerite-yourcenar-6218'
            },
            isAvailable: false
          },
          {
            id: 33,
            idLibrary: 33,
            idBook: 2,
            whenBack: '20/01/2021',
            cote: 'LAT',
            status: 'Emprunté',
            library: {
              id: 33,
              name: '75016 - Germaine Tillion',
              url: 'https://www.paris.fr/equipements/bibliotheque-germaine-tillion-1737'
            },
            isAvailable: false
          },
          {
            id: 35,
            idLibrary: 35,
            idBook: 2,
            isAvailable: true,
            cote: 'FANTASY LAT',
            status: 'En rayon',
            library: {
              id: 35,
              name: '75017 - Batignolles',
              url: 'https://www.paris.fr/equipements/bibliotheque-batignolles-1739'
            }
          },
          {
            id: 39,
            idLibrary: 39,
            idBook: 2,
            cote: 'SF LAT',
            status: 'Indisponible',
            library: {
              id: 39,
              name: '75018 - Jacqueline de Romilly',
              url: 'https://www.paris.fr/equipements/bibliotheque-jacqueline-de-romilly-1745'
            },
            isAvailable: false
          },
          {
            id: 41,
            idLibrary: 41,
            idBook: 2,
            isAvailable: true,
            cote: 'LAT FANTASY',
            status: 'En rayon',
            library: {
              id: 41,
              name: '75018 - Vaclav Havel',
              url: 'https://www.paris.fr/equipements/bibliotheque-vaclav-havel-8693'
            }
          },
          {
            id: 42,
            idLibrary: 42,
            idBook: 2,
            whenBack: '08/01/2021',
            cote: 'S.F LAT',
            status: 'Emprunté',
            library: {
              id: 42,
              name: '75019 - Claude Lévi-Strauss',
              url: 'https://www.paris.fr/equipements/bibliotheque-claude-levi-strauss-6280'
            },
            isAvailable: false
          },
          {
            id: 50,
            idLibrary: 50,
            idBook: 2,
            isAvailable: true,
            cote: 'SF/FAN LAT T.1',
            status: 'En rayon',
            library: {
              id: 50,
              name: '75020 - Oscar Wilde',
              url: 'https://www.paris.fr/equipements/bibliotheque-oscar-wilde-1754'
            }
          },
          {
            id: 51,
            idLibrary: 51,
            idBook: 2,
            whenBack: '20/01/2021',
            cote: 'LAT / FANTASY / T. 1',
            status: 'Emprunté',
            library: {
              id: 51,
              name: '75020 - Sorbier',
              url: 'https://www.paris.fr/equipements/bibliotheque-sorbier-1753'
            },
            isAvailable: false
          }
        ]
      }
    ]
  },
  {
    id: 3,
    title: 'Chevauche-Brumes',
    nbNovels: 1,
    novels: [
      {
        id: 3,
        title: 'Les flots sombres',
        description:
          'Les Chevauche-Brumes ont quitté les légions royales du Bleu-Royaume pour traquer les créatures maléfiques issues du brouillard noir. Ces dernières sont désormais dispersées aux quatre coins du monde où elles attaquent les populations civiles sans défense. ©Electre 2020<br/><br/>',
        idAuthor: 2,
        idSaga: 1,
        sagaVolume: 2,
        author: 'Thibaud Latil-Nicolas',
        thumb: '/img/book/3.jpg',
        status: 'STATUS_IS_READING',
        books: [
          {
            id: 3,
            idNovel: 3,
            isbn: '9782354087708',
            edition: 'Icares',
            url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1240689/les-flots-sombres',
            image: '/img/book/3.jpg',
            details: '{"holding":{"id":"1240689","base":"SYRACUSE"}}',
            bookcases: [
              {
                id: 2,
                idLibrary: 2,
                idBook: 3,
                whenBack: '14/01/2021',
                cote: 'FANTASY LAT T.2',
                status: 'Emprunté',
                library: {
                  id: 2,
                  name: '75001 - Canopée la fontaine',
                  url: 'https://www.paris.fr/equipements/mediatheque-de-la-canopee-la-fontaine-16634'
                },
                isAvailable: false
              },
              {
                id: 8,
                idLibrary: 8,
                idBook: 3,
                whenBack: '20/01/2021',
                cote: 'ROMAN FANTASY LAT 2',
                status: 'Emprunté',
                library: {
                  id: 8,
                  name: '75005 - Rainer Maria Rilke',
                  url: 'https://www.paris.fr/equipements/bibliotheque-rainer-maria-rilke-1672'
                },
                isAvailable: false
              },
              {
                id: 12,
                idLibrary: 12,
                idBook: 3,
                isAvailable: true,
                cote: 'FANTASY LAB',
                status: 'En rayon',
                library: {
                  id: 12,
                  name: '75008 - Europe',
                  url: 'https://www.paris.fr/equipements/bibliotheque-europe-1704'
                }
              },
              {
                id: 13,
                idLibrary: 13,
                idBook: 3,
                cote: 'FANTASY T.2',
                status: 'Indisponible',
                library: {
                  id: 13,
                  name: '75009 - Drouot',
                  url: 'https://www.paris.fr/equipements/bibliotheque-drouot-1719'
                },
                isAvailable: false
              },
              {
                id: 15,
                idLibrary: 15,
                idBook: 3,
                isAvailable: true,
                cote: 'SF / LAT',
                status: 'En rayon',
                library: {
                  id: 15,
                  name: '75009 - Valeyre',
                  url: 'https://www.paris.fr/equipements/bibliotheque-valeyre-1711'
                }
              },
              {
                id: 16,
                idLibrary: 16,
                idBook: 3,
                cote: 'SF LAT',
                status: 'Indisponible',
                library: {
                  id: 16,
                  name: '75010 - François Villon',
                  url: 'https://www.paris.fr/equipements/bibliotheque-francois-villon-1722'
                },
                isAvailable: false
              },
              {
                id: 17,
                idLibrary: 17,
                idBook: 3,
                isAvailable: true,
                cote: 'FANTASY LAT',
                status: 'En rayon',
                library: {
                  id: 17,
                  name: '75010 - Françoise Sagan',
                  url: 'https://www.paris.fr/equipements/mediatheque-francoise-sagan-8695'
                }
              },
              {
                id: 28,
                idLibrary: 28,
                idBook: 3,
                whenBack: '27/01/2021',
                cote: 'LAT',
                status: 'Emprunté',
                library: {
                  id: 28,
                  name: '75014 - Georges Brassens',
                  url: 'https://www.paris.fr/equipements/bibliotheque-georges-brassens-1733'
                },
                isAvailable: false
              },
              {
                id: 31,
                idLibrary: 31,
                idBook: 3,
                cote: 'SF LATI T.2',
                status: 'Indisponible',
                library: {
                  id: 31,
                  name: '75015 - Marguerite Yourcenar',
                  url: 'https://www.paris.fr/equipements/mediatheque-marguerite-yourcenar-6218'
                },
                isAvailable: false
              },
              {
                id: 33,
                idLibrary: 33,
                idBook: 3,
                isAvailable: true,
                cote: 'LAT T.2',
                status: 'En rayon',
                library: {
                  id: 33,
                  name: '75016 - Germaine Tillion',
                  url: 'https://www.paris.fr/equipements/bibliotheque-germaine-tillion-1737'
                }
              },
              {
                id: 35,
                idLibrary: 35,
                idBook: 3,
                isAvailable: true,
                cote: 'FANTASY LAT',
                status: 'En rayon',
                library: {
                  id: 35,
                  name: '75017 - Batignolles',
                  url: 'https://www.paris.fr/equipements/bibliotheque-batignolles-1739'
                }
              },
              {
                id: 39,
                idLibrary: 39,
                idBook: 3,
                isAvailable: true,
                cote: 'SF - LAT T.2',
                status: 'En rayon',
                library: {
                  id: 39,
                  name: '75018 - Jacqueline de Romilly',
                  url: 'https://www.paris.fr/equipements/bibliotheque-jacqueline-de-romilly-1745'
                }
              },
              {
                id: 42,
                idLibrary: 42,
                idBook: 3,
                whenBack: '21/01/2021',
                cote: 'S.F LAT',
                status: 'Emprunté',
                library: {
                  id: 42,
                  name: '75019 - Claude Lévi-Strauss',
                  url: 'https://www.paris.fr/equipements/bibliotheque-claude-levi-strauss-6280'
                },
                isAvailable: false
              },
              {
                id: 49,
                idLibrary: 49,
                idBook: 3,
                isAvailable: true,
                cote: 'FANTASY LAT',
                status: 'En rayon',
                library: {
                  id: 49,
                  name: '75020 - Marguerite Duras',
                  url: 'https://www.paris.fr/equipements/mediatheque-marguerite-duras-1752'
                }
              },
              {
                id: 50,
                idLibrary: 50,
                idBook: 3,
                isAvailable: true,
                cote: 'SF/FAN LAT T.2',
                status: 'En rayon',
                library: {
                  id: 50,
                  name: '75020 - Oscar Wilde',
                  url: 'https://www.paris.fr/equipements/bibliotheque-oscar-wilde-1754'
                }
              }
            ]
          }
        ]
      }
    ],
    isSaga: true,
    volumes: [1, 2]
  }
];

interface Library {
  id: number;
  name: string;
  url: string;
}

export const libraries = uniqBy<Library>(
  fullStack.reduce<Library[]>((o, { isSaga, books, novels }) => {
    if (isSaga) {
      books = flattenDeep(novels.map(({ books }) => books));
    }

    return o.concat(
      flattenDeep<Library>(
        (books ?? []).map(({ bookcases }) =>
          bookcases.map(({ library }) => library)
        )
      )
    );
  }, []),
  'name'
).sort((a, b) => (a.name > b.name ? 1 : -1));
