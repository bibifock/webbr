import getStory from 'utils/storybook/getStory';

import Component from './Login';

const story = getStory({
  Component,
  title: 'ui/pages/Login',
  argTypes: {
    loading: { control: 'boolean' },
    errors: { control: 'object' },
    password: { control: 'text' },
    login: { control: 'text' },
    onSubmit: { actions: 'onSubmit' }
  }
});

export default story.default;

export const base = story.bind();

export const filled = story.bindWithArgs({
  login: 'name@email.ru',
  password: 'may-password'
});

export const loading = story.bindWithArgs({ loading: true });

export const error = story.bindWithArgs({
  errors: { key: 'this is a form error' }
});
