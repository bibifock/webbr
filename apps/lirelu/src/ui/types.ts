export type Dispatcher<
  TEvents extends Record<keyof TEvents, CustomEvent<unknown>>
> = {
  [Property in keyof TEvents]: TEvents[Property]['detail'];
};

export type Color =
  | 'primary'
  | 'danger'
  | 'warning'
  | 'success'
  | 'info'
  | 'secondary';

export type Option<T = string | number> = {
  value: T | undefined;
  label: string;
};

export type User = { id: number; name: string };

export enum NovelStatusEnum {
  statusToRead = 'STATUS_TO_READ',
  statusIsReading = 'STATUS_IS_READING',
  statusFinished = 'STATUS_FINISHED',
  statusUnfinished = 'STATUS_UNFINISHED'
}

export type NovelStatus = `${NovelStatusEnum}`;

export type Library = {
  id: number;
  name: string;
  url: string;
};

export type LoanRenew = {
  HoldingId: string;
  RecordId: string;
  Id: string;
};

export type Loan = {
  idBook: number;
  library: Library;
  whenBack: string;
  renewData: LoanRenew;
  canRenew: boolean;
  cannotRenewReason?: string;
  isLate: boolean;
  isSoonLate: boolean;
  status: string;
  updated?: string;
};

export type Bookcase = {
  id: number;
  isAvailable: boolean;
  isReservable: boolean;
  idLibrary: number;
  whenBack?: string;
  details: string;
  library: Library;
  cote: string;
  status: string;
};

export type Book = {
  id?: number;
  isbn: string;
  edition?: string;
  url: string;
  image?: string;
  details: string;
  available?: boolean;
  bookcases: Bookcase[];
};

export type NovelSaga = {
  href: string;
  title: string;
  tome: number;
};

export type Novel = {
  id?: number;
  author: string;
  isbn: string;
  title: string;
  url: string;
  description: string;
  status?: NovelStatus;
  thumb?: string;
  library?: number;
  book?: Book;
  books: Book[];
  bookcase?: Bookcase;
  loading?: boolean;
  loan?: Loan;
};

export type SagaNovel = Novel & {
  saga: NovelSaga;
  sagaVolume: number;
};

export const isSagaNovel = (val: Novel | SagaNovel): val is SagaNovel =>
  'sagaVolume' in val;

export type Saga = {
  id?: number;
  title: string;
  volumes: number[];
  novels: SagaNovel[];
  tome: number;
  library?: number;
} & ({ id: number } | { query: string });

export function isSaga(item: Saga | Novel): item is Saga {
  return 'volumes' in item && !!item.volumes;
}
