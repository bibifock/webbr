import getStory from 'utils/storybook/getStory';
import { colorType } from 'utils/storybook/argTypes';

import Component from './DropdownItem';

import { argTypes } from '$ui/atoms/icon/stories.utils.svelte';




const story = getStory({
  Component,
  title: 'ui/atoms/Dropdown Item',
  argTypes: {
    color: colorType,
    icon: argTypes.name
  },
  args: {
    label: 'label'
  }
});

export default story.default;

export const base = story.bind();

export const active = story.bindWithArgs({ active: true });
