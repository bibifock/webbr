import getStory from 'utils/storybook/getStory';

import Component from './TextField.svelte';

const story = getStory({
  title: 'ui/atoms/fields/Text',
  Component,
  argTypes: {
    type: {
      defaultValue: 'text',
      control: {
        type: 'select',
        options: ['text', 'email', 'password']
      }
    },
    label: { control: 'text' },
    value: { control: 'text' },
    small: { control: 'boolean' },
    autofocus: { control: 'boolean' },
    error: { control: 'text' }
  }
});

export default story.default;

export const Base = story.bindWithArgs({
  label: 'label',
  value: 'value'
});

export const Small = story.bindWithArgs({
  ...Base.args,
  small: true
});

export const Error = story.bindWithArgs({
  ...Base.args,
  error: 'this is an error'
});
