import getStory from 'utils/storybook/getStory';

import Component from './CheckboxField.svelte';

const story = getStory({
  title: 'ui/atoms/fields/Checkbox',
  Component,
  argTypes: {
    id: { control: 'text' },
    label: { control: 'text' },
    value: { control: 'text' },
    checked: { control: 'boolean' },
    onChange: { actions: 'onChange' }
  },
  args: {
    id: 'id',
    label: 'label',
    value: 'value'
  }
});

export default story.default;

export const Base = story.bind();

export const Checked = story.bindWithArgs({ checked: true });
