import getStory from 'utils/storybook/getStory';

import Component from './BookcaseStatus';

const story = getStory({
  Component,
  title: 'ui/atoms/Bookcase Status',
  argTypes: {
    status: { control: 'text' },
    whenBack: { control: 'text' },
    cote: { control: 'text' },
    isAvailable: { control: 'boolean' }
  },
  args: {
    status: 'En rayon',
    isAvailable: true,
    cote: 'COTE BOOK'
  },
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const available = story.bind();

export const borrow = story.bindWithArgs({
  isAvailable: false,
  status: 'Emprunté',
  whenBack: '12/12/2020'
});
