export type Tome = {
  value: number;
  active?: boolean;
  loaded?: boolean;
};
