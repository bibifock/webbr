import getStory from 'utils/storybook/getStory';

import Component from './Tome';

const story = getStory({
  Component,
  title: 'ui/atoms/Tome',
  argTypes: {
    value: { control: 'text' },
    active: { control: 'boolean' },
    loaded: { control: 'boolean' },
    onClick: { actions: 'onClick' }
  },
  args: {
    value: 12
  }
});

export default story.default;

export const base = story.bind();

export const active = story.bindWithArgs({ active: true });
