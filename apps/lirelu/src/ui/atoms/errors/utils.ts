import isArray from 'lodash/isArray';

/**
 * @param {string|array|object} errors
 *
 * @return {array}
 */
export const parseErrors = (
  errors: string | string[] | Record<string, string>
) => {
  if (isArray(errors)) {
    return errors;
  }

  if (typeof errors === 'string') {
    return errors ? [errors] : [];
  }

  return Object.keys(errors).map(
    (k) => `<strong>${k} : </strong> ${errors[k]}`
  );
};
