import getStory from 'utils/storybook/getStory';
import { colorType } from 'utils/storybook/argTypes';

import Component from './NavItem';

import { argTypes } from '$ui/atoms/icon/stories.utils.svelte';




const story = getStory({
  title: 'ui/atoms/Nav Item',
  Component,
  argTypes: {
    icon: argTypes.name,
    color: colorType,
    onClick: { actions: 'onClick' }
  }
});

export default story.default;

export const base = story.bind();
