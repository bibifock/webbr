import Component, { colors as options } from './ButtonStories.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Component> = {
  component: Component,
  title: 'ui/atoms/button',
  argTypes: {
    color: {
      defaultValue: 'primary',
      control: {
        options,
        type: 'select'
      }
    },
    loading: { control: 'boolean' },
    small: { control: 'boolean' },
    disabled: { control: 'boolean' },
    onClick: { actions: 'onClick' }
  }
};

export default meta;

type Story = StoryObj<Component>;

// extra example
export const List: Story = {};
