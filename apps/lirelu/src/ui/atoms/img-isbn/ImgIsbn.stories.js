import getStory from 'utils/storybook/getStory';

import Component from './ImgIsbn';

const story = getStory({
  Component,
  title: 'ui/atoms/ImgIsbn',
  argTypes: {
    isbn: { control: 'text' },
    thumb: { control: 'text' }
  }
});

export default story.default;

export const base = story.bind();

export const thumb = story.bindWithArgs({
  thumb: 'https://www.fillmurray.com/200/300'
});

export const validIsbn = story.bindWithArgs({ isbn: '9782354086466' });

export const invalidThumbAndIsbn = story.bindWithArgs({
  thumb: 'http://failed-url',
  isbn: '12345'
});
