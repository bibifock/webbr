import Component from './Icon.svelte';
import IconStories from './IconStories.svelte';
import { icons } from './constants';

import type { Meta, StoryObj } from '@storybook/svelte';

const options = Object.keys(icons);

const meta: Meta<Component> = {
  component: Component,
  title: 'ui/atoms/icon',
  argTypes: {
    name: {
      defaultValue: options[0],
      control: 'select',
      options
    },
    size: {
      description: 'props to add to component for change size',
      defaultValue: 'small',
      control: 'inline-radio',
      options: ['small', 'medium', 'large']
    },
    noHover: {
      control: 'boolean',
      description: 'remove hover effect'
    }
  }
};

export default meta;

type Story = StoryObj<Component>;

export const All: Story = {
  render: () => ({
    Component: IconStories,
    props: {
      prop: 'value'
    }
  })
};

// extra example
export const List: Story = {};
