import IoIosAddCircle from 'svelte-icons/io/IoIosAddCircle.svelte';
import IoIosCheckmarkCircleOutline from 'svelte-icons/io/IoIosCheckmarkCircleOutline.svelte';
import IoIosFunnel from 'svelte-icons/io/IoIosFunnel.svelte';
import IoIosHelp from 'svelte-icons/io/IoIosHelp.svelte';
import IoIosHourglass from 'svelte-icons/io/IoIosHourglass.svelte';
import IoIosListBox from 'svelte-icons/io/IoIosListBox.svelte';
import IoIosPin from 'svelte-icons/io/IoIosPin.svelte';
import IoIosRemoveCircle from 'svelte-icons/io/IoIosRemoveCircle.svelte';
import IoIosTime from 'svelte-icons/io/IoIosTime.svelte';
import IoMdAdd from 'svelte-icons/io/IoMdAdd.svelte';
import IoMdClose from 'svelte-icons/io/IoMdClose.svelte';
import IoMdContact from 'svelte-icons/io/IoMdContact.svelte';
import IoMdEye from 'svelte-icons/io/IoMdEye.svelte';
import IoMdEyeOff from 'svelte-icons/io/IoMdEyeOff.svelte';
import IoMdFlag from 'svelte-icons/io/IoMdFlag.svelte';
import IoMdImages from 'svelte-icons/io/IoMdImages.svelte';
import IoMdLink from 'svelte-icons/io/IoMdLink.svelte';
import IoMdMenu from 'svelte-icons/io/IoMdMenu.svelte';
import IoMdPower from 'svelte-icons/io/IoMdPower.svelte';
import IoMdSearch from 'svelte-icons/io/IoMdSearch.svelte';
import IoMdStar from 'svelte-icons/io/IoMdStar.svelte';
import IoIosWarning from 'svelte-icons/io/IoIosWarning.svelte';

export const icons = {
  STATUS_TO_READ: IoMdEye,
  STATUS_IS_READING: IoIosHourglass,
  STATUS_FINISHED: IoIosCheckmarkCircleOutline,
  STATUS_UNFINISHED: IoMdEyeOff,
  eyeOff: IoMdEyeOff,
  eye: IoMdEye,
  addCircle: IoIosAddCircle,
  delCircle: IoIosRemoveCircle,
  add: IoMdAdd,
  time: IoIosTime,
  contact: IoMdContact,
  images: IoMdImages,
  link: IoMdLink,
  power: IoMdPower,
  search: IoMdSearch,
  menu: IoMdMenu,
  close: IoMdClose,
  star: IoMdStar,
  basket: IoIosListBox,
  flag: IoMdFlag,
  funnel: IoIosFunnel,
  pin: IoIosPin,
  warning: IoIosWarning,
  help: IoIosHelp
};

export const isIconName = (icon: string): icon is keyof typeof icons =>
  icon in icons;
