import getStory from 'utils/storybook/getStory';
import { colorType } from 'utils/storybook/argTypes';

import Component from './Loading.svelte';

const story = getStory({
  title: 'ui/atoms/loading',
  Component,
  argTypes: {
    color: colorType,
    border: { control: 'boolean' },
    size: {
      description: 'props to add to component for change size',
      defaultValue: 'small',
      control: {
        type: 'inline-radio',
        options: ['small', 'medium', 'large']
      }
    }
  },
  parser: {
    props: ({ size, ...props }) => {
      if (size !== 'small') {
        props[size] = true;
      }
      return props;
    }
  }
});

export default story.default;

export const base = story.bind();

export const border = story.bindWithArgs({ border: true });
