import IconButton from './IconButton.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<IconButton> = {
  component: IconButton,
  title: 'ui/atoms/IconButton',
  args: {
    name: 'time'
  }
};

export default meta;

type Story = StoryObj<IconButton>;

export const Basic: Story = {};

export const Loading: Story = { args: { loading: true } };
