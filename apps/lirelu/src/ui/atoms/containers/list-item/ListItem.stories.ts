import Component from './ListItem.stories.svelte';

import type { Meta, StoryObj } from '@storybook/svelte';

const meta: Meta<Component> = {
  component: Component,
  title: 'ui/atoms/containers/list-items'
};

export default meta;

type Story = StoryObj<Component>;

// extra example
export const Default: Story = {};
