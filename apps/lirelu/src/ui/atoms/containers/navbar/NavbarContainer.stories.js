import getStory from 'utils/storybook/getStory';
import Container, { propParser } from 'utils/storybook/StoryContainer';

import Component from './NavbarContainer';

const story = getStory({
  Component: Container,
  title: 'ui/atoms/containers/Navbar',
  argTypes: {
    reverse: { control: 'boolean' }
  },
  parser: {
    props: propParser(Component)
  }
});

export default story.default;

export const base = story.bind();

export const reverse = story.bindWithArgs({ reverse: true });
