import type { Color } from '$ui/types';

const classes: Record<Color, `text-${Color}`> = {
  danger: 'text-danger',
  info: 'text-info',
  primary: 'text-primary',
  secondary: 'text-secondary',
  success: 'text-success',
  warning: 'text-warning'
};

export const getClassTextColor = (
  color: Color | undefined
): `text-${Color}` | '' => (color ? classes[color] : '');
