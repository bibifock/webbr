import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

const all = async () => {
  const db = await getDb();

  const categories = await db.all(SQL`SELECT rowid AS id, name FROM categories`);

  await db.close();

  return categories;
};

export default { all };
