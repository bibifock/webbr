import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';
import Files from '$lib/models/file/File';

export const selectFields = [
  ...['rowid id', 'title', 'artist', 'author', 'link', 'level', 'created', 'updated'].map(
    (v) => `tabs.${v}`
  ),
  'categories.name category',
  'categories.rowid categoryId'
];

export const innerJoins = `
  INNER JOIN categories ON tabs.categoryId = categories.rowid
`;

const sqlSelect = () => {
  const query = SQL``;
  query.append(`
    SELECT ${selectFields.join(', ')}
    FROM tabs
      ${innerJoins}
  `);

  return query;
};

const SQL_ORDER_BY = ' ORDER BY title ASC, artist ASC';

const get = async (conditions = '') => {
  const db = await getDb();

  const query = sqlSelect();
  if (conditions) {
    query.append(conditions);
  }

  const tab = await db.get(query);
  if (!tab) {
    return false;
  }

  const files = await Files.all({ ids: [tab.id] });

  return { ...tab, files };
};

export const getById = (id, withTabs) => get(SQL`WHERE tabs.rowid=${id}`, { withTabs });
export const alreadyExists = ({ title, artist }) =>
  get(SQL`
  WHERE tabs.title=${title} AND tabs.artist=${artist}
`);

export const all = async ({ search, orderBy, limit }) => {
  const db = await getDb();

  const query = sqlSelect();
  query.append('WHERE 1=1');
  if (search) {
    search
      .split(' ')
      .filter((v) => /^[A-Z0-9]+$/i.test(v))
      .map((v) => `%${v}%`)
      .forEach((v) => {
        query.append(SQL`
          AND title || '%' || artist LIKE ${v}
        `);
      });
  }

  if (orderBy) {
    query.append(` ORDER BY ${orderBy}`);
  } else {
    query.append(SQL_ORDER_BY);
  }

  if (limit) {
    query.append(` LIMIT ${limit}`);
  }

  const tabs = await db.all(query);

  if (!tabs.length) {
    return tabs;
  }

  const idsToIndex = tabs.reduce((obj, { id }, index) => ({ ...obj, [id]: parseInt(index) }), {});

  const ids = Object.keys(idsToIndex);

  const files = await Files.all({ ids, db });

  files.forEach(({ tabId, ...file }) => {
    const index = idsToIndex[tabId];
    if (!tabs[index].files) {
      tabs[index].files = [];
    }
    tabs[index].files.push(file);
  });

  return tabs;
};

export const save = async (tab) => {
  const { id, title, artist, author, link, level, categoryId } = tab;

  const db = await getDb();

  const query = id
    ? SQL`
      UPDATE tabs SET
        title=${title},
        artist=${artist},
        author=${author},
        categoryId=${categoryId},
        link=${link},
        level=${level},
        updated=CURRENT_TIMESTAMP
      WHERE rowid=${id}
    `
    : SQL`
      INSERT INTO tabs (title, artist, author, link, level, categoryId)
      VALUES (${title}, ${artist}, ${author}, ${link}, ${level}, ${categoryId})
    `;
  const { lastID } = await db.run(query);

  return {
    ...tab,
    id: id || lastID
  };
};

export default {
  all,
  getById,
  alreadyExists,
  save
};
