import makeCheckRequiredFields from '$lib/utils/makeCheckRequiredFields';

const checkRequiredFields = makeCheckRequiredFields(['title', 'artist', 'categoryId']);

export const isInvalidTab = (
  tab: { link?: string },
  success: boolean | Record<string, unknown> = false
) => {
  const errors = checkRequiredFields(tab);

  if (tab.link && !/https?:\/\//.test(tab.link)) {
    errors.link = 'should http link';
  }

  if (Object.keys(errors).length) {
    return errors;
  }

  return success;
};
