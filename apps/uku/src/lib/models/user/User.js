import SQL from 'sql-template-strings';
import bcrypt from 'bcryptjs';

import getDb from '$lib/core/db';

const saltRound = 10;

export const canUpdate = async ({ email }) => {
  const db = await getDb();

  const result = await db.get(SQL`
    SELECT rowid id
    FROM users
    WHERE
      email=${email} AND password IS NULL
  `);

  return result;
};

export const update = async ({ id, email, name, password }) => {
  const hash = await bcrypt.hash(password, saltRound);

  const db = await getDb();
  await db.get(SQL`
    UPDATE users
    SET
      email=${email},
      password=${hash},
      name=${name}
    WHERE rowid=${id}
  `);
};

export const login = async ({ login, password }) => {
  const db = await getDb();

  const result = await db.get(SQL`
    SELECT rowid id, password, name, email
    FROM users
    WHERE
      email=${login} OR name=${login}
      AND password IS NOT NULL
  `);

  if (!result) {
    throw Error('user not found');
  }

  const { password: hash, ...user } = result;

  const isLogged = await bcrypt.compare(password, hash);

  if (!isLogged) {
    throw Error('sorry, but this will not be possible');
  }

  return user;
};

export default {
  canUpdate,
  update,
  login
};
