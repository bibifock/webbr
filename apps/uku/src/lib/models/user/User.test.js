import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

import { canUpdate, update, login } from './User';

const email = 'this-is-a-test-email@formytest.only';

const sqlInsert = SQL`INSERT INTO users(email) VALUES(${email});`;
const sqlDelete = SQL`DELETE FROM users WHERE UPPER(email)=UPPER(${email});`;

describe('app/models/User', () => {
  let db;
  beforeAll(async () => {
    db = await getDb();
    await db.run(sqlDelete);
  });

  afterAll(async () => {
    await db.run(sqlDelete);
    await db.close();
  });

  describe('canUpdate', () => {
    describe('should be case insensitive', () => {
      let id;
      beforeAll(async () => {
        const { lastID } = await db.run(sqlInsert);
        id = lastID;
      });

      it.each([
        'this-is-a-test-email@formytest.only',
        'THIS-IS-A-TEST-EMAIL@FORMYTEST.ONLY',
        'This-Is-A-Test-Email@ForMyTest.OnLy'
      ])('%p', async (email) => {
        expect(await canUpdate({ email })).toEqual({ id });
      });
    });

    it('should return undefined when email not present in database', async () => {
      expect(await canUpdate({ email: 'blablablajldejlejdle' })).toBeFalsy();
    });
  });

  describe('login', () => {
    const password = '01234567890';
    const name = 'test';
    let id;
    beforeAll(async () => {
      await db.run(sqlDelete);
      const { lastID } = await db.run(sqlInsert);
      id = lastID;
      await update({ id, email, name, password });
    });

    afterAll(async () => {
      await db.run(sqlDelete);
      await db.close();
    });

    describe('should return false on error', () => {
      it.each([
        ['wrong email', email + '___', password],
        ['wrong password', email, password + '____']
      ])('%s', async (msg, email, password) => {
        expect(await login(email, password)).toBeFalsy();
      });
    });
    it('should return user id when password is good', async () => {
      expect(await login(email, password)).toEqual({ id, name, email });
    });
  });
});
