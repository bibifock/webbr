import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

const sqlSelect = () => SQL`
  SELECT
    distinct artist
  FROM tabs
`;
export const all = async ({ search, limit }) => {
  const db = await getDb();

  const query = sqlSelect();

  if (search) {
    search = `%${search}%`;
    query.append(SQL`
      WHERE artist LIKE ${search}
    `);
  }

  query.append(' ORDER BY artist');

  if (limit) {
    query.append(` LIMIT ${limit}`);
  }

  const artists = await db.all(query);

  return artists;
};

export default {
  all
};
