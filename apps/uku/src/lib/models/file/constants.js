export const UPLOAD_DIR = '/static/';
export const UPLOAD_PATH = '/uploads/';
export const UPLOAD_PATH_TABS = `${UPLOAD_PATH}tabs/`;
export const UPLOAD_PATH_LIST = `${UPLOAD_PATH}playlists`;
