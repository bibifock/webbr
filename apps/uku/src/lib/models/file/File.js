import fs from 'fs';

import SQL from 'sql-template-strings';

import getDb from '$lib/core/db';

import { UPLOAD_PATH_TABS, UPLOAD_DIR } from './constants';

const sqlSelect = () => SQL`
    SELECT
      rowid id, name, type, fullpath, tabId, updated
    FROM files
`;

const SQL_ORDER_BY = SQL`
  ORDER BY tabId, updated DESC
`;

const get = async (conditions) => {
  const db = await getDb();

  const query = sqlSelect();

  if (conditions) {
    query.append(conditions);
  }

  const file = await db.get(query);

  return file;
};

const getById = (id) => get(SQL` WHERE id=${id}`);

const all = async ({ ids }) => {
  const db = await getDb();

  const query = sqlSelect();
  if (ids && ids.length) {
    const listIds = ids.map((id) => parseInt(id)).join(',');
    query.append(`WHERE tabId IN (${listIds})`);
  }

  query.append(SQL_ORDER_BY);

  const files = await db.all(query);

  return files || [];
};

const alreadyExists = async ({ tabId, fullpaths }) => {
  const db = await getDb();

  const results = await db.all(SQL`
    SELECT rowid, fullpath
    FROM files
    WHERE fullpath IN (${fullpaths.join(', ')}) AND tabId=${tabId}
  `);

  return results.length ? results : false;
};

export const storeFile = ({ fullpath, src, insert }) => {
  if (insert) {
    fullpath = UPLOAD_PATH_TABS + fullpath;
  }
  fs.copyFileSync(src, process.env.PWD + UPLOAD_DIR + fullpath);

  return fullpath;
};

const inserFiles = async (files) => {
  if (!files.length) {
    return true;
  }
  const db = await getDb();

  const query = SQL`
    INSERT INTO files (name, type, fullpath, tabId) VALUES
  `;

  files.forEach((file, index) => {
    const fullpath = storeFile({ ...file, insert: true });

    const { name, type, tabId } = file;

    if (index > 0) {
      query.append(', ');
    }
    query.append(SQL`(${name}, ${type}, ${fullpath}, ${tabId})`);
  });

  await db.get(query);
};

const updateFiles = async (files) => {
  if (!files.length) {
    return true;
  }

  const db = await getDb();

  const ids = files.map((file) => {
    const { id } = file;
    storeFile(file);

    return parseInt(id);
  });

  const query = SQL`
    UPDATE files
      SET updated=CURRENT_TIMESTAMP
    WHERE rowid IN (
  `;

  ids.forEach((id, index) => {
    if (index > 0) {
      query.append(SQL`, `);
    }
    query.append(SQL`${id}`);
  });
  query.append(SQL`)`);

  await db.get(query);
};

const saveAll = async (files) => {
  if (!files || !files.length) {
    return true;
  }

  const filesToAdd = files.filter(({ id }) => !id);
  const filesToUpdate = files.filter(({ id }) => id);

  await inserFiles(filesToAdd);
  await updateFiles(filesToUpdate);

  return true;
};

export default {
  all,
  getById,
  saveAll,
  alreadyExists
};
