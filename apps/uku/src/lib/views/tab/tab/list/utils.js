import post from '$lib/utils/post';
import { parseDate } from '$lib/utils/date';

import { API_URL_INDEX, URL_EDIT } from '../constants';

const prepareFiles = (files, fileId) => {
  if (!files || !files.length) {
    return [];
  }

  if (!fileId) {
    return files;
  }
  const index = files.findIndex(({ id }) => id === fileId);
  if (index > -1) {
    files[index].selected = true;
  }

  return files;
};

export const prepareTab = ({ created, updated, fileId, files, id, ...rest }) => ({
  ...rest,
  id,
  fileId,
  href: URL_EDIT.replace('#ID#', id),
  created: parseDate(created),
  updated: parseDate(updated),
  files: prepareFiles(files, fileId)
});

export async function searchTabs(params) {
  const { tabs } = await post(API_URL_INDEX, params);
  return tabs.map(prepareTab);
}
