export const URL_INDEX = '/tab';
export const URL_EDIT = '/tab/#ID#';

export const API_URL_INDEX = '/api/tabs.json';
export const API_URL_EDIT = '/api/tab/#ID#.json';

export const API_URL_CATEGORIES = '/api/categories.json';
export const API_URL_ARTISTS = '/api/artists.json';
