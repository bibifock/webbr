import post from '$lib/utils/post';
import uploadFile from '$lib/utils/uploadFile';
import { isInvalidTab } from '$lib/models/tab/isInvalidTab';

import { API_URL_ARTISTS, API_URL_EDIT } from '../constants';

export async function searchArtists(search) {
  const { artists } = await post(API_URL_ARTISTS, { search, limit: 10 });
  return artists;
}

export async function uploadTabFile({ files, file, index }) {
  const {
    file: { name, path, type }
  } = await uploadFile(file);

  const newFile = {
    type,
    name,
    src: path
  };

  const newFiles = [...files];

  if (index > -1) {
    newFiles[index] = { ...newFile, ...files[index] };
  } else {
    newFiles.push(newFile);
  }

  newFiles.sort((a, b) => a.name > b.name);
  return newFiles;
}

export async function saveTab(tab) {
  const { id = 0 } = tab;

  const errors = isInvalidTab(tab);
  if (errors) {
    return { errors };
  }

  const response = await post(API_URL_EDIT.replace('#ID#', id), tab);

  return response;
}

export function findFile({ file }) {
  return ({ name, type }) => file.name === `${name}.${type}`;
}
