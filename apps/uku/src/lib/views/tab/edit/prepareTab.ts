import { parseDate } from '$lib/utils/date';
import { URL_EDIT } from '$lib/views/tab/constants';

type TabFile = {
  id: string;
  selected?: boolean;
};

type Tab = {
  created: string;
  updated: string;
  files: TabFile[];
  fileId: string;
  id: string;
};

const prepareFiles = (files: TabFile[], fileId: string) => {
  if (!files || !files.length) {
    return [];
  }

  if (!fileId) {
    return files;
  }
  const index = files.findIndex(({ id }) => id === fileId);
  if (index > -1) {
    files[index].selected = true;
  }

  return files;
};

export const prepareTab = ({ created, updated, fileId, files, id, ...rest }: Tab) => ({
  ...rest,
  id,
  fileId,
  href: URL_EDIT.replace('#ID#', id),
  created: parseDate(created),
  updated: parseDate(updated),
  files: prepareFiles(files, fileId)
});
