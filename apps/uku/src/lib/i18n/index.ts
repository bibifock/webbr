import { init, register, waitLocale } from 'svelte-i18n';

register('fr', () => import('./locales/fr.json'));
register('en', () => import('./locales/en.json'));

export const loadLocale = async () => {
  init({ initialLocale: 'fr', fallbackLocale: 'fr' });
  await waitLocale();
};
