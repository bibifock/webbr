import User from 'app/models/user/User';

import { haveError, haveLoginError } from './utils';

export const getById = ({ params: { id } }) => User.getById(id);

export const register = async (user) => {
  const errors = haveError(user, {});

  if (errors.email) {
    throw errors;
  }

  const result = await User.canUpdate(user);
  if (!result) {
    errors.email = 'Are you sure this is the right email?!';
  }

  if (Object.keys(errors).length) {
    throw errors;
  }

  const { id } = result;
  await User.update({ ...user, id });

  return id;
};

export const login = async (user) => {
  const errors = haveLoginError(user);

  if (Object.keys(errors).length) {
    throw errors;
  }

  const { login, password } = user;

  const id = await User.login(login, password);

  if (!id) {
    const errors = {
      login: 'Unknown login or invalid password'
    };
    throw errors;
  }

  return id;
};

export default {
  getById,
  register,
  login
};
