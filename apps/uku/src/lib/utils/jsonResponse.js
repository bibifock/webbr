const jsonResponse = (func) => async (req, res, next) => {
  res.json = (response) => res.end(JSON.stringify(response));

  try {
    const output = await func(req, res, next);
    res.json(output);
  } catch (e) {
    let errors = e;
    let stack;
    if (e.message) {
      errors = { system: e.message };
      stack = e.stack;
    }

    res.json({ errors, stack });
  }
};

export default jsonResponse;
