export function post(endpoint: string, data: Record<string, string>) {
  return fetch(endpoint, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((r) => r.json());
}

export default post;
