import { API_URL_UPLOAD } from '$lib/views/constants';

const uploadFile = ({ file }) => {
  const formData = new FormData();
  formData.append('file', file);

  return fetch(API_URL_UPLOAD, {
    method: 'PUT',
    body: formData
  }).then((r) => r.json());
};

export default uploadFile;
