import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

const getDb = () =>
  open({
    filename: process.env.DB_SOURCE ?? '',
    driver: sqlite3.Database
  });

export default getDb;
