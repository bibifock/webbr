export const BASE_URL = '/api/';

function send({ method, path, data }) {
  const opts = { method, headers: {} };

  if (data) {
    opts.headers['Content-Type'] = 'application/json';
    opts.body = JSON.stringify(data);
  }

  return fetch(`${BASE_URL}${path}`, opts).then((r) => r.json());
}

export function get(path, token) {
  return send({ method: 'GET', path, token });
}

export function del(path, token) {
  return send({ method: 'DELETE', path, token });
}

export function post(path, data, token) {
  return send({ method: 'POST', path, data, token });
}

export function put(path, data, token) {
  return send({ method: 'PUT', path, data, token });
}

export default {
  get,
  del,
  post,
  put
};
