import faker from '$lib/utils/testing/faker';

import Component from './Title.svelte';

export default {
  title: 'molecules/title'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    title: 'I fought the law (bobby Fuller)',
    subtitle: 'Bobby Fuller Tour',
    ...props
  }
});

export const base = () => getStory();

export const link = () =>
  getStory({
    href: faker.internet.url(),
    target: '_blank'
  });

export const filled = () =>
  getStory({
    href: faker.internet.url(),
    target: '_blank',
    youtube: faker.internet.url(),
    file: faker.internet.url()
  });
