import { defaultProps as props } from './test.utils';
import Component from './File.svelte';

export default {
  title: 'molecules/file'
};

export const base = () => ({ Component, props });

export const unuploadedFile = () => ({
  Component,
  props: {
    type: 'pdf',
    name: "I'me waiting for a validation",
    path: '/tmp/balbla.pdf'
  }
});
