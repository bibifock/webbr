export const defaultProps = {
  name: 'name',
  type: 'pdf',
  fullpath: '/static/uploads/name.pdf',
  tabId: 2,
  created: new Date(2019, 10, 1),
  updated: new Date(2019, 11, 1)
};
