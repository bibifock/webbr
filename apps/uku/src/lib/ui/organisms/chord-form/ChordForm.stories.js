import Component from './ChordForm.svelte';

export default {
  title: 'organisms/chord-form'
};

export const base = () => ({
  Component,
  props: {
    name: 'Am7',
    chords: ['G', 'C', 'E', 'A'].map((name) => ({ name })),
    errors: {
      name: 'failed'
    }
  },
  on: {
    change: ({ detail }) => alert(JSON.stringify(detail))
  }
});
