import { defaultProps as file } from '$lib/ui/molecules/file/test.utils';

export const defaultProps = {
  href: '/tabs/id',
  title: 'A la belle étoile',
  artist: "Les yeux d'la tête",
  author: 'bbr',
  link: 'https://www.youtube.com/embed/esfJwdSl_7o',
  category: 'Chanson Française',
  level: 2,
  created: new Date(),
  updated: new Date(),
  files: [file, file]
};
