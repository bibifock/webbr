import { tabs } from './sample.json';
import Component from './ListTab.svelte';

export default {
  title: 'organisms/list-tab'
};

export const base = () => ({
  Component,
  props: {
    tabs
  },
  on: {
    click: ({ detail }) => alert(JSON.stringify(detail))
  }
});
