import { defaultProps } from '$lib/ui/organisms/tab/test.utils';

import Component from './TabEdit.svelte';

export default {
  title: 'organisms/tab-edit'
};

const categories = [
  { id: 1, label: 'Chanson française' },
  { id: 2, label: 'Chanson Internationales' }
];

const submit = ({ detail }) => alert(JSON.stringify(detail));
const fields = ['title', 'link', 'author', 'categoryId', 'level', 'artist'];

export const base = () => ({
  Component,
  props: {
    categories
  },
  on: { submit }
});

export const errors = () => ({
  Component,
  props: {
    ...defaultProps,
    categoryId: 2,
    categories,
    errors: fields.reduce((o, key) => ({ ...o, [key]: 'this is an error' }), {
      system: 'Am I going crazy? Have my years of wild hedonism finally caught up with me?'
    })
  },
  on: { submit }
});
