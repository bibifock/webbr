import Component from './YoutubeLink.svelte';

export default {
  title: 'atoms/youtube-link'
};

export const base = () => ({
  Component,
  props: { link: 'https://www.youtube.com/embed/esfJwdSl_7o' }
});

export const list = () => ({
  Component,
  props: {
    link: 'https://www.youtube.com/playlist?list=PLibS5rbMkb5QHUnOUA_12lidK5Xx6-tyJ'
  }
});

export const noPreview = () => ({
  Component,
  props: { link: 'http://www.perdu.com/' }
});
