import Component from './LastUpdate.svelte';

export default {
  title: 'atoms/last-update'
};

export const base = () => ({
  Component,
  props: {
    value: new Date(2019, 10, 1)
  }
});

export const string = () => ({
  Component,
  props: {
    value: '2019-12-13 17:41:45'
  }
});
