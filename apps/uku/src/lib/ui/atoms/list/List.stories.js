import TabFactory from '$lib/utils/testing/factory/Tab';
import ListTabItem from '$lib/ui/organisms/list-tab-item/ListTabItem.svelte';

import Component from './List.svelte';

export default {
  title: 'atoms/list'
};

export const base = () => ({
  Component,
  props: {
    component: ListTabItem,
    items: [TabFactory(), TabFactory(), TabFactory(), TabFactory()]
  }
});
