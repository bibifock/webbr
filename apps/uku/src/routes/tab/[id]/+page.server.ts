import { redirect } from '@sveltejs/kit';

import categoryModel from '$lib/models/category';
import tabModel from '$lib/models/tab/Tab';
import { prepareTab } from '$lib/views/tab/edit/prepareTab';

import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();
  const id = Number(params.id);

  if (!session?.id && !id) {
    throw redirect(302, '/tab');
  }

  const categories = await categoryModel.all();
  const resultTab = await tabModel.getById(params.id);
  const { files, ...tab } = prepareTab(resultTab);

  return {
    id,
    categories: categories.map((c) => ({ id: c.id, label: c.name })),
    tab,
    files
  };
}) satisfies PageServerLoad;
