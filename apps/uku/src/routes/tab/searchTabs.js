import post from '$lib/utils/post';
import { API_URL_INDEX } from '$lib/views/tab/constants';
import { prepareTab } from '$lib/views/tab/edit/prepareTab';

export async function searchTabs(params) {
  const { tabs } = await post(API_URL_INDEX, params);
  return tabs.map(prepareTab);
}
