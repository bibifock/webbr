import { json } from '@sveltejs/kit';

import artistModel from '$lib/models/artist/Artist';

import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request }) => {
  const { search, limit } = await request.json();

  const artists = await artistModel.all({ search, limit });

  return json({ artists });
}) satisfies RequestHandler;
