import { json } from '@sveltejs/kit';

import fileModel from '$lib/models/file/File';
import tabModel from '$lib/models/tab/Tab';
import { isInvalidTab } from '$lib/models/tab/isInvalidTab';

import { getFilesToAdd } from './getFilesToAdd';

import type { RequestHandler } from '@sveltejs/kit';
import type { TabFile } from './types';

export const GET = (async ({ params }) => {
  const tab = await tabModel.getById(params.id);

  return json({ tab });
}) satisfies RequestHandler;

const getFullpath = ({ fullpath }: { fullpath: string }) => fullpath;

const insertFilesForTab = async ({ tabId, files }: { tabId: number; files: TabFile[] }) => {
  const filesToUpdate = getFilesToAdd(files, tabId);

  if (!filesToUpdate.length) {
    return true;
  }

  const filesToAdd = filesToUpdate.filter(({ id }) => !id);

  // check news files aren't already in database
  const fullpaths = filesToAdd.map(getFullpath);
  const results = await fileModel.alreadyExists({ tabId, fullpaths });

  if (results) {
    return results.map(getFullpath);
  }

  await fileModel.saveAll(filesToUpdate);

  return true;
};

export const POST = (async ({ params, request }) => {
  const id = Number(params.id);
  const { files, ...tab } = await request.json();

  const errors = isInvalidTab(tab, {});

  const result = await tabModel.alreadyExists(tab);
  if (result && id !== result.id) {
    const { id: rowid, title } = result;
    errors.title = `${title} already present <a href="/tab/${rowid}" class="alert-link" target="_blank">here</a>`;
  }

  if (Object.keys(errors).length) {
    throw errors;
  }

  const { id: tabId } = await tabModel.save(tab);

  insertFilesForTab({ tabId, files });

  return json({
    success: true,
    id: tabId,
    tab: await tabModel.getById(tabId)
  });
}) satisfies RequestHandler;
