export type TabFile = {
  fullpath: string;
  id: number;
  name: string;
  src?: string;
  type: string;
};
