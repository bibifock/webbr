import { haveError, getFilesToAdd } from './utils';

describe('$lib/controllers/tabs utils', () => {
  describe('getFilesToAdd', () => {
    describe('should return empty array with', () => {
      it.each([undefined, [], [{}]])('%p', (files) => {
        expect(getFilesToAdd(files)).toEqual([]);
      });
    });

    it('should only return files with src keys', () => {
      const files = [
        { test: true },
        { src: true },
        { src: false },
        { test: 'jkejkde', src: 'dede' }
      ];

      expect(getFilesToAdd(files)).toHaveLength(files.filter(({ src }) => src).length);
    });
  });

  describe('haveError', () => {
    let props;
    beforeEach(() => {
      props = {
        title: 'A la belle étoile',
        artist: "Les yeux d'la tête",
        author: 'bbr',
        link: 'https://www.youtube.com/watch?v=esfJwdSl_7o',
        categoryId: 1,
        level: 2
      };
    });

    it('should be valid', () => {
      expect(haveError(props)).toBe(false);
    });

    it('should return succes value', () => {
      const toto = 'toto';
      expect(haveError(props, toto)).toBe(toto);
    });

    it('should be false with empty values', () => {
      const fields = Object.keys(props);
      const wanted = fields.reduce((obj, key) => {
        props[key] = null;
        return { ...obj, [key]: expect.any(String) };
      }, {});
      expect(haveError(props)).toEqual(wanted);
    });

    it('should detect bad links', () => {
      props.link = 'prepod';
      expect(haveError(props)).toEqual({ link: expect.any(String) });
    });
  });
});
