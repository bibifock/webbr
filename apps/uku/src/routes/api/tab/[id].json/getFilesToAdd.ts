import type { TabFile } from './types';

/**
 * prepare files for insert in db
 * @param array file
 * @param ?int tabId
 *
 * @return func ( file )
 */
export const getFilesToAdd = (files: TabFile[], tabId?: number) => {
  if (!files || !files.length) {
    return [];
  }

  return files
    .filter(({ src }) => src)
    .map(({ type, name, src, id, fullpath }) => ({
      // TODO remove space and special-chars
      fullpath: fullpath || `${name}_${tabId}.${type}`,
      id,
      type,
      name,
      src,
      tabId
    }));
};
