import fs from 'fs';
import { mkdtemp } from 'fs/promises';
import path from 'path';
import os from 'os';

import { json, error } from '@sveltejs/kit';

import type { RequestHandler } from '@sveltejs/kit';

const acceptedFileType = [
  'application/pdf',
  'application/vnd.oasis.opendocument.text',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
];

export const PUT = (async ({ locals, request }) => {
  if (!locals.session.data?.id) {
    throw error(403, "sorry, this isn't possible");
  }

  const formData = await request.formData();
  const file = formData.get('file');

  if (!(file instanceof File)) {
    throw error(400, 'missing file');
  }

  const { name, type } = file;
  if (acceptedFileType.indexOf(type) === -1 && !/^image\/.+$/.test(type)) {
    throw error(400, 'file type not accepted');
  }

  const { ext, name: basename } = path.parse(name);
  const tmpPath = (await mkdtemp(path.join(os.tmpdir(), 'uku-tab_'))) + ext;
  fs.writeFileSync(tmpPath, Buffer.from(await file.arrayBuffer()));

  return json({
    file: {
      name: basename,
      type: ext.substr(1),
      path: tmpPath
    }
  });
}) satisfies RequestHandler;
