import { json } from '@sveltejs/kit';

import tabModel from '$lib/models/tab/Tab';

import type { RequestHandler } from '@sveltejs/kit';

const orderByParam: Record<string, string> = {
  titleAsc: 'title ASC, artist ASC',
  titleDesc: 'title DESC, artist ASC',
  artistAsc: 'artist asc, title asc',
  artistDesc: 'artist desc, title asc',
  createdDesc: 'created desc, title ASC, artist ASC',
  updatedDesc: 'updated desc, title ASC, artist ASC'
};

export const POST = (async ({ request }) => {
  const { search, sort, limit } = await request.json();
  const orderBy = orderByParam[sort];

  const tabs = await tabModel.all({ search, orderBy, limit });

  return json({ tabs });
}) satisfies RequestHandler;
