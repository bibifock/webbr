module.exports = {
  extends: './.eslintrc.cjs',
  plugins: ['jest'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,
    extraFileExtensions: ['.svelte']
  },
  extends: [
    'plugin:svelte/all', // beware there will be rules update on all minor upgrade
    'plugin:svelte/prettier'
  ],
  overrides: [
    {
      files: ['*.svelte'],
      parser: 'svelte-eslint-parser',
      parserOptions: {
        parser: '@typescript-eslint/parser'
      }
    }
  ],
  settings: {
    'svelte3/typescript': () => require('typescript')
  },
  env: {
    browser: true,
    es2017: true,
    node: true,
    "jest/globals": true
  },
  rules: {
    'svelte/no-at-html-tags': 'warn',
    'svelte/block-lang': ['error', { script: 'ts' }],
    'svelte/experimental-require-slot-types': 'warn',
  }
};
