import { parsers } from './parsers';

import type { Parser } from './parsers';

export type ParserItem = {
  idFeed: number;
  title: string;
  url: string;
  created: Date;
  image?: string;
  description?: string;
};

type ParseFeedArgs = {
  type: Parser;
  config: string;
  id: number;
  url: string;
};

export const parseFeed = ({
  type,
  ...rest
}: ParseFeedArgs): Promise<ParserItem[]> =>
  parsers[type](rest) as Promise<ParserItem[]>;
