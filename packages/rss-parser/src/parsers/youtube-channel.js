import get from 'lodash/get';
import RssParser from 'rss-parser';

const rssParser = new RssParser({
  customFields: {
    item: [['media:group', 'mediaGroup']]
  }
});

const channelRSS =
  'https://www.youtube.com/feeds/videos.xml?channel_id=#CHANNEL_ID#';
const regChannelId = /channel\/(?<id>.+)$/;

const youtubeChannel = async ({ id: idFeed, url }) => {
  if (!regChannelId.test(url)) {
    throw Error(`ERROR: parse youtube-channel; ${idFeed}. ${url}`);
  }

  const { groups } = url.match(regChannelId);

  const urlRSS = channelRSS.replace('#CHANNEL_ID#', groups.id);
  let { items } = await rssParser.parseURL(urlRSS);

  return items.map(({ title, link: url, pubDate, mediaGroup }) => ({
    idFeed,
    title,
    url,
    created: new Date(pubDate).getTime(),
    image: get(mediaGroup, 'media:thumbnail.0.$.url'),
    description: get(mediaGroup, 'media:description')
      .join()
      .replace(/[\n\r]/g, '<br />')
  }));
};

export default youtubeChannel;
