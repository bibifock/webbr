import RssParser from 'rss-parser';

import rss from './rss';

jest.mock('rss-parser');

const url = 'https://www.dragonball-multiverse.com/flux.rss.php?lang=fr';
const idFeed = 12;

const mockParseReturn = (items) => {
  const parseURL = jest.fn().mockResolvedValue({ items });
  RssParser.mockReturnValue({ parseURL });

  return parseURL;
};

describe('app/controllers/feeds/parsers/rss', () => {
  beforeEach(() => {
    RssParser.mockClear();
  });

  it('should call parser with rigth url', async () => {
    const parseURL = mockParseReturn(Array(6).fill({}));
    await rss({ id: idFeed, url });

    expect(parseURL).toHaveBeenCalledWith(url);
  });

  it('should add idFeed on all items', async () => {
    mockParseReturn(Array(6).fill({}));
    const items = await rss({ id: idFeed, url });

    items.forEach((item) => expect(item.idFeed).toEqual(idFeed));
  });

  describe('should auto rename', () => {
    // eslint-disable-next-line mocha/no-setup-in-describe
    it.each([
      ['link', 'url'],
      ['content', 'description']
    ])('%p => %p', async (src, dst, defaultValue = 'test-value') => {
      const mock = {
        [src]: defaultValue
      };
      mockParseReturn(Array(6).fill(mock));

      const items = await rss({});
      items.forEach((i) => expect(i[dst]).toEqual(defaultValue));
    });
  });

  describe('should automatically convert date to created', () => {
    // eslint-disable-next-line mocha/no-setup-in-describe
    it.each(['date', 'pubDate'])('%p to created', async (key) => {
      const value = '2020-12-16T20:00:00Z';
      const mock = { [key]: value };
      mockParseReturn(Array(6).fill(mock));

      const timestamp = new Date(value).getTime();
      const items = await rss({ url });
      items.forEach(({ created }) => expect(created).toEqual(timestamp));
    });
  });

  describe('config options', () => {
    it('sort, should sort by value', async () => {
      mockParseReturn(
        Array.from(Array(10)).map((_, index) => ({ test: { index } }))
      );

      const items = await rss({ url, config: { sort: 'test.index' } });

      items.forEach((item, index) =>
        expect(item.test.index).toEqual(10 - (index + 1))
      );
    });

    describe('fields rename', () => {
      it('should simply rename extra string', async () => {
        const value =
          "Bon. Ca c'est bien gentil mais… À quel moment on trahit ?";
        mockParseReturn(Array(10).fill({ blabla: value, test: { value } }));

        const config = {
          fields: {
            blabla: 'description',
            ['test.value']: 'title'
          }
        };

        const items = await rss({ config });

        items.forEach((i) =>
          expect(i).toEqual(
            expect.objectContaining({
              description: value,
              title: value
            })
          )
        );
      });
    });
  });
});
