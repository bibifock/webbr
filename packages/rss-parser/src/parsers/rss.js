import get from 'lodash/get';
import omit from 'lodash/omit';
import RssParser from 'rss-parser';

const makeRenamer = (fields) => {
  const keys = Object.keys(fields);

  return (item) => {
    keys.forEach((k) => (item[fields[k]] = get(item, k)));

    return item;
  };
};

const rss = async ({ id: idFeed, url, config }) => {
  const rssParser = new RssParser();

  const result = await rssParser.parseURL(url);
  let items = result.items.map(
    ({ image = '', link, date, content, pubDate, ...item }) => {
      date = new Date(date);
      if (isNaN(date)) {
        date = new Date(pubDate);
      }

      if (content) {
        content = content.replace(/ href/g, ' target="_blank" href');
      }

      return {
        ...omit(item, 'id'),
        image,
        url: link,
        description: content,
        created: date.getTime(),
        idFeed
      };
    }
  );

  if (!config) {
    return items;
  }

  const { sort: sortBy, fields } = config;
  if (sortBy) {
    items.sort((a, b) => {
      const va = get(a, sortBy);
      const vb = get(b, sortBy);
      if (va < vb) return 1;
      if (va > vb) return -1;
      return 0;
    });
  }

  if (fields) {
    const renamer = makeRenamer(fields);
    items = items.map(renamer);
  }

  return items;
};

export default rss;
