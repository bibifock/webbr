import youtubeChannel from './youtube-channel';
import rss from './rss';

export const parsers = {
  ['youtube-channel']: youtubeChannel,
  ['rss-feed']: rss
};

export type Parser = keyof typeof parsers;
