import SQL from 'sql-template-strings';

import database from '../database';

import { model } from './model';

const getServiceId = async (service: 'spotify'): Promise<number> => {
  const db = await database.connect();

  const result = await db.get(SQL`
    SELECT id
    FROM feeds
    WHERE type = 'service' AND name = ${service}
  `);

  return result.id;
};

export const feedModel = {
  ...model,
  getServiceId
};
