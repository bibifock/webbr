import SQL from 'sql-template-strings';

import database from '../database';
import baseModel from '../model';
import { subscriptionsModel } from '../subscriptions';

const fields = ['id', 'name', 'type', 'url', 'config'];

const table = 'feeds';

const feedModel = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'url'
});

const selectWithUser = ({ join = 'INNER' } = {}) => {
  const subscriptionsTable = subscriptionsModel.config.table;
  const fields = [...model.getFields(), ...subscriptionsModel.getFields()];

  return SQL` SELECT `
    .append(fields.join(', '))
    .append(` FROM ${table} `)
    .append(` ${join} JOIN ${subscriptionsTable} on ${subscriptionsTable}.idFeed=${table}.id `);
};

/**
 * @param {object} user
 *
 * @return {Promise}
 */
const getByUser = async ({ id }) => {
  const subscriptionsTable = subscriptionsModel.config.table;
  const query = selectWithUser({ join: 'LEFT' })
    .append(` WHERE ( ${subscriptionsTable}.idUser IS NULL OR ${subscriptionsTable}.idUser=`)
    .append(SQL`${id} )`)
    .append(' AND ' + feedModel.config.table + ".type != 'service' ")
    .append(' ORDER BY name');

  return database.connect().then((db) => db.all(query));
};

/**
 * return followed feeds
 *
 * @return {Promise}
 */
const withSubscriptions = async (idFeed) => {
  const db = await database.connect();
  const query = selectWithUser().append(SQL` WHERE feeds.type != 'service' `);
  if (idFeed) {
    query.append(SQL` AND idFeed=${idFeed}`);
  }

  const result = await db.all(query);

  return result.reduce((o, { idUser, filter, ...row }) => {
    const index = o.findIndex((a) => a.id == row.id);
    if (index === -1) {
      o.push({
        ...row,
        subs: [
          {
            id: idUser,
            filter
          }
        ]
      });
      return o;
    }
    o[index].subs.push({ id: idUser, filter });
    return o;
  }, []);
};

export const model = {
  ...feedModel,
  getByUser,
  withSubscriptions
};
