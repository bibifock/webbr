import baseModel from './model';

const fields = ['idUser', 'idFeed', 'filter'];

const table = 'subscriptions';

const model = baseModel.init({
  fields,
  table
});

export const subscriptionsModel = {
  ...model,
  save: model.save
};
