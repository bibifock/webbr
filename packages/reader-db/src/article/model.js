import SQL from 'sql-template-strings';
import omit from 'lodash/omit';

import database from '../database';
import baseModel from '../model';
import { feedModel } from '../feed';
import { notificationsModel } from '../notifications';

const fields = ['id', 'idFeed', 'url', 'image', 'title', 'description', 'created', 'updated'];

const table = 'articles';

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  orderBy: 'created DESC',
  populateBy: 'url'
});

/**
 * @param {object} user
 * @param {object} opts
 *
 * @return {Promise}
 */
const getByUser = async ({ id }, opts = {}) => {
  const feedTable = feedModel.config.table;
  const userArtTable = notificationsModel.config.table;

  const feedPrefix = 'feed_';

  const fields = [
    ...model.getFields(),
    ...feedModel.getFields(feedPrefix),
    ...notificationsModel.getFields()
  ];

  const query = SQL` SELECT `
    .append(fields.join(','))
    .append(` FROM ${table}`)
    .append(` INNER JOIN ${feedTable} on ${feedTable}.id=${table}.idFeed`)
    .append(` INNER JOIN ${userArtTable} on ${userArtTable}.idArticle=${table}.id`)
    .append(` WHERE ${userArtTable}.idUser=`)
    .append(SQL`${id}`);

  if (opts.unread) {
    query.append(` AND ${userArtTable}.unread = 1 `);
  }

  query.append(' ORDER BY created DESC');

  const db = await database.connect();

  const result = await db.all(query);

  if (!result.length) {
    return [];
  }

  const isFeed = (k) => k.indexOf(feedPrefix) === 0;

  const output = result.map((row) => {
    const feedKeys = Object.keys(row).filter(isFeed);
    const feed = feedKeys.reduce(
      (o, k) => ({
        ...o,
        [k.replace(feedPrefix, '')]: row[k]
      }),
      {}
    );

    return {
      ...omit(row, feedKeys),
      feed
    };
  });

  return output;
};

export default {
  ...model,
  saveAndPopulate: model.saveAndPopulate,
  getByUser,
  findByProps: model.findByProps
};
