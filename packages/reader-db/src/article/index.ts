import SQL from 'sql-template-strings';

import database from '../database';
import model from './model';

type Article = {
  id: number;
  idFeed: number;
  image: string;
  title: string;
  description: string;
  url: string;
  created: string;
  unread?: boolean;
};

const getByFeed = async (idFeed: number, idUser?: number): Promise<Article[]> => {
  const query = SQL`
    SELECT
      art.id,
      art.idFeed,
      art.url,
      art.image,
      art.title,
      art.description,
      art.created,
      art.updated,
      no.unread
    FROM articles art
    LEFT JOIN notifications no ON no.idUser=${idUser} AND no.idArticle=art.id
    WHERE idFeed=${idFeed}
    ORDER BY created DESC
  `;
  const db = await database.connect();

  const result = await db.all(query);

  if (!result.length) {
    return [];
  }

  return result;
};

export const articleModel = {
  ...model,
  getByFeed
};
