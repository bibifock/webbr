import get from 'lodash/get';

import { concatValues } from './vege-data';
import database from './database';

/**
 * generate populateBy and saveBy functions
 *
 * @param {string} _.by pivot field
 * @param {object} _.model vege-data model
 * @param {func} _.populate
 *
 * @return {object} { populateBy, saveBy }
 */
export const generateSaveAndPopulate = ({ by, model, populate }) => {
  if (!populate) {
    populate = (items) =>
      database
        .connect()
        .then((db) => {
          const query = model.queries
            .select()
            .append(` WHERE ${by} IN (`)
            .append(concatValues(items.map((i) => i[by])))
            .append(')');

          return db.all(query);
        })
        .then((rows) =>
          items.map((item) => ({
            ...(rows.find((i) => i[by].toLowerCase() === item[by].toLowerCase()) || {}),
            ...item
          }))
        );
  }

  const { primaryKey } = model.config;

  const saveAndPopulate = (objects) => {
    let items = [];
    return populate(objects)
      .then((r) => {
        items = r;
        return model.saveAll(r);
      })
      .then(async () => {
        const uncomplete = items.filter((i) => !i[primaryKey]);
        if (!uncomplete.length) {
          return items;
        }

        const newItems = await populate(uncomplete);

        return items.filter((i) => i[primaryKey]).concat(newItems);
      });
  };

  return { saveAndPopulate, populate };
};

/**
 * @param {string} _.populateBy field to populate by
 * @param {object} ...rest vege-data model.init options
 *
 * @return {object}
 */
const init = ({ populateBy: by = '', ...config }) => {
  const model = database.model.init(config);

  if (by) {
    const { populate, saveAndPopulate } = generateSaveAndPopulate({ by, model });
    model.populate = populate;
    model.saveAndPopulate = saveAndPopulate;
  }

  Object.keys(model.queries).forEach(
    // for fix append object issue
    (key) => {
      model.queries[key + 'Raw'] = model.queries[key]().strings[0];
    }
  );
  model.allBy = model.findBy;
  model.allByProps = model.findByProps;
  model.getBy = (...args) => model.findBy(...args).then((r) => get(r, '0'));
  model.getByProps = (...args) => model.findByProps(...args).then((r) => get(r, '0'));

  model.getFields = (prefix = '') =>
    model.config.fields.map((f) => `${model.config.table}.${f} as ${prefix}${f}`);

  return model;
};

export default { init };
