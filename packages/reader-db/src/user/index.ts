import SQL from 'sql-template-strings';
import bcrypt from 'bcryptjs';

import database from '../database';

import { model, getAllFields, table } from './model';

type User = {
  id: number;
  email: string;
  name: string;
  created: string;
  updated: string;
  lastConnection: string;
  spotify: string;
};

const saltRounds = 10;

const isSpotifyEnabled = (spotify: string): boolean => {
  try {
    const json = JSON.parse(spotify);

    return 'accessToken' in json;
  } catch (e) {
    return false;
  }
};

/**
 * @param {string} _.login
 * @param {string} _.password
 *
 * @throws {Error} user not found
 * @throws {Error} login not working
 *
 * @return {Promise}
 */
const login = async ({ login, password }: { login: string; password: string }) => {
  const db = await database.connect();

  const query = getAllFields().append(
    SQL` WHERE email=${login} OR name=${login} AND password IS NOT NULL`
  );

  const result = await db.get(query);

  if (!result) {
    throw Error('user not found');
  }

  const { password: hash, spotify, ...user } = result;

  const isLogged = await bcrypt.compare(password, hash);

  if (!isLogged) {
    throw Error('sorry, but this will not be possible');
  }

  return {
    ...user,
    spotify: isSpotifyEnabled(spotify)
  };
};

/**
 * @param {object} user
 *
 * @throws {Error} user not found
 * @throws {Error} not possible
 * @throws {Error} name not available
 *
 * @return {object}
 */
const register = async (args: { email: string; password: string; name: string }) => {
  const { email, password, name } = args;
  const db = await database.connect();

  const user = await db.get(getAllFields().append(SQL` WHERE email=${email}`));

  if (!user) {
    throw Error('user not found');
  }

  if (user.password) {
    throw Error('sorry, this is not possible');
  }

  const result = (await model.findByProps({ name })) as User[];

  const names = result.filter((u) => u.email !== email);

  if (names.length) {
    throw Error('username, already taken');
  }

  const hash = await bcrypt.hash(password, saltRounds);

  await db.run(
    SQL` UPDATE `.append(table).append(SQL` SET
        updated=CURRENT_TIMESTAMP,
        password=${hash},
        name=${name}
        WHERE email=${email}
      `)
  );

  return args;
};

const saveSpotify = async (id: number, tokens?: { accessToken: string; refreshToken: string }) => {
  const jsonTokens = tokens ? JSON.stringify(tokens) : null;

  const db = await database.connect();
  await db.run(
    SQL`
      UPDATE `.append(table).append(SQL` SET
        updated=CURRENT_TIMESTAMP,
        spotify=${jsonTokens}
      WHERE id=${id}
    `)
  );
};

type UserSpotifyAccount<Spotify = { accessToken: string; refreshToken: string }> = {
  id: number;
  spotify: Spotify;
};

const getSpotifyAccounts = async (): Promise<UserSpotifyAccount[]> => {
  const db = await database.connect();

  const results: UserSpotifyAccount<string>[] = await db.all(SQL`
    SELECT id, spotify
    FROM users
    WHERE spotify IS NOT NULL
  `);

  return results
    .filter((v: UserSpotifyAccount<string>) => isSpotifyEnabled(v.spotify))
    .map(
      ({ id, spotify }: UserSpotifyAccount<string>): UserSpotifyAccount => ({
        id,
        spotify: JSON.parse(spotify)
      })
    );
};

export const userModel = {
  ...model,
  saveSpotify,
  getSpotifyAccounts,
  login,
  register
};
