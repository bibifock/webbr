import SQL from 'sql-template-strings';

import database from '../database';

const fields = ['id', 'email', 'name', 'created', 'updated', 'lastConnection', 'spotify'];

export const table = 'users';

export const getAllFields = () =>
  SQL`SELECT password, `.append(fields.join(',')).append(' FROM ' + table);

export const model = database.model.init({
  fields,
  table,
  primaryKey: 'id'
});
