import SQL from 'sql-template-strings';

import { concatValues } from './vege-data';
import database from './database';
import baseModel from './model';

const fields = ['idUser', 'idArticle', 'unread'];

const table = 'notifications';

const model = baseModel.init({
  fields,
  table
});

/**
 * save only passed props
 * @param {array} data to save
 *
 * @return {Promise}
 */
const saveNew = (items) => {
  const query = SQL`INSERT OR IGNORE INTO `
    .append(table + '(idUser, idArticle) VALUES')
    .append(concatValues(items, (i) => SQL`(${i.idUser}, ${i.idArticle})`));

  return database.connect().then((db) => db.run(query));
};

export const notificationsModel = {
  ...model,
  saveNew
};
