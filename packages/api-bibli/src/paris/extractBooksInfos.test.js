import { parisBibli as api } from './api-index';
import { extractBooksInfos } from './extractBooksInfos';

describe('extractBooksInfos', () => {
  beforeEach(() => {
    jest.spyOn(api, 'search');
  });

  afterEach(() => {
    api.search.mockRestore();
  });

  it('when saga is defined, should return only book with saga', async () => {
    const apiResult = [
      {
        _sorted: 'le vieil homme et la guerre',
        author: 'John Scalzi ',
        title: 'Le vieil homme et la guerre',
        uid: 419816,
        saga: undefined,
        description: undefined,
        isbn: '9782841723560',
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/SzWOwuS2gFNm1WTbyVJpzQ2/978-2-84172-356-0/MEDIUM',
        books: []
      },
      {
        _sorted: 'le vieil homme et la guerre1le vieil homme et la guerre. 1',
        author: 'John Scalzi ',
        title: 'Le vieil homme et la guerre. 1',
        uid: 856052,
        saga: {
          query: 'SeriesTitle_exact:"Le vieil homme et la guerre"',
          title: 'Le vieil homme et la guerre',
          tome: '1',
          href: '/search?s=SeriesTitle_exact:"Le vieil homme et la guerre"'
        },
        description:
          'A 75 ans, John Perry intègre les forces de défenses coloniales, avec un aller simple pour les étoiles. Sa femme morte, plus rien ne le retient. Il veut défendre la Terre contre les espèces intelligentes qui menacent son espace interstellaire vital et obtenir le statut de colon sur une nouvelle planète. ©Electre 2016<br/><br/>',
        isbn: '9782811217778',
        thumb:
          'https://covers.archimed.fr/Cover/VPCO/MONO/G7gtUCuVy0CL0T8uo09t_A2/978-2-8112-1777-8/MEDIUM',
        books: []
      }
    ];

    api.search.mockResolvedValue(apiResult);

    const novel = {
      title: 'Le viel homm et la guerre',
      saga: {
        query: 'SeriesTitle_exact:"Le vieil homme et la guerre"'
      }
    };

    const { novels } = await extractBooksInfos(novel);

    const uids = apiResult.filter(({ saga }) => saga).map(({ uid }) => uid);

    novels.forEach((item) => expect(uids).toContain(item.uid));
  });
});
