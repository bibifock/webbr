import get from 'lodash/get';
import uniq from 'lodash/uniq';
import uniqBy from 'lodash/uniqBy';

import { search } from './api/search';
import {
  APILoan,
  APIBook,
  Book,
  Loan,
  APINovel,
  Novel,
  APISaga,
  Saga
} from '../types';

const parseLoan = (loan?: APILoan): Loan | undefined => {
  if (!loan) {
    return;
  }

  const {
    renewData,
    isLate,
    isSoonLate,
    status,
    whenBack,
    canRenew,
    cannotRenewReason,
    location,
    locationLink
  } = loan;

  return {
    library: {
      name: location,
      url: locationLink
    },
    whenBack,
    details: JSON.stringify({
      renewData,
      canRenew,
      cannotRenewReason,
      isLate,
      isSoonLate,
      status
    })
  };
};

/**
 * parse book infos
 *
 */
const parseAPIBook =
  (indexNovel: number) =>
  ({ holding, loan, ...b }: APIBook): Book => ({
    ...b,
    details: JSON.stringify({ holding }),
    indexNovel,
    loan: parseLoan(loan)
  });

/**
 *
 * return tomeIndex
 */
const getTomeIndex = (
  novels: Omit<Novel, 'saga' | 'books'>[],
  sagaVolume?: number
) => {
  if (!sagaVolume) {
    return novels.length;
  }

  const index = novels.findIndex((n) => n?.sagaVolume === sagaVolume);
  return index > -1 ? index : novels.length;
};

/**
 * create saga query
 */
const getSagaQuery = ({ title, query }: { title: string; query: string }) =>
  query || `SeriesTitle_exact:"${title}"`;

export const extractBooksInfos = async (params: APINovel) => {
  if (!params.saga) {
    return {
      books: params.books.map(parseAPIBook(0)),
      authors: [{ name: params.author }],
      novels: [
        {
          ...params,
          title: params.title.replace(/ : roman$/, ''),
          indexNovel: 0,
          wanted: true
        }
      ]
    };
  }

  const result = await search(getSagaQuery(params.saga));

  const content = result
    .filter(
      (
        apiNovel: APINovel
      ): apiNovel is Omit<APINovel, 'saga'> & { saga: APISaga } =>
        !!apiNovel?.saga
    )
    .reduce<{
      novels: Omit<Novel, 'books' | 'saga'>[];
      books: Book[];
      authors: string[];
      saga?: Saga;
    }>(
      (o, item) => {
        const {
          books,
          saga: { tome, ...saga },
          author,
          ...apiNovel
        } = item;

        const sagaVolume = tome;

        // bon là c'est un peu hasardeux, j'espère que ça ne foirera pas :/
        // en même temps vu l'heure ça ne peux que être ue mauvaise idée :D
        const wanted =
          get(params, 'title', '').toLowerCase() ===
          apiNovel.title.toLowerCase();

        o.saga = saga;
        o.authors.push(author);
        const indexNovel = getTomeIndex(o.novels, sagaVolume);
        if (indexNovel >= o.novels.length) {
          o.novels.push({
            ...apiNovel,
            sagaVolume: !isNaN(sagaVolume) ? sagaVolume : undefined,
            author,
            indexNovel,
            wanted
          });
        } else {
          o.novels[indexNovel].wanted = o.novels[indexNovel].wanted || wanted;
        }

        if (wanted) {
          // si c'est le bon bouquin on récupère les loans
          params.books.forEach((b) => {
            const index = books.findIndex((item) => item.isbn === b.isbn);
            if (index < 0) {
              return;
            }

            books[index].loan = b.loan;
          });
        }

        o.books = o.books.concat(books.map(parseAPIBook(indexNovel)));

        return o;
      },
      { novels: [], books: [], authors: [] }
    );

  if (!content.novels.length) {
    return null;
  }

  return {
    ...content,
    saga: {
      ...content.saga,
      nbNovels: uniqBy(content.novels, 'sagaVolume').length
    },
    authors: uniq(content.authors).map((name) => ({ name }))
  };
};
