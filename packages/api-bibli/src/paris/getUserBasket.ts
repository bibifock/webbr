import set from 'lodash/set';
import get from 'lodash/get';

import { parisBibli as api } from './api-index';
import { UserLogin, APINovel, APILoan } from '../types';

/**
 * @param {object} loan
 * @param {array} basket
 *
 * @return {arr}
 */
const getNovelBookKey = ({ holding }: APILoan, basket: APINovel[]) => {
  let bookIndex;
  const novelIndex = basket.findIndex(({ books }) => {
    const result = books.findIndex((b) => b.holding.id === holding.id);
    if (result > -1) {
      bookIndex = result;
      return true;
    }

    return false;
  });

  return `${novelIndex}.books.${bookIndex}`;
};

export const getUserBasket = async (userLogin: UserLogin) => {
  await api.getAuthCookies(userLogin);

  const [basket, loans] = await Promise.all([
    api.getBasket({ ...userLogin, logged: true }),
    api.getLoans({ ...userLogin, logged: true })
  ]);

  const notInBasket = loans.filter((loan: APILoan) => {
    if (!loan.inBasket) {
      return true;
    }

    const key = getNovelBookKey(loan, basket);
    set(basket, key, { ...get(basket, key, {}), loan });

    return false;
  });

  const toAdd = await Promise.all(
    notInBasket.map((loan: APILoan) =>
      api.search(loan.searchId).then(([novel]) => {
        if (novel === undefined) {
          // eslint-disable-next-line no-console
          console.warn('getUserBasket::searchId not found', loan);
          return undefined;
        }
        const index = novel.books.findIndex(
          (b) => b.holding.id === loan.holding.id
        );

        if (index < -1) {
          // eslint-disable-next-line no-console
          console.warn('loan book not found', loan);
        } else {
          novel.books[index].loan = loan;
        }

        return novel;
      })
    )
  );

  return basket.concat(
    toAdd.filter((v: APINovel | undefined): v is APINovel => v !== undefined)
  );
};
