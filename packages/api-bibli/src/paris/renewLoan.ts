import { parisBibli as api } from './api-index';
import { fetchWithCookies } from './fetchWithCookies';
import { URL_ACTION_LOANS_RENEW } from './urls';

import type { LoanRenewData, UserLogin } from '../types';

export const renewLoan = async (userLogin: UserLogin, loan: LoanRenewData) => {
  await api.getAuthCookies(userLogin);

  return fetchWithCookies(URL_ACTION_LOANS_RENEW, {
    method: 'POST',
    body: JSON.stringify({
      serviceCode: 'SYRACUSE',
      loans: [loan]
    }),
    headers: {
      'content-type': 'application/json' // Is set automatically
    }
  })
    .then((r) => r.json())
    .then(({ success, d }) => {
      if (!success) {
        throw 'renew loans failed ';
      }

      const { ErrorCount, Errors } = d;

      if (ErrorCount > 0) {
        throw 'renew loans operation failed: ' + Errors[0].Value;
      }

      return true;
    });
};
