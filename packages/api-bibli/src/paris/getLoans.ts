import { parisBibli as api } from './api-index';
import { fetchWithCookies } from './fetchWithCookies';
import { UserLogin, APILoan, ParisLoan } from '../types';
import { URL_LOANS } from './urls';

export const getLoans = async (userLogin: UserLogin): Promise<APILoan[]> => {
  await api.getAuthCookies(userLogin);

  const queryString = new URLSearchParams({
    serviceCode: 'SYRACUSE',
    token: `${Date.now()}`,
    userUniqueIdentifier: '',
    timestamp: `${Date.now()}`
  }).toString();

  return fetchWithCookies(`${URL_LOANS}?${queryString}`)
    .then((res) => res.json())
    .then((response) => {
      const { success, d, errors } = response;
      if (!success) {
        throw 'getLoans failed: ' + errors[0].msg;
      }

      return d.Loans.map((loans: ParisLoan) => ({
        renewData: {
          HoldingId: loans.HoldingId,
          RecordId: loans.RecordId,
          Id: loans.HoldingId
        },
        holding: {
          id: loans.RecordId
        },
        searchId: loans.HoldingId,
        title: loans.Title,
        url: loans.TitleLink,
        location: loans.Location.trim(),
        locationLink: loans.AdditionalProperties.LocationLink,
        thumb: loans.ThumbnailUrl,
        canRenew: loans.CanRenew,
        cannotRenewReason: loans.CannotRenewReason,
        isLate: loans.IsLate,
        isSoonLate: loans.IsSoonLate,
        inBasket: loans.IsInUserBasket,
        status: loans.State,
        whenBack: parseInt(loans.WhenBack.replace(/\/Date\((.+)\+.+/gi, '$1'))
      }));
    });
};
