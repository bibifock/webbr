import chunk from 'lodash/chunk';

import { parisBibli as api } from './api-index';
import { APIHolding } from '../types';

type Library = {
  name: string;
  url: string;
};

type Bookcase = {
  idBook: string;
  isAvailable: 0 | 1;
  isReservable: 0 | 1;
  whenBack: string;
  details: string;
  libraryURL: string;
};

type BookcasesAndLibraries = { bookcases: Bookcase[]; libraries: Library[] };

/**
 * load all book cases informations
 * @param {array} books
 *
 * @return {promise}
 */
export const getBookcases = async (
  books: { details: string; id: string }[]
) => {
  const iterations = chunk(books, 10);

  let results: Array<BookcasesAndLibraries | undefined> = [];
  for (const it of iterations) {
    const items: Array<BookcasesAndLibraries | undefined> = await Promise.all(
      it.map((book: { details: string; id: string }) => {
        const { holding } = JSON.parse(book.details);
        if (!holding) {
          return undefined;
        }
        const { id, base } = holding;

        return api
          .getHoldings(id, base)
          .then((result: APIHolding[]) =>
            result.reduce<BookcasesAndLibraries>(
              (o, holding) => {
                if (!holding) {
                  return o;
                }

                const {
                  site,
                  siteLink,
                  isAvailable,
                  isReservable,
                  whenBack,
                  cote,
                  status
                } = holding;

                const currLib = o.libraries.find((v) => v.url === siteLink);

                return {
                  libraries: currLib
                    ? o.libraries
                    : [...o.libraries, { name: site, url: siteLink }],
                  bookcases: [
                    ...o.bookcases,
                    {
                      libraryURL: currLib?.url ?? siteLink,
                      idBook: book.id,
                      isAvailable: isAvailable ? 1 : 0,
                      isReservable: isReservable ? 1 : 0,
                      whenBack,
                      details: JSON.stringify({ cote, status })
                    }
                  ]
                };
              },
              { bookcases: [], libraries: [] }
            )
          )
          .catch((e) => {
            // eslint-disable-next-line no-console
            console.error('[getBookcases]', book, e);
            return { bookcases: [], libraries: [] };
          });
      })
    );

    results = results.concat(items);
  }

  return results.filter(
    (v?: BookcasesAndLibraries): v is BookcasesAndLibraries =>
      v !== undefined && v.bookcases.length > 0
  );
};
