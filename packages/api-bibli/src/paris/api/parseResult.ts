import Url from 'url';

import get from 'lodash/get';

import { APINovel, APISaga, APIBook } from '../../types';

const regSaga =
  /class="tarzan linktype-link"[^>]+href="(?<link>[^"]+QUERY=(?<query>[^&]+)[^"]+)"[^>]+>(?<title>[^<]+)<\/a>( (livre|tome))* (?<tome>\d+)/i;
const regTomePart = /\. (?<tomePart>\d+)$/;

const regCat =
  /class="tarzan linktype-link"[^>]+href="(?<link>[^"]+QUERY=(?<query>[^&]+)[^"]+)"[^>]+>(?<title>[^<]+)<\/a>/i;

const regThumStr =
  '"(?<thumb>[^"]+covers.archimed.fr[^"]+\\/%%ISBN%%\\/[^?"]+)';

type ResultItem = {
  FriendlyUrl: string;
  Resource: {
    Id: string;
    Crtr: string;
    Ttl: string;
    RscUid: string;
    Desc: string;
    RscId: string;
    RscBase: string;
    Pbls: string;
  };
  CustomResult: string;
};

type Result = {
  Results: ResultItem[];
  HtmlResult: string;
};

type RegExpGroups<T extends string> =
  | (RegExpMatchArray & {
      groups?: { [name in T]: string } | { [key: string]: string };
    })
  | null;

const getTomePart = (saga: string, title?: string): string | undefined => {
  // roshar case: saga with tomes splitted:
  if (!title || !/roshar/i.test(saga)) {
    return undefined;
  }

  const match: RegExpGroups<'tomePart'> = title.match(regTomePart);
  if (!match) {
    return undefined;
  }

  const { tomePart } = match.groups || {};

  return tomePart;
};

const getSagaTitle = (saga: string): string =>
  saga
    .trim()
    .replace(/Le livres des Terres/i, 'Le livre des Terres')
    .replace(/^les annales du disque.monde.?$/i, 'Les annales du disque-monde');

const getSaga = (
  customResult: string,
  novelTitle: string
): APISaga | undefined => {
  const match: RegExpGroups<'title' | 'query' | 'tome' | 'link'> =
    customResult.match(regSaga);
  if (!match?.groups) {
    return undefined;
  }

  const saga = match.groups;

  const queryObject = Url.parse(saga.link, true).query;

  const tomePart = getTomePart(saga.title, novelTitle);

  return {
    title: getSagaTitle(saga.title),
    query: queryObject.QUERY as string,
    tome: tomePart
      ? parseFloat(`${saga.tome}.${tomePart}`)
      : (parseInt(saga.tome) ?? 0)
  };
};

const getTitle = (title: string) =>
  title.replace(/ : roman$/, '').replace('Émissaires', 'Emissaires');

const getSortedTome = (tome = 0) => {
  if (tome < 10) {
    return `00${tome}`;
  }
  if (tome < 100) {
    return `0${tome}`;
  }

  return tome;
};

/**
 * @param {object} _.Results
 *
 * @return {array}
 */
export const parseResult = ({ Results, HtmlResult }: Result): APINovel[] =>
  Results.map(({ FriendlyUrl: url, Resource: r, CustomResult }) => {
    const { groups: category } = CustomResult.match(regCat) || {};

    const isbnRaw = (r.Id || '')
      .replace(/isbn:/, '')
      .replace(/\(éd\..+\)/, '')
      .trim();
    const isbn = isbnRaw.replace(/[-]/g, '');

    const reg = new RegExp(regThumStr.replace('%%ISBN%%', isbnRaw));
    const thumb = get(HtmlResult.match(reg), 'groups.thumb');

    const author = r.Crtr || '-';
    const title = getTitle(r.Ttl);
    const uid = r.RscUid;
    const description = r.Desc;
    const saga = getSaga(CustomResult, title);

    const tomeSorted = getSortedTome(saga?.tome);
    return {
      _sorted: [get(saga, 'title'), tomeSorted, title].join('').toLowerCase(),
      url,
      author: author.replace(/\. Auteur$/, '').replace(/\(.+/, ''),
      title,
      uid,
      saga,
      thumb,
      edition: get(category, 'title') ?? r.Pbls,
      isbn,
      description,
      holding: {
        id: r.RscId,
        base: r.RscBase
      }
    };
  })
    .sort((a, b) => {
      if (a._sorted < b._sorted) {
        return -1;
      }

      if (a._sorted > b._sorted) {
        return 1;
      }

      return 0;
    })
    .reduce<APINovel[]>(
      (o, { thumb, url, isbn, edition, holding, ...v }, i) => {
        const book: APIBook = { thumb, isbn, url, edition, holding };
        const lastIndex = o.length - 1;
        if (i === 0 || o[lastIndex]._sorted !== v._sorted) {
          o.push({ ...v, url, isbn, thumb, books: [book] });
        } else {
          o[lastIndex].books.push(book);
        }

        return o;
      },
      []
    );
