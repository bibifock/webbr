import { parseResult } from './parseResult';
import rosharResult from './roshars.json';

it('should parse correctly saga books', () => {
  expect(parseResult(rosharResult)).toEqual([
    {
      _sorted: 'les archives de roshar1.1la voie des rois. 1',
      author: 'Brandon Sanderson ',
      title: 'La voie des rois. 1',
      uid: 1010450,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '1.1'
      },
      description:
        "A Roshar, monde de pierres et d'orages, des épées et des armures qui transforment les hommes en guerriers quasi invincibles sont échangées contre des royaumes et sont à l'origine de guerres. Kaladin, qui possède une de ses armes pour protéger son petit frère, a été réduit en esclavage et lutte pour sauver les hommes dans une de ces guerres insensées. ©Electre 2018<br/><br/>",
      isbn: '9782253132905',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/BltVtaVax0U2zyP2V7PiZg2/978-2-253-13290-5/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar1.2la voie des rois. 2',
      author: 'Brandon Sanderson ',
      title: 'La voie des rois. 2',
      uid: 953192,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '1.2'
      },
      description:
        "A Roshar, monde de pierres et d'orages, des épées et des armures mystiques qui transforment les hommes en guerriers quasi invincibles sont échangées contre des royaumes, provoquant des guerres. Kaladin, qui possède une de ces armes pour protéger son petit frère, a été réduit en esclavage. Il lutte pour sauver les hommes lors de ces conflits insensés. David Gemmell legend award 2011. ©Electre 2018<br/><br/>",
      isbn: '9782253132912',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/zdaQncqJLRAEsjrnN1CwPg2/978-2-253-13291-2/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar2.1le livre des radieux. 1',
      author: 'Brandon Sanderson ',
      title: 'Le livre des radieux. 1',
      uid: 879049,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '2.1'
      },
      description:
        "Roshar, monde de pierres et d'orages où les familles aristocratiques possèdent des éclats de lames ou de plaques provenant des épées et des armures mystiques d'anciens guerriers quasi invincibles. Les conflits se multiplient pour la conquête de ces puissants vestiges issus de l'Eclat. Suite du cycle de La voie des rois. ©Electre 2017<br/><br/>",
      isbn: '9782253191292',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/V1xIWmF9M0TtGJaNlYFnjw2/978-2-253-19129-2/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar2.2le livre des radieux. 2',
      author: 'Brandon Sanderson ',
      title: 'Le livre des radieux. 2',
      uid: 879048,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '2.2'
      },
      description:
        "Suite des guerres sur Roshar, monde de pierres et d'orages où les familles aristocratiques possèdent des éclats de lames ou de plaques provenant des épées et des armures mystiques d'anciens guerriers quasi invincibles. Les conflits se multiplient pour la conquête de ces puissants vestiges issus de l'Eclat. ©Electre 2017<br/><br/>",
      isbn: '9782253191308',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/YNbY3mUWhsSXMK4sSncSOQ2/978-2-253-19130-8/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar3.1justicière. 1',
      author: 'Brandon Sanderson ',
      title: 'Justicière. 1',
      uid: 1124788,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '3.1'
      },
      description:
        "La tempête éternelle sévit à nouveau sur Roshar, réveillant la véritable nature des serviteurs parshes. Les Néantifères veulent assouvir leur vengeance. Le peuple Alethi s'apprête à combattre. ©Electre 2019<br/><br/>",
      isbn: '9782253083726',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/avhDK3y-F3bo6EpNUEXY8w2/978-2-253-08372-6/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar3.2justicière. 2',
      author: 'Brandon Sanderson ',
      title: 'Justicière. 2',
      uid: 1135731,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '3.2'
      },
      description:
        "La tempête éternelle s'abat de nouveau sur Roshar et réveille la véritable nature des serviteurs parshes. Les Néantifères, assoiffés de vengeance et constituant une grande armée, sont de retour. Réfugié dans les nuages, au-dessus de la tempête, dans la cité-tour d'Urithiru, le peuple Alethi se prépare à les combattre. ©Electre 2019<br/><br/>",
      isbn: '9782253083733',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/r6GMMKBLZJTMTIg3jp9D3g2/978-2-253-08373-3/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar4.1rythme de guerre. 1',
      author: 'Brandon Sanderson ',
      title: 'Rythme de guerre. 1',
      uid: 1219611,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '4.1'
      },
      description:
        "La coalition formée par Dalinar et ses Chevaliers radieux pour résister à l'invasion ennemie n'a pas réussi à prendre l'avantage. Chacune de ses décisions se voit de plus suspendue à la menace d'une trahison par son allié Taravangian. Dans le même temps, les avancées technologiques des savants de Navani bouleversent le cours de la guerre, tandis que l'ennemi prépare une mystérieuse opération. ©Electre 2021<br/><br/>",
      isbn: '9782253242222',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/wodO9wIDfOpGl-CLfqz60A2/978-2-253-24222-2/MEDIUM',
      books: expect.any(Array)
    },
    {
      _sorted: 'les archives de roshar4.2rythme de guerre. 2',
      author: 'Brandon Sanderson ',
      title: 'Rythme de guerre. 2',
      uid: 1246850,
      saga: {
        query: 'SeriesTitle_exact:"Les archives de Roshar"',
        title: 'Les archives de Roshar',
        tome: '4.2'
      },
      description:
        "A l'exception de Kaladin et Lift, les Radieux sont plongés dans l'inconscience et la tour d'Utithiru est tombée dans les mains des Fusionnés. Prisonnière de Raboniel, Navani mène des expériences pour découvrir les secrets de la Flamme. Dans le même temps, Adolin tente une manoeuvre pour rallier les sprènes d'honneur à sa cause. ©Electre 2021<br/><br/>",
      isbn: '9782253242239',
      thumb:
        'https://covers.archimed.fr/Cover/VPCO/MONO/lWGeZBibbPib3EbCPy9IYg2/978-2-253-24223-9/MEDIUM',
      books: expect.any(Array)
    }
  ]);
});
