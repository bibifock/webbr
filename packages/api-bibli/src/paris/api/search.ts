import { APINovel } from '../../types';
import { URL_SEARCH } from '../urls';

import { parseResult } from './parseResult';

export const search = (search: string): Promise<APINovel[]> =>
  fetch(URL_SEARCH, {
    method: 'POST',
    body: JSON.stringify({
      query: {
        FacetFilter: '{"_586":"Livre||BD"}',
        QueryString: search,
        ScenarioCode: 'CATALOGUE',
        ResultSize: 100
      }
    }),
    headers: {
      'content-type': 'application/json' // Is set automatically
    }
  })
    .then((r) => r.json())
    .then(({ success, d, errors }) => {
      if (!success) throw errors;

      return parseResult(d);
    });
