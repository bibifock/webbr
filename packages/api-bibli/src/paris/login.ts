import { URL_LOGIN } from './urls';
import { UserLogin } from '../types';
import { fetchWithCookies } from './fetchWithCookies';

export const login = async ({ username, password }: UserLogin) => {
  const formData = new FormData();
  formData.append('username', username);
  formData.append('password', password);

  const response = await fetchWithCookies(URL_LOGIN, {
    method: 'POST',
    body: formData,
    credentials: 'include'
  });

  const { success } = await response.json();

  if (success) {
    return true;
  }

  throw new Error('[Paris Bibli] login failed');
};
