import makeFetchCookie from 'fetch-cookie';

export const fetchWithCookies = makeFetchCookie(fetch);
