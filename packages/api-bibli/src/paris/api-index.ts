import get from 'lodash/get';

import { ParisHolding, APIHolding, UserLogin } from '../types';

import { URL_HOLDING, URL_BASKET } from './urls';
import { login } from './login';
import { parseResult } from './api/parseResult';
import { search } from './api/search';
import { fetchWithCookies } from './fetchWithCookies';
// TODO remove this
import { getLoans } from './getLoans';
import { getBookcases } from './getBookcases';
import { extractBooksInfos } from './extractBooksInfos';
import { getUserBasket } from './getUserBasket';
import { renewLoan } from './renewLoan';

// TODO rename this package parisBibli -> bibli-api
// TODO move all paris bibli in subdir paris/
// TODO in index create a function that return object with good function for good api ?
// TODO create dir romainville
// TODO add source param romainville and try to parse romainville website params

const getHoldings = (RscId: string, Docbase: string): Promise<APIHolding[]> =>
  fetch(URL_HOLDING, {
    method: 'POST',
    body: JSON.stringify({
      Record: { RscId, Docbase }
    }),
    headers: {
      'content-type': 'application/json' // Is set automatically
    }
  })
    .then((res) => res.json())
    .then(({ success, d }) => {
      if (!success) {
        throw Error('holding request failed');
      }

      if (!d?.Holdings) {
        // eslint-disable-next-line no-console
        console.warn(`holdings not found for: ${RscId} - ${Docbase}`);
        return [];
      }

      const holdings = d.Holdings.map(
        ({
          IsAvailable,
          IsReservable,
          Site,
          Statut,
          WhenBack,
          Cote,
          Other
        }: ParisHolding) => ({
          isAvailable: IsAvailable,
          isReservable: IsReservable,
          site: Site.trim(),
          status: Statut,
          whenBack: WhenBack,
          cote: Cote,
          siteLink: get(
            Other.find(
              ({ Key: key }: { Key: string }) => key === 'SiteLabelLink'
            ),
            'Value',
            // here ot fix for missing url see how to improve it
            Site.trim().toLocaleLowerCase() === '75005 - heure joyeuse'
              ? 'https://www.paris.fr/lieux/bibliotheque-l-heure-joyeuse-2882'
              : ''
          )
        })
      );

      holdings.sort((a: { site: string }, b: { site: string }) =>
        a.site < b.site ? -1 : 1
      );

      return holdings;
    });

export const getAuthCookies = async ({
  username,
  password,
  logged
}: UserLogin): Promise<boolean> => {
  if (logged) {
    return logged;
  }

  await login({ username, password });

  return true;
};

export const getBasket = async (userLogin: UserLogin) => {
  await getAuthCookies(userLogin);

  return fetchWithCookies(URL_BASKET, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify({
      query: {
        ResultSize: 100,
        Page: 0,
        SearchInput: '',
        LabelFilter: []
      }
    }),
    headers: {
      'content-type': 'application/json' // Is set automatically
    }
  })
    .then((r) => r.json())
    .then((res) => {
      const { success, d } = res;
      if (!success) {
        throw Error('request failed');
      }

      return parseResult(d);
    });
};

export const parisBibli = {
  search,
  getAuthCookies,
  getHoldings,
  getBasket,
  getLoans,
  getBookcases,
  extractBooksInfos,
  getUserBasket,
  renewLoan
};
