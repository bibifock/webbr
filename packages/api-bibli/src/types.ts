export type ParisHolding = {
  IsAvailable: boolean;
  IsReservable: boolean;
  Site: string;
  Statut: string;
  WhenBack: string;
  Cote: string;
  Other: Array<{ Key: string }>;
};

export type APIHolding = {
  isAvailable: boolean;
  isReservable: boolean;
  site: string;
  status: string;
  whenBack: string;
  cote: string;
  siteLink: string;
};

export type ParisLoan = {
  HoldingId: string;
  RecordId: string;
  Title: string;
  TitleLink: string;
  Location: string;
  AdditionalProperties: { LocationLink: string };
  ThumbnailUrl: string;
  CanRenew: string;
  CannotRenewReason: string;
  IsLate: string;
  IsSoonLate: string;
  IsInUserBasket: string;
  State: string;
  WhenBack: string;
  // custom part?
  holding: BookHolding;
  inBasket: boolean;
  searchId: string;
};

export type LoanRenewData = {
  HoldingId: string;
  RecordId: string;
  Id: string;
};

export type APILoan = {
  renewData?: LoanRenewData;
  holding: {
    id: string;
  };
  searchId: string;
  title: string;
  url: string;
  location: string;
  locationLink: string;
  thumb: string;
  canRenew: string;
  cannotRenewReason: string;
  isLate: string;
  isSoonLate: string;
  inBasket: string;
  status: string;
  whenBack: string;
};

export type Loan = {
  library: {
    name: string;
    url: string;
  };
  whenBack: string;
  details: string;
};

export type UserLogin = {
  username: string;
  password: string;
  logged?: boolean;
};

export type BookHolding = {
  id: string;
  base: string;
};

export type APIBook = {
  url: string;
  isbn: string;
  edition?: string;
  holding: BookHolding;
  thumb?: string;
  loan?: APILoan;
};

export type APISaga = {
  query: string;
  title: string;
  tome: number;
};

export type APINovel = {
  _sorted: string;
  url: string;
  author: string;
  title: string;
  //uid: string;
  saga?: APISaga;
  thumb?: string;
  books: APIBook[];
  edition?: string;
  isbn: string;
  description: string;
};

export type Saga = Omit<APISaga, 'tome'>;
export type Book = Omit<APIBook, 'loan' | 'holding'> & {
  loan?: Loan;
  details: string;
  indexNovel: number;
};

export type Novel = Omit<APINovel, 'books' | 'saga'> & {
  books: Book[];
  sagaVolume?: number;
  indexNovel: number;
  wanted: boolean;
  saga: Saga;
};
