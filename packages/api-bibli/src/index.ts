import { parisBibli } from './paris';

export * from './types';
import { type APINovel } from './types';

//const apiRomainville = {
//search: async (search: string): Promise<APINovel> => {
//const result = await fetch(
//`https://romainville-pom.c3rb.org/recherche-simple/simple/Mots%2BNotice/-1/${encodeURIComponent(search)}/ligne`
//);
//console.log(result);
//return [];
//}
//};

export const search = async (search: string): Promise<APINovel[]> => {
  const parisItems = await parisBibli.search(search);

  //const romainvilleItems = await apiRomainville.search(search);
  //console.log(romainvilleItems);
  return parisItems;
};

export { parisBibli };

export default {
  ...parisBibli,
  search
};
