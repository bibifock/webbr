// @ts-check
import baseConfig from '../../eslint.config.mjs';
import jestPlugin from 'eslint-plugin-jest';

export default [...baseConfig, jestPlugin.configs['flat/recommended']];
