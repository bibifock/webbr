import { getLibrariesId } from '../library';

import { bookcaseModel } from './bookcaseModel';

// TODO move all this shit to ts
const chunk = (arr, size) =>
  Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
    arr.slice(i * size, i * size + size)
  );

export const saveBookcases = async (results) => {
  if (results.length === 0) {
    return;
  }
  const libraries = results.flatMap((r) => r.libraries);
  const librariesId = await getLibrariesId(libraries);

  const bookcases = results.flatMap(({ bookcases }) =>
    bookcases.map(({ libraryURL, ...rest }) => ({
      ...rest,
      idLibrary: librariesId[libraryURL]
    }))
  );

  const bookcaseChunks = chunk(bookcases, 20);

  for (const chunk of bookcaseChunks) {
    await bookcaseModel.saveAll(chunk);
  }
};
