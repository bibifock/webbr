import baseModel from '../model';

const fields = [
  'idLibrary',
  'idBook',
  'isAvailable',
  'isReservable',
  'whenBack',
  'details'
];

const table = 'bookcase';

const model = baseModel.init({
  fields,
  table
});

export const bookcaseModel = model;
