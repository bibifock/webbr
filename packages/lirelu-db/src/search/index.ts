import SQL from 'sql-template-strings';

import { model } from './model';

import database from '../database';
import { userModel } from '../user';
import { searchFollowedModel } from '../search-followed';

export type Search = {
  id: number;
  search: string;
  nbResults: number;
  lastUpdate?: string | Date;
};

type FollowProps = {
  user: { id: number };
  search: { search: string; nbResults: number };
};

const followAction = async ({
  search,
  user,
  isActive
}: FollowProps & { isActive: boolean }) => {
  const found = await userModel.get(user.id);
  if (!found) {
    throw Error('[lirelu-db] User not found');
  }
  const { id: idUser } = found;
  const [{ id: idSearch }] = await model.saveAndPopulate([search]);

  await searchFollowedModel.save({
    idSearch,
    idUser,
    isActive
  });

  return true;
};

const follow = ({ search, user }: FollowProps) =>
  followAction({ search, user, isActive: true });

const unfollow = ({ search, user }: FollowProps) =>
  followAction({ search, user, isActive: false });

const isSearchFollowedByUser = async ({
  search,
  user
}: {
  search: string;
  user: { id: number };
}) => {
  const query = SQL`
    SELECT
      sf.isActive
    FROM searchFollowed sf
      INNER JOIN searchs s ON s.id = sf.idSearch
    WHERE
      s.search=${search} AND sf.idUser=${user.id}
  `;

  const db = await database.connect();
  const result = await db.get(query);

  return result?.isActive === 1;
};

const getActiveSearch = async (user?: { id: number }): Promise<Search[]> => {
  const query = SQL`
    SELECT
      s.id,
      s.nbResults,
      s.search,
      s.created,
      s.updated
    FROM searchs s
      INNER JOIN searchFollowed sf ON sf.idSearch = s.id AND sf.isActive > 0
  `;

  if (user?.id) {
    query.append(SQL`
      WHERE sf.idUser = ${user.id}
    `);
  }

  query.append(SQL`
    ORDER BY s.search
  `);

  const db = await database.connect();
  const result = await db.all(query);

  return result;
};

export const searchModel = {
  ...model,
  follow,
  unfollow,
  isSearchFollowedByUser,
  getActiveSearch
};
