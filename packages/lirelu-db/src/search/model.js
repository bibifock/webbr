import baseModel from '../model';

const fields = ['id', 'search', 'nbResults', 'lastUpdate'];

const table = 'searchs';

const { saveAndPopulate, ...restModel } = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'search'
});

export const model = {
  ...restModel,
  saveAndPopulate
};
