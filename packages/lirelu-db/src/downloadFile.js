import fs from 'fs';
import url from 'url';
import http from 'http';
import https from 'https';

import get from 'lodash/get';

const protocols = { http, https };

/**
 * download image
 * @param {string} url
 * @param {string} dest
 *
 * @return {Promise}
 */
const downloadFile = (uri, dest) =>
  new Promise((resolve, reject) => {
    const onError = (e) => {
      if (fs.existsSync(dest)) {
        fs.unlinkSync(dest);
      }
      reject(e);
    };

    const protocol = url.parse(uri).protocol;
    if (!protocol) {
      reject(new Error('invalid url'));
    }

    const func = get(protocols, protocol.slice(0, -1));

    func
      .get(uri, function (response) {
        if (response.statusCode >= 200 && response.statusCode < 300) {
          const fileStream = fs.createWriteStream(dest);
          fileStream.on('error', onError);
          fileStream.on('close', resolve);
          response.pipe(fileStream);
          return;
        }

        if (response.headers.location) {
          resolve(downloadFile(response.headers.location, dest));
          return;
        }

        reject(new Error(response.statusCode + ' ' + response.statusMessage));
      })
      .on('error', onError);
  });

export default downloadFile;
