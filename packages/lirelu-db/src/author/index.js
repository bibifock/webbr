import baseModel from '../model';

const fields = ['id', 'name'];

const table = 'authors';

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'name'
});

export const authorModel = {
  ...model,
  config: model.config
};
