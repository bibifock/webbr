import SQL from 'sql-template-strings';

import database from '../database';
import { parseStackList } from '../novels-stack/parseStackList';

import { loanModel as baseModel } from './model';

const getLoansForUser = async ({ id }: { id: number }) => {
  const query = SQL`
    SELECT
      JSON_OBJECT(
        'saga', IIF(s.id IS NOT NULL, JSON_OBJECT(
          'id', s.id,
          'title', s.title,
          'volumes', JSON_GROUP_ARRAY(nsaga.sagaVolume)
        ), null),
        'loan', IIF(lo.idBook IS NOT NULL, JSON_OBJECT(
          'idBook', lo.idBook,
          'whenBack', lo.whenBack,
          'isActive', lo.isActive,
          'details', lo.details,
          'updated', lo.updated,
          'library', JSON_OBJECT(
            'id', l.id,
            'name', l.name,
            'url', l.url
          )
        ), NULL),
        'author', JSON_OBJECT(
          'id', a.id,
          'name', a.name
        ),
        'novel', JSON_OBJECT(
          'id', n.id,
          'title', n.title,
          'description', n.description,
          'sagaVolume', n.sagaVolume,
          'created', n.created,
          'thumb', b.image,
          'books', JSON_ARRAY(
            JSON_OBJECT(
              'id', b.id,
              'isbn', b.isbn,
              'edition', b.edition,
              'url', b.url,
              'image', b.image,
              'details', b.details
            )
          )
        )
      ) AS item
    FROM
      loans lo
      INNER JOIN books b ON b.id=lo.idBook
      INNER JOIN novels n ON n.id=b.idNovel
      INNER JOIN authors a ON a.id=n.idAuthor
      LEFT JOIN libraries l ON l.id=lo.idLibrary
      LEFT JOIN saga s ON s.id=n.idSaga
      LEFT JOIN novels nsaga ON nsaga.idSaga=s.id
      WHERE lo.idUser=1
      GROUP BY s.id, n.id, b.id, lo.idLibrary
      ORDER BY lo.updated DESC
    `;

  const db = await database.connect();
  const rows = await db.all(query);

  return rows.map((row: { item: string }) => JSON.parse(row.item));
};

export const loanModel = {
  ...baseModel,
  getLoansForUser
};
