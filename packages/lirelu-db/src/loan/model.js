import SQL from 'sql-template-strings';

import database from '../database';
import baseModel from '../model';

const fields = [
  'idUser',
  'idBook',
  'idLibrary',
  'whenBack',
  'details',
  'updated'
];

const table = 'loans';

const model = baseModel.init({
  fields,
  table
});

/**
 * @param {object} user
 *
 * @return {promise}
 */
model.cleanUser = ({ id }) =>
  database
    .connect()
    .then((db) =>
      db.run(
        SQL`UPDATE `
          .append(table)
          .append(` SET isActive=0 WHERE idUser=${id} AND isActive=1`)
      )
    );

export const loanModel = {
  ...model,
  save: model.save,
  saveAll: model.saveAll
};
