import baseModel from '../model';

const fields = ['idUser', 'idNovel', 'idStatus', 'remark'];

export const table = 'novelsStack';

export const model = baseModel.init({
  fields,
  table
});
