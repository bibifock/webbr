import SQL from 'sql-template-strings';

import database from '../database';
import { concatValues } from '../model';
import { sagaModel } from '../saga';
import { authorModel } from '../author';
import { bookModel } from '../book';
import { novelModel } from '../novel';
import { groupByTable, getAllFields } from '../novel/utils';

import { model, table } from './model';
import { parseStackList } from './parseStackList';

/**
 * @param {string} _.idUser
 * @param {string} _.idNovel
 *
 * @return {promise}
 */
const deleteIt = ({ idUser, idNovel }: { idUser: number; idNovel: number }) =>
  database
    .connect()
    .then((db) =>
      db.run(
        SQL`DELETE FROM `
          .append(table)
          .append(SQL` WHERE idUser=${idUser} AND idNovel=${idNovel} `)
      )
    );

/**
 *
 * get all user novel stack content
 * @param {object} user
 *
 * @return {promise}
 */
const allByUser = async ({ id }: { id: number }) => {
  const query = SQL`
    SELECT
      JSON_OBJECT(
        'saga', IIF(s.id IS NOT NULL, JSON_OBJECT(
          'id', s.id,
          'title', s.title,
          'volumes', JSON_GROUP_ARRAY(nsaga.sagaVolume)
        ), null),
        'loan', IIF(lo.idBook IS NOT NULL, JSON_OBJECT(
          'idBook', lo.idBook,
          'whenBack', lo.whenBack,
          'details', lo.details,
          'library', JSON_OBJECT(
            'id', l.id,
            'name', l.name,
            'url', l.url
          )
        ), NULL),
        'idStackStatus', ns.idStatus,
        'novel', JSON_OBJECT(
          'id', n.id,
          'title', n.title,
          'description', n.description,
          'sagaVolume', n.sagaVolume,
          'created', n.created,
          'author', a.name,
          'thumb', b.image,
          'books', JSON_ARRAY(
            JSON_OBJECT(
              'id', b.id,
              'isbn', b.isbn,
              'edition', b.edition,
              'url', b.url,
              'image', b.image,
              'details', b.details,
              'bookcases', JSON_GROUP_ARRAY(
                JSON_OBJECT(
                  'isAvailable', bc.isAvailable,
                  'isReservable', bc.isReservable,
                  'whenBack', bc.whenBack,
                  'details', bc.details,
                  'library', JSON_OBJECT(
                    'id', l.id,
                    'name', l.name,
                    'url', l.url
                  )
                )
              )
            )
          )
        )
      ) AS item
    FROM
      novelsStack ns
      INNER JOIN novels n ON n.id=ns.idNovel
      INNER JOIN authors a ON a.id=n.idAuthor
      LEFT JOIN books b ON b.idNovel=n.id
      LEFT JOIN bookcase bc ON bc.idBook=b.id
      LEFT JOIN libraries l ON l.id=bc.idLibrary
      LEFT JOIN saga s ON s.id=n.idSaga
      LEFT JOIN loans lo ON
        lo.idBook=b.id AND lo.idUser=ns.idUser AND lo.idLibrary=l.id AND lo.isActive=1
      LEFT JOIN novels nsaga ON nsaga.idSaga=s.id
      WHERE ns.idUser=${id}
      GROUP BY s.id, n.id, b.id, bc.idBook, bc.idLibrary
  `;

  const db = await database.connect();
  const result = await db.all(query);

  return parseStackList(result);
};

export type Novel = {
  novel: {
    id: number;
    title: string;
    description: string;
    idAuthor: number;
    idSaga: number;
    sagaVolume: number;
    created: string;
  };
  loan?: {
    whenBack: string;
    isActive: number;
    updated: string;
    library: {
      id: string;
      name: string;
      url: string;
    };
  };
  saga: {
    id: number;
    title: string;
    nbNovels: number;
  };
  author: {
    id: number;
    name: string;
  };
};

/**
 * return all user linked (saga and novels order by created date desc)
 * @param {object} user
 *
 * @return {promise}
 */
const getNewNovelsForUser = async ({
  id
}: {
  id: number;
}): Promise<Novel[]> => {
  const fields = getAllFields({
    novel: novelModel,
    saga: sagaModel,
    author: authorModel
  });

  const query = SQL`SELECT `
    .append(fields.join(', '))
    .append(
      `
      FROM ${novelModel.config.table}
        INNER JOIN ${authorModel.config.table} ON ${authorModel.config.table}.id=${novelModel.config.table}.idAuthor
        LEFT JOIN ${sagaModel.config.table} ON ${sagaModel.config.table}.id=${novelModel.config.table}.idSaga
      WHERE idSaga IN (
        SELECT DISTINCT ${novelModel.config.table}.idSaga
          FROM ${table}
            INNER JOIN ${novelModel.config.table} ON ${novelModel.config.table}.id=${table}.idNovel
            LEFT JOIN ${sagaModel.config.table} ON ${sagaModel.config.table}.id=${novelModel.config.table}.idSaga
            WHERE ${table}.idUser=
    `
    )
    .append(SQL`${id}`).append(`
      )
      ORDER BY ${novelModel.config.table}.id DESC, ${novelModel.config.table}.sagaVolume DESC
      LIMIT 20
    `);

  const db = await database.connect();
  const result = await db.all(query);

  return result.map(groupByTable);
};

/**
 * get all books in stack by isbn
 * @param {array} _.isbns
 * @param {object} _.user
 *
 * @return {Promise}
 */
const allByISBN = ({
  isbns,
  user
}: {
  user: { id: number };
  isbns: string[];
}): Promise<{ idStatus: number; isbn: string; idNovel: number }[]> => {
  const query = SQL` SELECT `
    .append(
      `
      ${table}.idStatus idStatus,
      ${bookModel.config.table}.isbn isbn,
      ${bookModel.config.table}.idNovel
    `
    )
    .append(
      `
      FROM ${table}
        INNER JOIN ${bookModel.config.table}
          ON ${bookModel.config.table}.idNovel=${table}.idNovel
    `
    )
    .append(` AND ${bookModel.config.table}.isbn IN (`)
    .append(concatValues(isbns))
    .append(')')
    .append(` WHERE ${table}.idUser = `)
    .append(SQL`${user.id}`);

  return database.connect().then((db) => db.all(query));
};

export const novelsStackModel = {
  ...model,
  save: model.save,
  delete: deleteIt,
  allByUser,
  allByISBN,
  getNewNovelsForUser
};
