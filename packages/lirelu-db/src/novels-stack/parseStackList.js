import get from 'lodash/get';
import set from 'lodash/set';

import { STATUSES_VALUES } from '../status';

const findNovelIndex = (v) => (i) => !i.isSaga && i.id === v.id;

const parseBookcase = (v) => ({
  ...v,
  ...JSON.parse(v.details),
  isAvailable: v.isAvailable > 0
});

const parseBookcases = ({ bookcases, ...rest }) => ({
  ...rest,
  bookcases: bookcases.map(parseBookcase)
});

const parseLoan = (data) => {
  if (!data) {
    return;
  }

  const { details, whenBack, ...rest } = data;
  return {
    ...rest,
    whenBack: parseInt(whenBack),
    ...JSON.parse(details)
  };
};

const getNewModel = ({ novel, idStackStatus, loan }) => {
  return {
    ...novel,
    loan: parseLoan(loan),
    books: [parseBookcases(novel.books[0])],
    status: get(STATUSES_VALUES, idStackStatus)
  };
};

const applyBookItem = (arr, { novel, loan }, novelKey) => {
  if (loan) {
    set(arr, `${novelKey}.loan`, parseLoan(loan));
  }

  const booksKey = `${novelKey}.books`;
  const indexBooks = get(arr, booksKey).findIndex(
    (b) => b.id === novel.books[0].id
  );

  if (indexBooks === -1) {
    set(arr, booksKey, [...get(arr, booksKey), parseBookcases(novel.books[0])]);
    return arr;
  }

  const bookcasesKey = `${booksKey}.${indexBooks}.bookcases`;
  const bookcase = novel.books[0].bookcases[0];

  const index = get(arr, bookcasesKey).findIndex(
    (b) => b.library.id === bookcase.library.id
  );
  if (index === -1) {
    set(arr, bookcasesKey, [
      ...get(arr, bookcasesKey),
      parseBookcase(bookcase)
    ]);
  }

  return arr;
};

const applyNovelItem = (arr, item) => {
  const { novel } = item;
  const novelIndex = arr.findIndex(findNovelIndex(novel));

  if (novelIndex === -1) {
    return [...arr, getNewModel(item)];
  }

  return applyBookItem(arr, item, novelIndex);
};

const applySagaItem = (arr, item) => {
  const { saga, novel } = item;

  const sagaIndex = arr.findIndex((s) => s.isSaga && s.id === saga.id);
  if (sagaIndex === -1) {
    return [
      ...arr,
      {
        ...saga,
        isSaga: true,
        novels: [getNewModel(item)]
      }
    ];
  }

  const novelIndex = arr[sagaIndex].novels.findIndex(findNovelIndex(novel));
  if (novelIndex === -1) {
    const { novels, ...sagaRest } = arr[sagaIndex];
    set(arr, sagaIndex, {
      ...sagaRest,
      novels: [...novels, getNewModel(item)]
    });
    return arr;
  }

  return applyBookItem(arr, item, `${sagaIndex}.novels.${novelIndex}`);
};

export const parseStackList = (result) =>
  result.reduce((acc, v) => {
    const item = JSON.parse(v.item);
    if (item.saga) {
      return applySagaItem(acc, item);
    }

    return applyNovelItem(acc, item);
  }, []);
