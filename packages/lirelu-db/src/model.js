import SQL from 'sql-template-strings';

import database from './database';

/**
 * transform array of items.
 * [1, 2, 3] => SQL'${1}, ${2}, ${3}';
 *
 * @param {array} items
 * @param {func} each function call for each elements (by default add value)
 * @param {func} concat function call for each join action (by default add ', ')
 *
 * @return object sql template string
 */
export const concatValues = (
  items,
  each = (item) => SQL`${item}`,
  concat = (index) => (index > 0 ? ', ' : '')
) => {
  const query = SQL``;
  items.forEach((item, index) =>
    query.append(concat(index)).append(each(item))
  );
  return query;
};

/**
 * generate populateBy and saveBy functions
 *
 * @param {string} _.by pivot field
 * @param {object} _.model vege-data model
 * @param {func} _.populate
 *
 * @return {object} { populateBy, saveBy }
 */
export const generateSaveAndPopulate = ({ by, model, populate }) => {
  if (!populate) {
    populate = (items) =>
      database
        .connect()
        .then((db) => {
          const query = SQL``
            .append(model.queries.raw.select)
            .append(` WHERE ${by} IN (`)
            .append(concatValues(items.map((i) => i[by])))
            .append(')');

          return db.all(query);
        })
        .then((rows) =>
          items.map((item) => ({
            ...(rows.find(
              (i) => i[by].toLowerCase() === item[by].toLowerCase()
            ) || {}),
            ...item
          }))
        );
  }

  const { primaryKey } = model.config;

  const saveAndPopulate = (objects) => {
    let items = [];
    return populate(objects)
      .then((r) => {
        items = r;
        return model.saveAll(r);
      })
      .then(async () => {
        const uncomplete = items.filter((i) => !i[primaryKey]);
        if (!uncomplete.length) {
          return items;
        }

        const newItems = await populate(uncomplete);

        return items.filter((i) => i[primaryKey]).concat(newItems);
      });
  };

  return { saveAndPopulate, populate };
};

/**
 * @param {string} _.populateBy field to populate by
 * @param {object} ...rest vege-data model.init options
 *
 * @return {object}
 */
const init = ({ populateBy: by = '', ...config }) => {
  const model = database.model.init(config);

  if (by) {
    const { populate, saveAndPopulate } = generateSaveAndPopulate({
      by,
      model
    });
    model.populate = populate;
    model.saveAndPopulate = saveAndPopulate;
  }

  model.getFields = (prefix = '') =>
    model.config.fields.map(
      (f) => `${model.config.table}.${f} as ${prefix}${f}`
    );

  return model;
};

export default { init };
