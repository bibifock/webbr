import SQL from 'sql-template-strings';
import bcrypt from 'bcryptjs';

import database from '../database';

const saltRounds = 10;

const fields = [
  'id',
  'email',
  'name',
  'created',
  'updated',
  'dateOfBirth',
  'lastConnection'
];

const table = 'users';

const getAllFields = () =>
  SQL`SELECT password, `.append(fields.join(',')).append(' FROM ' + table);

const model = database.model.init({
  fields,
  table,
  primaryKey: 'id'
});

/**
 * @param {string} _.login
 * @param {string} _.password
 *
 * @throws {Error} user not found
 * @throws {Error} login not working
 *
 * @return {Promise}
 */
model.login = async ({ login, password }) => {
  const db = await database.connect();

  const query = getAllFields().append(
    SQL` WHERE email=${login} OR name=${login} AND password IS NOT NULL`
  );

  const result = await db.get(query);

  if (!result) {
    throw Error('user not found');
  }

  const { password: hash, ...user } = result;

  const isLogged = await bcrypt.compare(password, hash);

  if (!isLogged) {
    throw Error('sorry, but this will not be possible');
  }

  return user;
};

/**
 * @param {object} user
 *
 * @throws {Error} user not found
 * @throws {Error} not possible
 * @throws {Error} name not available
 *
 * @return {object}
 */
model.register = async (args) => {
  const { email, password, name } = args;
  const db = await database.connect();

  const user = await db.get(getAllFields().append(SQL` WHERE email=${email}`));

  if (!user) {
    throw Error('user not found');
  }

  if (user.password) {
    throw Error('sorry, this is not possible');
  }

  const result = await model.getByProps({ name });
  if (result && result.email !== email) {
    throw Error('username, already taken');
  }

  const hash = await bcrypt.hash(password, saltRounds);

  await db.run(
    SQL` UPDATE `.append(table).append(SQL` SET
        updated=CURRENT_TIMESTAMP,
        password=${hash},
        name=${name}
        WHERE email=${email}
      `)
  );

  return args;
};

/**
 * get user with active library account
 *
 * @return {promise}
 */
const getActiveAccounts = () =>
  database.connect().then((db) =>
    db.all(
      SQL``.append(model.queries.raw.select).append(`
        WHERE dateOfBirth IS NOT NULL
        AND id IN (
          SELECT DISTINCT idUser
          FROM novelsStack
        )
      `)
    )
  );

export default {
  ...model,
  getActiveAccounts
};
