import baseModel from './model';

const { getActiveAccounts, ...model } = baseModel;

export type User = {
  id: number;
  email: string;
  name: string;
  dateOfBirth: string;
};

export const userModel = {
  ...model,
  getActiveAccounts
};
