import { libraryModel as baseModel } from './libraryModel';
export * from './getLibrariesId';

export const libraryModel = {
  ...baseModel,
  all: baseModel.all
};
