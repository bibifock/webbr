import baseModel from '../model';

const fields = ['id', 'name', 'url'];

const table = 'libraries';

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'url',
  orderBy: 'name'
});

export const libraryModel = model;
