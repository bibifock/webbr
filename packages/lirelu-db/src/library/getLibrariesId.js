import uniqBy from 'lodash/uniqBy';

import { libraryModel } from './libraryModel';

/**
 *
 * get libraries ids and saving missing librairies
 * @param {array}
 * @return {promise}
 */
export const getLibrariesId = async (libraries) => {
  const libs = await libraryModel.saveAndPopulate(uniqBy(libraries, 'url'));

  return libs.reduce((o, { id, url }) => ({ ...o, [url]: id }), {});
};
