import SQL from 'sql-template-strings';

import database from '../database';
import { Search } from '../search';
import type { User } from '../user';
import { concatValues } from '../vege-data';

import { model as baseModel } from './model';

export type SearchFollowed = Search & { id: number; lastUpdate: string };

export const searchFollowedModel = {
  ...baseModel,
  getFollowedNewResult: async (user: User): Promise<SearchFollowed[]> => {
    const query = SQL`
    SELECT
      s.id,
      s.search,
      s.nbResults,
      s.lastUpdate
    FROM searchFollowed sf
      INNER JOIN searchs s ON s.id = sf.idSearch
    WHERE
      sf.idUser = ${user.id}
      AND sf.isActive > 0
      AND s.nbResults > 0
    ORDER BY s.lastUpdate DESC
  `;

    const db = await database.connect();
    const rows = await db.all(query);

    return rows;
  },
  inactivateSearch: async (ids: number[]) => {
    const query = SQL`
      UPDATE searchFollowed SET isActive = 0 WHERE idSearch IN (
        `
      .append(concatValues(ids))
      .append(')');

    const db = await database.connect();
    await db.all(query);
  }
};
