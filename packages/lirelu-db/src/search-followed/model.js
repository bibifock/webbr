import database from '../database';

const fields = ['idUser', 'idSearch', 'isActive'];

const table = 'searchFollowed';

export const model = database.model.init({
  fields,
  table,
  primaryKey: 'idUser'
});
