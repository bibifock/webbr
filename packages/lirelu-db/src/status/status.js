import database from '../database';

const fields = ['id', 'name'];

const table = 'status';

const model = database.model.init({
  fields,
  table,
  primaryKey: 'id'
});

export const statusModel = model;
