import invert from 'lodash/invert';

export const STATUS_TO_READ = 1;
export const STATUS_IS_READING = 2;
export const STATUS_FINISHED = 3;
export const STATUS_UNFINISHED = 4;

export const STATUSES = {
  STATUS_TO_READ: STATUS_TO_READ,
  STATUS_IS_READING: STATUS_IS_READING,
  STATUS_FINISHED: STATUS_FINISHED,
  STATUS_UNFINISHED: STATUS_UNFINISHED
};

export const isStatus = (value: unknown): value is keyof typeof STATUSES =>
  typeof value === 'string' && value in STATUSES;

export const STATUSES_VALUES = invert(STATUSES);
