// TODO move this function in another package
const getIsbndbUrl = (isbn: string) => {
  const regIsbnbd = /(\d{2})(\d{2})$/;
  if (!regIsbnbd.test(isbn)) {
    return undefined;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [_, one, two] = isbn.match(regIsbnbd) as string[];
  return `https://images.isbndb.com/covers/${one}/${two}/${isbn}.jpg`;
};

const getAcebookImageByIsbn = (isbn: string) =>
  `https://pictures.abebooks.com/isbn/${isbn}.jpg`;

export const getImagesUrlByIsbn = (isbn: string) => [
  getIsbndbUrl(isbn),
  getAcebookImageByIsbn(isbn)
];
