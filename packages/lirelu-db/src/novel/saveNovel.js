import get from 'lodash/get';

import { STATUS_TO_READ } from '../status';
import { authorModel } from '../author';
import { bookModel } from '../book';
import { saveBookcases } from '../bookcase';
import { getLibrariesId } from '../library';
import { loanModel } from '../loan';
import { novelsStackModel } from '../novels-stack';
import { sagaModel } from '../saga';
import downloadFile from '../downloadFile';

import { novelModel } from './novelModel';
import { getImagesUrlByIsbn } from './getImagesUrlByIsbn';

/**
 * get book image upload dir
 * @param {object} book
 * @param {string} ext
 *
 * @return {string}
 */
export const getBookImg = ({ id }, path, ext = '.png') =>
  `${path.replace(/\/$/, '')}/${id}${ext}`;

/**
 * @param {object} book
 *
 * @return {Promise}
 */
export const downloadBookImage =
  ({ booksPath, uploadDir }) =>
  async (book) => {
    const { thumb } = book;
    const imagesIsbnUrls = getImagesUrlByIsbn(book.isbn);
    const images = (thumb ? [thumb, imagesIsbnUrls] : imagesIsbnUrls).filter(
      (v) => !!v
    );

    if (images.length === 0) {
      return null;
    }

    // thumbs are jpg by default
    const image = getBookImg(book, booksPath, '.jpg');
    const errors = [];
    for (let i = 0; i < images.length; i++) {
      const currUrl = images[i];

      try {
        await downloadFile(currUrl, uploadDir + image);

        return { ...book, image };
      } catch (e) {
        errors.push({ url: currUrl, error: e.message });
      }
    }

    // eslint-disable-next-line no-console
    console.warn('downloadBookImage', { isbn: book.isbn, errors });
    return null;
  };

/**
 * @param {array}
 *
 * @return {Promise}
 */
export const getMissingBookImage = async (books, uploadOptions) => {
  const needsImage = books.filter(({ image }) => !image);
  if (!needsImage.length) {
    return;
  }
  const result = await Promise.all(
    needsImage.map(downloadBookImage(uploadOptions))
  );

  // remove null thumb
  const newBooksWithImage = result.filter((b) => b);
  if (!newBooksWithImage.length) {
    return;
  }

  return bookModel.saveAll(newBooksWithImage);
};

/**
 * @param {object} user
 * @param {array} novels
 *
 * @return {promise}
 */
const addNovelToStack = async (user, novels) => {
  if (!user) {
    return;
  }

  const wantedNovel = novels.find(({ wanted }) => wanted);
  if (!wantedNovel) {
    return;
  }

  const row = {
    idNovel: wantedNovel.id,
    idUser: user.id
  };

  const stack = await novelsStackModel.getByProps(row);
  if (stack) {
    // already in stack
    return;
  }

  await novelsStackModel.save({
    ...row,
    idStatus: STATUS_TO_READ
  });
};

/**
 *
 * @param {array} books
 * @param {object} user
 * @return {promise}
 */
const saveBookLoans = async (books, user) => {
  if (!books.length || !user) {
    return;
  }

  const librariesId = await getLibrariesId(
    books.map(({ loan: { library } }) => library)
  );

  const toSave = books.map(({ id, loan: { library, ...rest } }) => ({
    idBook: id,
    idUser: user.id,
    idLibrary: librariesId[library.url],
    updated: Date.now(),
    ...rest
  }));

  await loanModel.saveAll(toSave);
};

export const saveNovel = async (
  { saga, ...infos },
  user = undefined,
  { getBookcases, ...uploadOptions }
) => {
  let idSaga;
  if (saga) {
    const [found] = await sagaModel.saveAndPopulate([saga]);
    idSaga = found.id;
  }

  const authors = await authorModel.saveAndPopulate(infos.authors);
  const authorsId = authors.reduce(
    (o, { id, name }) => ({ ...o, [name]: id }),
    {}
  );

  const novels = await novelModel.saveAndPopulate(
    infos.novels.map(({ author, ...rest }) => ({
      ...rest,
      idAuthor: get(authorsId, author),
      idSaga
    }))
  );

  const novelsId = novels.reduce(
    (o, { id, indexNovel }) => ({ ...o, [indexNovel]: id }),
    {}
  );

  const books = await bookModel.saveAndPopulate(
    infos.books.map(({ indexNovel, ...rest }) => ({
      ...rest,
      idNovel: get(novelsId, indexNovel)
    }))
  );

  const loans = books.filter((b) => !!b.loan);

  await Promise.all([
    getMissingBookImage(books, uploadOptions),
    addNovelToStack(user, novels),
    getBookcases(books).then(saveBookcases)
  ]);

  await saveBookLoans(loans, user);

  const { id: idNovel } = novels.find(({ wanted }) => wanted) || {};
  if (!idNovel) {
    return null;
  }

  const [nov, novBooks] = await Promise.all([
    novelModel.get(idNovel),
    bookModel.allByProps({ idNovel })
  ]);

  return { ...nov, books: novBooks };
};
