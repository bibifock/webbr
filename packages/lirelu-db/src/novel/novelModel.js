import SQL from 'sql-template-strings';
import get from 'lodash/get';

import database from '../database';
import { authorModel } from '../author';
import { bookModel } from '../book';
import { bookcaseModel } from '../bookcase';
import { libraryModel } from '../library';
import baseModel, { generateSaveAndPopulate } from '../model';

import { table, groupByTable, getJoins, getAllFields } from './utils';

const fields = [
  'id',
  'title',
  'description',
  'idAuthor',
  'idSaga',
  'sagaVolume',
  'created'
];

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id'
});

/**
 * currying comparaison
 *
 * @return {boolean}
 */
const isSame = (a) => (b) =>
  b.title.toLowerCase() === a.title.toLowerCase() &&
  b.idAuthor === a.idAuthor &&
  get(b, 'idSaga', null) === get(a, 'idSaga', null) &&
  get(b, 'sagaVolume', null) === get(a, 'sagaVolume', null);

/**
 * populate novels
 * @param {array}
 *
 * @return {promise}
 */
model.populate = (items) =>
  database
    .connect()
    .then((db) => {
      const query = SQL``.append(model.queries.raw.select).append(' WHERE ');
      items.forEach(({ title, idAuthor, sagaVolume, idSaga }, index) => {
        query.append(index ? ' OR ' : '');
        query.append(SQL` ( title = ${title} AND idAuthor= ${idAuthor} `);
        if (idSaga) {
          query.append(SQL` AND idSaga=${idSaga}`);
          if (sagaVolume) {
            // et ui parce que des fois il y a des bouquins dans une saga
            // qui n'ont pas de tomes :'(
            query.append(SQL` AND sagaVolume=${sagaVolume}`);
          }
        }
        query.append(') ');
      });

      return db.all(query);
    })
    .then((rows) =>
      items.map((item) => {
        const found = rows.find(isSame(item)) || {};

        return {
          ...found,
          ...item,
          created: get(
            found,
            'created',
            new Date().toISOString().replace(/T/, ' ').replace(/\..+$/, '')
          )
        };
      })
    );

const { saveAndPopulate } = generateSaveAndPopulate({
  populate: model.populate,
  model
});

model.saveAndPopulate = saveAndPopulate;

/**
 * @param {object} params
 *
 * @return {promise}
 */
const getFilledByProps = async (params) => {
  const fields = getAllFields({
    author: authorModel,
    book: bookModel,
    bookcase: bookcaseModel,
    library: libraryModel,
    novel: model
  });

  const query = SQL`SELECT `
    .append(fields.join(', '))
    .append(` FROM ${table} `)
    .append(getJoins())
    .append(SQL` WHERE 1=1 `);

  Object.keys(params).forEach((key) => {
    const value = params[key];
    query.append(` AND ${table}.${key} = `).append(SQL`${value}`);
  });

  const db = await database.connect();
  const result = await db.all(query);

  return result.map(groupByTable);
};

export const novelModel = {
  ...model,
  config: model.config,
  get: model.get,
  getFilledByProps
};
