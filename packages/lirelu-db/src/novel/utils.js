import get from 'lodash/get';
import flatten from 'lodash/flatten';

import { authorModel } from '../author';
import { bookModel } from '../book';
import { bookcaseModel } from '../bookcase';
import { libraryModel } from '../library';

export const table = 'novels';

export const getAllFields = (models) =>
  flatten(Object.keys(models).map((k) => models[k].getFields(`__${k}__`)));

export const getJoins = () => `
  INNER JOIN ${authorModel.config.table} ON ${authorModel.config.table}.id=${table}.idAuthor
  LEFT JOIN ${bookModel.config.table} ON ${bookModel.config.table}.idNovel=${table}.id
  LEFT JOIN ${bookcaseModel.config.table} ON ${bookcaseModel.config.table}.idBook=${bookModel.config.table}.id
  LEFT JOIN ${libraryModel.config.table} ON ${libraryModel.config.table}.id=${bookcaseModel.config.table}.idLibrary
`;

export const groupByTable = (row) => {
  const keys = Object.keys(row);

  return keys.reduce((o, key) => {
    if (!row[key]) {
      return o;
    }
    const {
      groups: { table, field }
    } = key.match(/^__(?<table>[^_]+)__(?<field>.+)$/);

    return {
      ...o,
      [table]: {
        ...get(o, table, {}),
        [field]: row[key]
      }
    };
  }, {});
};
