import SQL from 'sql-template-strings';

import database from '../database';
import { userModel } from '../user';
import { novelModel } from '../novel';
import { novelsStackModel } from '../novels-stack';
import baseModel from '../model';

const fields = ['id', 'idNovel', 'isbn', 'edition', 'url', 'image', 'details'];

const table = 'books';

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'isbn'
});

/**
 * return all book in user stack
 *
 * @return {promise}
 */
model.getBookInStack = () => {
  const query = SQL`SELECT DISTINCT `.append(
    fields.map((v) => `${table}.${v}`).join(', ')
  ).append(`
      FROM ${table}
        INNER JOIN ${novelsStackModel.config.table}
          ON ${novelsStackModel.config.table}.idNovel=${table}.idNovel
        INNER JOIN ${userModel.config.table}
          ON ${userModel.config.table}.id=${novelsStackModel.config.table}.idUser
      WHERE ${userModel.config.table}.dateOfBirth IS NOT NULL
    `);

  return database.connect().then((db) => db.all(query));
};

model.getBookWithoutSaga = () => {
  const query = SQL`SELECT DISTINCT `.append(
    fields.map((v) => `${table}.${v}`).join(', ')
  ).append(`
      FROM ${table}
        INNER JOIN ${novelModel.config.table} ON ${novelModel.config.table}.id=${table}.idNovel
      WHERE ${novelModel.config.table}.idSaga IS NULL
    `);

  return database.connect().then((db) => db.all(query));
};

export const bookModel = { ...model, config: model.config };
