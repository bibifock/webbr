import baseModel from '../model';

const fields = ['id', 'title', 'nbNovels'];

const table = 'saga';

const model = baseModel.init({
  fields,
  table,
  primaryKey: 'id',
  populateBy: 'title'
});

export const sagaModel = {
  ...model,
  config: model.config
};
